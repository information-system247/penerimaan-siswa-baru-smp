/*
SQLyog Ultimate v12.5.1 (32 bit)
MySQL - 10.4.22-MariaDB : Database - pendaftaran_smp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pendaftaran_smp` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `pendaftaran_smp`;

/*Table structure for table `admin_pendaftaran` */

DROP TABLE IF EXISTS `admin_pendaftaran`;

CREATE TABLE `admin_pendaftaran` (
  `id_admin_pendaftaran` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(300) NOT NULL,
  `nik` varchar(25) NOT NULL,
  `hak_akses` varchar(20) NOT NULL,
  PRIMARY KEY (`id_admin_pendaftaran`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `no_hp` (`no_hp`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `nik` (`nik`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `admin_pendaftaran` */

insert  into `admin_pendaftaran`(`id_admin_pendaftaran`,`nama`,`gender`,`email`,`no_hp`,`username`,`password`,`nik`,`hak_akses`) values 
(1,'wahyu imam','L','admin@admin.com','088980006642','admin','7122ec39733cc51de892a1d1973eb00e23745de066dc7c29e296657d7c6900390f561861ed2cb9404823eb1f07cee69598b7e424317a1f8f218d9287d3332047mTLUTLX5JvPTLi3FIsVIfdc1FpuVQQSdn0R/WKw+Va0=','123456789','ADMIN'),
(3,'imam','P','test@test.com','0818245823543','user1','22ea54b10bc1c1eaeb0f06273c9290560adb157a8b31a791d1c8559b64235e4e31ace5b15cd8b8973f6b52a57bca2977a6bc9636a6ff5ccb8179880a98f0e4a7s80qKu7PSZFLHZbPwZb/1pdyWzscoaTKPc9zk4N/scM=','12121212121212','PANITIA');

/*Table structure for table `calon_siswa` */

DROP TABLE IF EXISTS `calon_siswa`;

CREATE TABLE `calon_siswa` (
  `id_calon_siswa` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `agama` varchar(20) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `kecamatan` varchar(50) DEFAULT NULL,
  `kabupaten` varchar(50) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `no_hp` varchar(15) DEFAULT NULL,
  `nama_ortu` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(300) DEFAULT NULL,
  `nisn` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_calon_siswa`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `calon_siswa` */

insert  into `calon_siswa`(`id_calon_siswa`,`nama_lengkap`,`gender`,`agama`,`alamat`,`kecamatan`,`kabupaten`,`tempat_lahir`,`tanggal_lahir`,`no_hp`,`nama_ortu`,`username`,`password`,`nisn`) values 
(1,'test','L','islam','test','test','rembang','test','1998-01-04','2114454235353',NULL,'test','02eb8c2d6ef58dde56601480588a6faf76bf38d23451c0ee955290a70f4ef893a59bc23d0457a6f4d0485589af5eab9b1f6523298838713aa64c7d708523c1cabWb9dxCmnsTQ/1pWGlZ7LUvl82ookmtoJ4W4NVJ7zyc=',NULL),
(2,'test','L','islam','test','test','rembang','test','2022-04-12','424312534643245','test','test1','491b3fe39e544acd0d0e9501a17d15aeb3943e3557d2334156b9e04042f617c378a19b4eb4598e3f8914c9277b0235d445d40c142d2ef9610c59c1907435f129BVqcZIczwIvE8QQcU58Q1IG1jxrZQRGpcsoJVONhB5Q=',NULL);

/*Table structure for table `daftar_ulang` */

DROP TABLE IF EXISTS `daftar_ulang`;

CREATE TABLE `daftar_ulang` (
  `id_daftar_ulang` bigint(20) NOT NULL AUTO_INCREMENT,
  `tgl_daftar_ulang` datetime NOT NULL,
  `status_id` bigint(20) NOT NULL,
  `no_pendaftaran` varchar(20) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `nisn` varchar(20) NOT NULL,
  `nik_siswa` varchar(20) NOT NULL,
  `tmp_lhr_siswa` varchar(50) NOT NULL,
  `tgl_lhr_siswa` date NOT NULL,
  `agama` varchar(20) NOT NULL,
  `kewarganegaan` varchar(3) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `rt` varchar(3) NOT NULL,
  `rw` varchar(3) NOT NULL,
  `nama_dusun` varchar(50) NOT NULL,
  `nama_kelurahan` varchar(50) NOT NULL,
  `kecamatan` varchar(50) NOT NULL,
  `kabupaten` varchar(50) NOT NULL,
  `kode_pos` varchar(5) NOT NULL,
  `nama_ayah` varchar(50) NOT NULL,
  `nik_ayah` varchar(20) NOT NULL,
  `thn_lhr_ayah` varchar(4) NOT NULL,
  `nama_ibu` varchar(50) NOT NULL,
  `nik_ibu` varchar(20) NOT NULL,
  `thn_lhr_ibu` varchar(4) NOT NULL,
  `no_hp_ortu` varchar(15) NOT NULL,
  `upload_dokumen` varchar(100) NOT NULL,
  `status_review` varchar(20) NOT NULL,
  `catatan` text DEFAULT NULL,
  PRIMARY KEY (`id_daftar_ulang`),
  UNIQUE KEY `no_pendaftaran` (`no_pendaftaran`),
  UNIQUE KEY `nisn` (`nisn`),
  UNIQUE KEY `nik_siswa` (`nik_siswa`),
  UNIQUE KEY `status_id` (`status_id`),
  CONSTRAINT `daftar_ulang_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status_seleksi_pendaftaran_calon_siswa` (`id_status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `daftar_ulang` */

insert  into `daftar_ulang`(`id_daftar_ulang`,`tgl_daftar_ulang`,`status_id`,`no_pendaftaran`,`nama_lengkap`,`gender`,`nisn`,`nik_siswa`,`tmp_lhr_siswa`,`tgl_lhr_siswa`,`agama`,`kewarganegaan`,`alamat`,`rt`,`rw`,`nama_dusun`,`nama_kelurahan`,`kecamatan`,`kabupaten`,`kode_pos`,`nama_ayah`,`nik_ayah`,`thn_lhr_ayah`,`nama_ibu`,`nik_ibu`,`thn_lhr_ibu`,`no_hp_ortu`,`upload_dokumen`,`status_review`,`catatan`) values 
(1,'2022-05-07 05:26:24',1,'2022050520220000001','Muhammad Wahyu Imamuddin','L','14215657235457675634','65536547657345643654','Rembang','2006-01-04','islam','WNI','Rembang','007','001','Garang','Tlogomojo','Rembang','Rembang','59251','Masrur','41356254764584526345','1976','Sunarti','14135637243543453265','1980','2114454235353','2022050520220000001_DAFTAR_ULANG.zip','REVIEWED','ok');

/*Table structure for table `parameter` */

DROP TABLE IF EXISTS `parameter`;

CREATE TABLE `parameter` (
  `id_parameter` bigint(20) NOT NULL AUTO_INCREMENT,
  `module` varchar(30) NOT NULL,
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`id_parameter`),
  UNIQUE KEY `module` (`module`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `parameter` */

insert  into `parameter`(`id_parameter`,`module`,`value`) values 
(1,'Tahun Ajaran','2022'),
(2,'Batas Waktu Pendaftaran','2022-05-31'),
(3,'Pengumuman Seleksi','2022-06-15'),
(4,'Batas Waktu Daftar Ulang','2022-06-30');

/*Table structure for table `pendaftaran` */

DROP TABLE IF EXISTS `pendaftaran`;

CREATE TABLE `pendaftaran` (
  `id_pendaftaran` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_pendaftaran` varchar(20) NOT NULL,
  `tanggal_daftar` datetime NOT NULL,
  `tahun_ajaran` int(4) NOT NULL,
  `no_ijazah` varchar(20) NOT NULL,
  `upload_dokumen` varchar(100) DEFAULT NULL,
  `calon_siswa_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id_pendaftaran`),
  UNIQUE KEY `no_ijazah` (`no_ijazah`),
  UNIQUE KEY `no_pendaftaran` (`no_pendaftaran`),
  KEY `calon_siswa_id` (`calon_siswa_id`),
  CONSTRAINT `pendaftaran_ibfk_1` FOREIGN KEY (`calon_siswa_id`) REFERENCES `calon_siswa` (`id_calon_siswa`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

/*Data for the table `pendaftaran` */

insert  into `pendaftaran`(`id_pendaftaran`,`no_pendaftaran`,`tanggal_daftar`,`tahun_ajaran`,`no_ijazah`,`upload_dokumen`,`calon_siswa_id`) values 
(5,'2022050520220000001','2022-05-05 12:31:36',2022,'56775453434534535435','2022050520220000001_PENDAFTARAN.zip',1);

/*Table structure for table `status_seleksi_pendaftaran_calon_siswa` */

DROP TABLE IF EXISTS `status_seleksi_pendaftaran_calon_siswa`;

CREATE TABLE `status_seleksi_pendaftaran_calon_siswa` (
  `id_status` bigint(20) NOT NULL AUTO_INCREMENT,
  `no_pendaftaran` varchar(20) NOT NULL,
  `tgl_diterima_atau_ditolak` datetime NOT NULL,
  `umur` varchar(3) NOT NULL,
  `status_diterima_atau_ditolak` varchar(20) NOT NULL,
  `admin_pendaftaran_id` bigint(20) NOT NULL,
  `status_review` varchar(20) NOT NULL,
  `batas_waktu_seleksi` date NOT NULL,
  PRIMARY KEY (`id_status`),
  UNIQUE KEY `no_pendaftaran` (`no_pendaftaran`),
  KEY `admin_pendaftaran_id` (`admin_pendaftaran_id`),
  CONSTRAINT `status_seleksi_pendaftaran_calon_siswa_ibfk_2` FOREIGN KEY (`no_pendaftaran`) REFERENCES `pendaftaran` (`no_pendaftaran`),
  CONSTRAINT `status_seleksi_pendaftaran_calon_siswa_ibfk_3` FOREIGN KEY (`admin_pendaftaran_id`) REFERENCES `admin_pendaftaran` (`id_admin_pendaftaran`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `status_seleksi_pendaftaran_calon_siswa` */

insert  into `status_seleksi_pendaftaran_calon_siswa`(`id_status`,`no_pendaftaran`,`tgl_diterima_atau_ditolak`,`umur`,`status_diterima_atau_ditolak`,`admin_pendaftaran_id`,`status_review`,`batas_waktu_seleksi`) values 
(1,'2022050520220000001','2022-05-06 10:46:48','24','TERIMA',1,'REVIEWED','2022-06-15');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
