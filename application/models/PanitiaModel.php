<?php 

class PanitiaModel extends CI_Model
{
  public function findAll()
  {
    $this->db->where('hak_akses', 'PANITIA');
    return $this->db->get('admin_pendaftaran')->result();
  }

  public function findByUsernameAndPassword($username, $password)
  {
    $this->db->where('username', $username);
    $this->db->where('password', $password);
    return $this->db->get('admin_pendaftaran')->result();
  }

  public function findByUsername($username)
  {
    return $this->db->get_where('admin_pendaftaran', array('username' => $username))->result();
  }

  public function findByNik($nik)
  {
    return $this->db->get_where('admin_pendaftaran', array('nik' => $nik))->result();
  }

  public function findByEmail($email)
  {
    return $this->db->get_where('admin_pendaftaran', array('email' => $email))->result();
  }

  public function findByPhoneNumber($phoneNumber)
  {
    return $this->db->get_where('admin_pendaftaran', array('no_hp' => $phoneNumber))->result();
  }

  public function save($data)
  {
    return $this->db->insert('admin_pendaftaran', $data);
  }

  public function findByHakAkses($hakAkses)
  {
    return $this->db->get_where('admin_pendaftaran', array('hak_akses' => $hakAkses))->result();
  }

  public function findByNotInHakAkses($hakAkses)
  {
    return $this->db->get_where('admin_pendaftaran', array('hak_akses !=' => $hakAkses))->result();
  }

  public function findById($id)
  {
    return $this->db->get_where('admin_pendaftaran', array('id_admin_pendaftaran' => $id))->result();
  }

  public function update($data)
  {
    return $this->db->replace('admin_pendaftaran', $data);
  }

  public function delete($id)
  {
    return $this->db->delete('admin_pendaftaran', array('id_admin_pendaftaran' => $id));
  }
}
