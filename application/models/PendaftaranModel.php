<?php 

class PendaftaranModel extends CI_Model
{
  public function findByCalonSiswaId($id)
  {
    return $this->db->get_where('pendaftaran', array('calon_siswa_id' => $id))->result();
  }

  public function findAll()
  {
    return $this->db->get('pendaftaran')->result();
  }

  public function delete($id)
  {
    return $this->db->delete('pendaftaran', array('id_pendaftaran' => $id));
  }

  public function findById($id)
  {
    return $this->db->get_where('pendaftaran', array('id_pendaftaran' => $id))->result();
  }

  public function findByNoIjazah($noIjazah)
  {
    return $this->db->get_where('pendaftaran', array('no_ijazah' => $noIjazah))->result();
  }

  public function save($data)
  {
    return $this->db->insert('pendaftaran', $data);
  }

  public function update($data)
  {
    return $this->db->replace('pendaftaran', $data);
  }

  public function findOrderByIdDescLimit1()
  {
    $this->db->from('pendaftaran');
    $this->db->order_by('id_pendaftaran', 'DESC');
    return $this->db->limit(1)->get()->result();
  }

  public function findByTahunAjaranAndCalonSiswaId($tahunAjaran, $calonSiswaId)
  {
    $data = array(
      'tahun_ajaran' => $tahunAjaran,
      'calon_siswa_id' => $calonSiswaId
    );
    return $this->db->get_where('pendaftaran', $data)->result();
  }

  public function findByTahunAjaran($tahunAjaran)
  {
    return $this->db->get_where('pendaftaran', array('tahun_ajaran' => $tahunAjaran))->result();
  }

  public function findByNoPendaftaranIn($noPendaftaranList)
  {
    $this->db->from('pendaftaran');
    return $this->db->where_in('no_pendaftaran', $noPendaftaranList)->get()->result();
  }

  public function findByCalonSiswaIdOrderByIdDesc($calonSiswaId)
  {
    $this->db->from('pendaftaran');
    $this->db->where('calon_siswa_id', $calonSiswaId);
    $this->db->order_by('id_pendaftaran', 'DESC');
    return $this->db->get()->result();
  }

  public function findByNoPendaftaran($noPendaftaran)
  {
    return $this->db->get_where('pendaftaran', array('no_pendaftaran' => $noPendaftaran))->result();
  }
}
