<?php 

class DaftarUlangModel extends CI_Model
{
  public function findByNoPendaftaran($noPendaftaran)
  {
    return $this->db->get_where('daftar_ulang', array('no_pendaftaran' => $noPendaftaran))->result();
  }

  public function findAll()
  {
    return $this->db->get('daftar_ulang')->result();
  }

  public function findByNisn($nisn)
  {
    return $this->db->get_where('daftar_ulang', array('nisn' => $nisn))->result();
  }

  public function findByNIkSiswa($nikSiswa)
  {
    return $this->db->get_where('daftar_ulang', array('nik_siswa' => $nikSiswa))->result();
  }

  public function findByStatusId($statusId)
  {
    return $this->db->get_where('daftar_ulang', array('status_id' => $statusId))->result();
  }

  public function save($data)
  {
    return $this->db->insert('daftar_ulang', $data);
  }

  public function update($data)
  {
    return $this->db->replace('daftar_ulang', $data);
  }

  public function findByNoPendaftaranIn($noPendaftaranList)
  {
    $this->db->from('daftar_ulang');
    return $this->db->where_in('no_pendaftaran', $noPendaftaranList)->get()->result();
  }

  public function findById($id)
  {
    return $this->db->get_where('daftar_ulang', array('id_daftar_ulang', $id))->result();
  }
}
