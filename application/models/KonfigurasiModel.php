<?php 

class KonfigurasiModel extends CI_Model
{
  public function findAll()
  {
    return $this->db->get('parameter')->result();
  }

  public function save($data)
  {
    return $this->db->insert('parameter', $data);
  }

  public function update($data)
  {
    return $this->db->replace('parameter', $data);
  }

  public function findByModule($module)
  {
    return $this->db->get_where('parameter', array('module' => $module))->result();
  }
}
