<?php 

class CalonSiswaModel extends CI_Model
{
  public function findAll()
  {
    return $this->db->get('calon_siswa')->result();
  }

  public function findByUsername($username)
  {
    return $this->db->get_where('calon_siswa', array('username' => $username))->result();
  }

  public function save($data)
  {
    return $this->db->insert('calon_siswa', $data);
  }

  public function findById($id)
  {
    return $this->db->get_where('calon_siswa', array('id_calon_siswa' => $id))->result();
  }

  public function update($data)
  {
    return $this->db->replace('calon_siswa', $data);
  }

  public function delete($id)
  {
    return $this->db->delete('calon_siswa', array('id_calon_siswa' => $id));
  }

  public function findByIdCalonSiswaAndKabupaten($id, $kabupaten)
  {
    $this->db->select('*');
    $this->db->from('calon_siswa');
    $this->db->where('id_calon_siswa', $id);
    return $this->db->like('kabupaten', $kabupaten)->get()->result();
  }

  public function findByIdIn($idList)
  {
    $this->db->from('calon_siswa');
    return $this->db->where_in('id_calon_siswa', $idList)->get()->result();
  }

  public function findByNisn($nisn)
  {
    return $this->db->get_where('calon_siswa', array('nisn' => $nisn))->result();
  }

  public function findByStatus($status)
  {
    return $this->db->get_where('calon_siswa', array('status' => $status))->result();
  }
}
