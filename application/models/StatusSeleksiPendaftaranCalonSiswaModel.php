<?php 

class StatusSeleksiPendaftaranCalonSiswaModel extends CI_Model
{
  public function findByAdminPendaftaranId($id)
  {
    return $this->db->get_where('status_seleksi_pendaftaran_calon_siswa', array('admin_pendaftaran_id' => $id))->result();
  }

  public function findByNoPendaftaran($noPendaftaran)
  {
    return $this->db->get_where('status_seleksi_pendaftaran_calon_siswa', array('no_pendaftaran' => $noPendaftaran))->result();
  }

  public function findAll()
  {
    return $this->db->get('status_seleksi_pendaftaran_calon_siswa')->result();
  }

  public function save($data)
  {
    return $this->db->insert('status_seleksi_pendaftaran_calon_siswa', $data);
  }

  public function update($data)
  {
    return $this->db->replace('status_seleksi_pendaftaran_calon_siswa', $data);
  }

  public function findByNoPendaftaranIn($noPendaftaranList)
  {
    $this->db->from('status_seleksi_pendaftaran_calon_siswa');
    return $this->db->where_in('no_pendaftaran', $noPendaftaranList)->get()->result();
  }

  public function findByStatusDiterimaAtauDitolak($statusSeleksi)
  {
    return $this->db->get_where('status_seleksi_pendaftaran_calon_siswa', array('status_diterima_atau_ditolak' => $statusSeleksi))->result();
  }

  public function findByStatusDiterimaAtauDitolakAndNoPendaftaran($statusSeleksi, $noPendaftaran)
  {
    $data = array(
      'status_diterima_atau_ditolak' => $statusSeleksi,
      'no_pendaftaran' => $noPendaftaran
    );
    return $this->db->get_where('status_seleksi_pendaftaran_calon_siswa', $data)->result();
  }
}
