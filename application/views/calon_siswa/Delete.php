<!-- modal -->
<div class="modal fade" id="modalDelete<?php echo $id ?>">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-danger">
        <h4 class="modal-title">Konfirmasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="needs-validation" action="<?php echo base_url('master/calonsiswa/delete') ?>" method="POST" novalidate>
        <div class="modal-body">
          <p>Anda ingin menghapus data ini ?</p>
          <input class="form-control" type="text" name="id" id="id" value="<?php echo $id ?>" hidden>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          <button type="submit" class="btn btn-primary">Yes</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
