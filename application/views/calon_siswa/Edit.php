<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $activeMenu);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Calon Siswa</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Master</a></li>
              <li class="breadcrumb-item active">Calon Siswa</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Ubah Data Calon Siswa</h3>
              </div>
              <!-- /.card-header -->
              <form id="formPerusashaan" method="post" action="<?php echo base_url('master/calonsiswa/store') ?>">
                <div class="card-body">
                  <input type="text" name="id" id="id" value="<?php echo $calonSiswa[0]->id_calon_siswa ?>" hidden>
                  <div class="form-group">
                    <label for="nisn">NISN</label>
                    <input type="text" class="form-control" id="nisn" name="nisn" placeholder="masukan nis" value="<?php echo $calonSiswa[0]->nisn ?>" maxlength="20" onkeypress="return event.charCode >= 48 && event.charCode <=57" required>
                  </div>
                  <div class="form-group">
                    <label for="namaLengkap">Nama lengkap</label>
                    <input type="text" class="form-control" id="namaLengkap" name="namaLengkap" placeholder="masukan nama lengkap" value="<?php echo $calonSiswa[0]->nama_lengkap ?>" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required>
                  </div>
                  <div class="form-group">
                    <label for="gender">Jeni Kelamin</label>
                    <div class="row">
                      <?php if ($calonSiswa[0]->gender == 'L') { ?>
                        <div class="col-sm-2">
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio1" name="gender" value="L" checked>
                            <label for="customRadio1" class="custom-control-label">Laki - Laki</label>
                          </div>
                        </div><?php
                      } else { ?>
                        <div class="col-sm-2">
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio1" name="gender" value="L">
                            <label for="customRadio1" class="custom-control-label">Laki - Laki</label>
                          </div>
                        </div><?php
                      }

                      if ($calonSiswa[0]->gender == 'P') {?>
                        <div class="col-sm-2">
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio2" name="gender" value="P" checked>
                            <label for="customRadio2" class="custom-control-label">Perempuan</label>
                          </div>
                        </div><?php
                      } else { ?>
                        <div class="col-sm-2">
                          <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" id="customRadio2" name="gender" value="P">
                            <label for="customRadio2" class="custom-control-label">Perempuan</label>
                          </div>
                        </div><?php
                      }?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="tempatLahir">Tempat Lahir</label>
                    <input type="text" class="form-control" id="tempatlahir" name="tempatLahir" placeholder="masukan tempat lahir" value="<?php echo $calonSiswa[0]->tempat_lahir ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="25" required>
                  </div>
                  <div class="form-group">
                    <label for="tanggalLahir">tanggal Lahir</label>
                    <input type="date" class="form-control" id="tanggalLahir" name="tanggalLahir" placeholder="Masukkan tanggal lahir" value="<?php echo $calonSiswa[0]->tanggal_lahir ?>" required>
                  </div>
                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <textarea class="form-control" rows="3" id="alamat" name="alamat" placeholder="masukan alamat" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="500" required><?php echo $calonSiswa[0]->alamat ?></textarea>
                  </div>
                  <div class="form-group">
                    <label for="kecamatan">Kecamatan</label>
                    <input type="text" class="form-control" id="kecamatan" name="kecamatan" placeholder="masukan kecamatan" value="<?php echo $calonSiswa[0]->kecamatan ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="50" required>
                  </div>
                  <div class="form-group">
                    <label for="kabupaten">Kabupaten</label>
                    <input type="text" class="form-control" id="kabupaten" name="kabupaten" placeholder="masukan kabupaten" value="<?php echo $calonSiswa[0]->kabupaten ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="50" required>
                  </div>
                  <div class="form-group">
                    <label for="agama">Agama</label>
                    <select class="form-control" id="agama" name="agama"><?php 
                      if ($calonSiswa[0]->agama == 'islam') { ?>
                        <option value="islam" selected>Islam</option><?php
                      } else { ?>
                        <option value="islam">Islam</option><?php
                      }
                      
                      if ($calonSiswa[0]->agama == 'protestan') { ?>
                        <option value="protestan" selected>Protestan</option><?php
                      } else { ?>
                        <option value="protestan">Protestan</option><?php
                      }
                      
                      if ($calonSiswa[0]->agama == 'katolik') { ?>
                        <option value="katolik" selected>Katolik</option><?php
                      } else { ?>
                        <option value="katolik">Katolik</option><?php
                      }
                      
                      if ($calonSiswa[0]->agama == 'hindu') { ?>
                        <option value="hindu" selected>Hindu</option><?php
                      } else { ?>
                        <option value="hindu">Hindu</option><?php
                      }
                      
                      if ($calonSiswa[0]->agama == 'buddha') { ?>
                        <option value="buddha" selected>Buddha</option><?php
                      } else { ?>
                        <option value="buddha">Buddha</option><?php
                      }
                      
                      if ($calonSiswa[0]->agama == 'khonghucu') { ?>
                        <option value="khonghucu" selected>Khonghucu</option><?php
                      } else { ?>
                        <option value="khonghucu">Khonghucu</option><?php
                      }?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="telepon">Telepon</label>
                    <input type="text" class="form-control" id="telepon" name="telepon" placeholder="masukan nomor telepon" value="<?php echo $calonSiswa[0]->no_hp ?>" minlength="12" maxlength="15" onkeypress="return event.charCode >= 48 && event.charCode <=57" required>
                  </div>
                  <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" placeholder="Masukkan username" value="<?php echo $calonSiswa[0]->username ?>" maxlength="50" required>
                  </div>
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan password" value="<?php echo $this->encryption->decrypt($calonSiswa[0]->password) ?>" maxlength="50" required>
                  </div>
                  <div class="form-group">
                    <label for="namaortu">Nama Ortu</label>
                    <input type="text" class="form-control" id="namaortu" name="namaOrtu" placeholder="masukan nama orang tua" value="<?php echo $calonSiswa[0]->nama_ortu ?>" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');
?>