<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $activeMenu);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Calon Siswa</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Master</a></li>
              <li class="breadcrumb-item active">Calon Siswa</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-2">
            <div class="card">
              <a href="<?php echo base_url('master/calonsiswa/add') ?>" type="button" class="btn btn-block btn-primary">Tambah</a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="dataTable" class="table table-bordered table-hover">
                  <thead>
                    <tr class="expandable-body">
                      <th style="text-align: center;" >No</th>
                      <th style="text-align: center; width: 60px;">NISN</th>
                      <th style="text-align: center; width: 120px;">Nama lengkap</th>
                      <th style="text-align: center; width: 30px;">Gender</th>
                      <th style="text-align: center; width: 60px;">Tempat Lahir</th>
                      <th style="text-align: center; width: 60px;">tanggal lahir</th>
                      <th style="text-align: center; width: 60px;">Agama</th>
                      <th style="text-align: center; width: 150px;">Alamat</th>
                      <th style="text-align: center; width: 60px;">Username</th>
                      <th style="text-align: center; width: 60px;">Telepon</th>
                      <th style="text-align: center; width: 120px;">nama Ortu</th>
                      <!-- <th style="width: 80px; text-align: center;">Aksi</th> -->
                    </tr>
                  </thead>
                  <tbody><?php $no = 1;
                    if (count($calonSiswaList) == 0) { ?>
                      <tr>
                        <td colspan="12" style="text-align: center;">Data Masih Kosong ...</td>
                      </tr><?php
                    } else {
                      foreach ($calonSiswaList as $data) {
                        $alamat = $data->alamat." kec. ".$data->kecamatan." kab. ".$data->kabupaten;
                        $calonSiswaId = array('id' => $data->id_calon_siswa); ?>
                        <tr>
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $data->nisn ?></td>
                          <td><?php echo $data->nama_lengkap ?></td>
                          <td><?php echo $data->gender ?></td>
                          <td><?php echo $data->tempat_lahir ?></td>
                          <td><?php echo $data->tanggal_lahir ?></td>
                          <td><?php echo $data->agama ?></td>
                          <td><?php echo $alamat ?></td>
                          <td><?php echo $data->username ?></td>
                          <td><?php echo $data->no_hp ?></td>
                          <td><?php echo $data->nama_ortu ?></td>
                          <!-- <td>
                            <div class="row">
                              <div class="col-6">
                                <a type="submit" href="<?php echo base_url('master/calonsiswa/get/'. $data->id_calon_siswa) ?>" class="btn btn-block bg-gradient-warning btn btn-m"><i class="nav-icon fas fa-pen"></i></a>
                              </div>
                              <div class="col-6">
                                <button type="button" class="btn btn-block bg-gradient-danger btn btn-m" data-toggle="modal" data-target="#modalDelete<?php echo $data->id_calon_siswa ?>"><i class="nav-icon fas fa-trash"></i></button>
                              </div>
                            </div>
                          </td> -->
                        </tr>
                        <?php $this->load->view('calon_siswa/delete', $calonSiswaId);
                      }
                    } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <button type="submit" class="btn btn-primary swalDefaultSuccess" style="visibility: hidden">success</button>
  <button type="submit" class="btn btn-primary swalDefaultError" style="visibility: hidden">failed</button>
  <button type="submit" class="btn btn-primary swalDefaultWarning" style="visibility: hidden">warning</button>

<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');

  if ($this->session->flashdata('success')) {
    echo $this->session->flashdata('success');
    unset($_SESSION['success']);
  }
  
  if ($this->session->flashdata('failed')) {
    echo $this->session->flashdata('failed');
    unset($_SESSION['failed']);
  }
  
  if ($this->session->flashdata('warning')) {
    echo $this->session->flashdata('warning');
    unset($_SESSION['warning']);
  }
?>


