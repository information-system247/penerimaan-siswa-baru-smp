<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $activeMenu);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Seleksi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Seleksi Pendaftaran</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Seleksi Pendaftaran Calon Siswa Baru</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="dataTable" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th style="text-align: center;">No</th>
                      <th style="text-align: center;">No. Pendaftaran</th>
                      <th style="text-align: center;">Nama lengkap</th>
                      <th style="text-align: center;">Tanggal Seleksi</th>
                      <th style="text-align: center;">Umur</th>
                      <th style="text-align: center;">Status Seleksi</th>
                    </tr>
                  </thead>
                  <tbody><?php $no = 1;
                    if (count($seleksiList) == 0) { ?>
                      <tr>
                        <td colspan="5" style="text-align: center;">Data Masih Kosong ...</td>
                      </tr><?php
                    } else {
                      foreach ($seleksiList as $data) { ?>
                        <tr>
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $data->no_pendaftaran ?></td>
                          <?php foreach ($pendaftaranList as $pendaftaran) {
                            if ($pendaftaran->no_pendaftaran == $data->no_pendaftaran) {
                              foreach ($calonSiswaList as $calonSiswa) {
                                if ($calonSiswa->id_calon_siswa == $pendaftaran->calon_siswa_id) { ?>
                                  <td><?php echo $calonSiswa->nama_lengkap ?></td><?php
                                }
                              }
                            }
                          }?>
                          <td><?php echo $data->tgl_diterima_atau_ditolak ?></td>
                          <td><?php echo $data->umur ?></td>
                          <td><?php echo $data->status_diterima_atau_ditolak ?></td>
                        </tr><?php
                      }
                    } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <button type="submit" class="btn btn-primary swalDefaultSuccess" style="visibility: hidden">success</button>
  <button type="submit" class="btn btn-primary swalDefaultError" style="visibility: hidden">failed</button>
  <button type="submit" class="btn btn-primary swalDefaultWarning" style="visibility: hidden">warning</button>

<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');

  if ($this->session->flashdata('success')) {
    echo $this->session->flashdata('success');
    unset($_SESSION['success']);
  }
  
  if ($this->session->flashdata('failed')) {
    echo $this->session->flashdata('failed');
    unset($_SESSION['failed']);
  }
  
  if ($this->session->flashdata('warning')) {
    echo $this->session->flashdata('warning');
    unset($_SESSION['warning']);
  }
?>
