<?php 
  $this->load->view("base_template/Head");
?>

<body class="hold-transition register-page">
<div class="register-box">
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="#" class="h1"><b>Pendaftaran </b>Siswa Baru</a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Register a new membership</p>

      <form action="<?php echo base_url('siswa/register/store') ?>" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" id="nisn" name="nisn" placeholder="masukan nisn" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="20" required>
          <div class="input-group-append"><div class="input-group-text"><span class="fas fa-user"></span></div></div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" id="namaLengkap" name="namaLengkap" placeholder="masukan nama lengkap" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required>
          <div class="input-group-append"><div class="input-group-text"><span class="fas fa-user"></span></div></div>
        </div>
        <div class="input-group mb-3">
          <div class="row">
            <div class="col-sm-6">
              <div class="custom-control custom-radio">
                <input class="custom-control-input" type="radio" id="customRadio1" name="gender" value="L" checked>
                <label for="customRadio1" class="custom-control-label">Laki - Laki</label>
              </div>
            </div>
            <div class="col-sm-2">
              <div class="custom-control custom-radio">
                <input class="custom-control-input" type="radio" id="customRadio2" name="gender" value="P">
                <label for="customRadio2" class="custom-control-label">Perempuan</label>
              </div>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" id="tempatlahir" name="tempatLahir" placeholder="masukan tempat lahir" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="50" required>
          <div class="input-group-append"><div class="input-group-text"><span class="fas fa-user"></span></div></div>
        </div>
        <div class="input-group mb-3">
          <input type="date" class="form-control" id="tanggalLahir" name="tanggalLahir" required>
          <div class="input-group-append"><div class="input-group-text"><span class="fas fa-user"></span></div></div>
        </div>
        <div class="input-group mb-3">
        <textarea class="form-control" rows="3" id="alamat" name="alamat" placeholder="masukan alamat" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="500" required></textarea>
          <div class="input-group-append"><div class="input-group-text"><span class="fas fa-user"></span></div></div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" id="kecamatan" name="kecamatan" placeholder="masukan kecamatan" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="50" required>
          <div class="input-group-append"><div class="input-group-text"><span class="fas fa-user"></span></div></div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" id="kabupaten" name="kabupaten" placeholder="masukan kabupaten" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="50" required>
          <div class="input-group-append"><div class="input-group-text"><span class="fas fa-user"></span></div></div>
        </div>
        <div class="input-group mb-3">
          <select class="form-control" id="agama" name="agama">
            <option value="islam">Islam</option>
            <option value="protestan">Protestan</option>
            <option value="katolik">Katolik</option>
            <option value="hindu">Hindu</option>
            <option value="buddha">Buddha</option>
            <option value="khonghucu">Khonghucu</option>
          </select>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" id="telepon" name="telepon" placeholder="masukan nomor telepon" minlength="12" maxlength="15" onkeypress="return event.charCode >= 48 && event.charCode <=57" required>
          <div class="input-group-append"><div class="input-group-text"><span class="fas fa-phone"></span></div></div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" id="namaortu" name="namaOrtu" placeholder="masukan nama orang tua" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required>
          <div class="input-group-append"><div class="input-group-text"><span class="fas fa-user"></span></div></div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" id="username" name="username" placeholder="Masukkan username" maxlength="50" required>
          <div class="input-group-append"><div class="input-group-text"><span class="fas fa-user"></span></div></div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan password" maxlength="50" required>
          <div class="input-group-append"><div class="input-group-text"><span class="fas fa-lock"></span></div></div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" id="password2" name="password2" placeholder="Masukkan ulang password" maxlength="50" required>
          <div class="input-group-append"><div class="input-group-text"><span class="fas fa-lock"></span></div></div>
        </div>
        <div class="row">
          <div class="col-8"></div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <div class="social-auth-links text-center">
        <a href="<?php echo base_url('siswa/login') ?>" class="btn btn-block btn-danger">
          <i class="fas fa-lock mr-2"></i>Saya sudah memiliki akun
        </a>
      </div>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<button type="submit" class="btn btn-primary swalDefaultError" style="visibility: hidden">failed</button>

<?php
  $this->load->view("base_template/Script");

  if ($this->session->flashdata('failed')) {
    echo $this->session->flashdata('failed');
    unset($_SESSION['failed']);
  }
?>
