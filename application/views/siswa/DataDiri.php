<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $activeMenu);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Data Diri</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Data Diri</h3>
              </div>
              <!-- /.card-header -->
              <form id="formPerusashaan" method="post" action="<?php echo base_url('siswa/datadiri/store') ?>">
                <div class="card-body">
                  <input type="text" name="id" id="id" value="<?php echo $calonSiswa[0]->id_calon_siswa ?>" hidden>
                  <div class="form-group">
                    <label for="nisn">NISN</label>
                    <?php if ($validUpdateData) {?>
                      <input type="text" class="form-control" id="nisn" name="nisn" placeholder="masukan nis" value="<?php echo $calonSiswa[0]->nisn ?>" maxlength="20" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="nisn" name="nisn" placeholder="masukan nis" value="<?php echo $calonSiswa[0]->nisn ?>" maxlength="20" onkeypress="return event.charCode >= 48 && event.charCode <=57" disabled><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="namaLengkap">Nama lengkap</label>
                    <?php if ($validUpdateData) {?>
                      <input type="text" class="form-control" id="namaLengkap" name="namaLengkap" placeholder="masukan nama lengkap" value="<?php echo $calonSiswa[0]->nama_lengkap ?>" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="namaLengkap" name="namaLengkap" placeholder="masukan nama lengkap" value="<?php echo $calonSiswa[0]->nama_lengkap ?>" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" disabled><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="gender">Jeni Kelamin</label>
                    <?php if ($validUpdateData) {?>
                      <div class="row">
                        <?php if ($calonSiswa[0]->gender == 'L') { ?>
                          <div class="col-sm-2">
                            <div class="custom-control custom-radio">
                              <input class="custom-control-input" type="radio" id="customRadio1" name="gender" value="L" checked>
                              <label for="customRadio1" class="custom-control-label">Laki - Laki</label>
                            </div>
                          </div><?php
                        } else { ?>
                          <div class="col-sm-2">
                            <div class="custom-control custom-radio">
                              <input class="custom-control-input" type="radio" id="customRadio1" name="gender" value="L">
                              <label for="customRadio1" class="custom-control-label">Laki - Laki</label>
                            </div>
                          </div><?php
                        }

                        if ($calonSiswa[0]->gender == 'P') {?>
                          <div class="col-sm-2">
                            <div class="custom-control custom-radio">
                              <input class="custom-control-input" type="radio" id="customRadio2" name="gender" value="P" checked>
                              <label for="customRadio2" class="custom-control-label">Perempuan</label>
                            </div>
                          </div><?php
                        } else { ?>
                          <div class="col-sm-2">
                            <div class="custom-control custom-radio">
                              <input class="custom-control-input" type="radio" id="customRadio2" name="gender" value="P">
                              <label for="customRadio2" class="custom-control-label">Perempuan</label>
                            </div>
                          </div><?php
                        }?>
                      </div><?php
                    } else {?>
                      <input type="text" class="form-control" id="namaLengkap" name="namaLengkap" placeholder="masukan nama lengkap" value="<?php echo $calonSiswa[0]->gender ?>" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" disabled><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="tempatLahir">Tempat Lahir</label>
                    <?php if ($validUpdateData) {?>
                      <input type="text" class="form-control" id="tempatlahir" name="tempatLahir" placeholder="masukan tempat lahir" value="<?php echo $calonSiswa[0]->tempat_lahir ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="50" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="tempatlahir" name="tempatLahir" placeholder="masukan tempat lahir" value="<?php echo $calonSiswa[0]->tempat_lahir ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="50" disabled><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="tanggalLahir">tanggal Lahir</label>
                    <?php if ($validUpdateData) {?>
                      <input type="date" class="form-control" id="tanggalLahir" name="tanggalLahir" placeholder="Masukkan tanggal lahir" value="<?php echo $calonSiswa[0]->tanggal_lahir ?>" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="tanggalLahir" name="tanggalLahir" placeholder="Masukkan tanggal lahir" value="<?php echo $calonSiswa[0]->tanggal_lahir ?>" disabled><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <?php if ($validUpdateData) {?>
                      <textarea class="form-control" rows="3" id="alamat" name="alamat" placeholder="masukan alamat" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 44 || event.charCode == 46" maxlength="500" required><?php echo $calonSiswa[0]->alamat ?></textarea><?php
                    } else {?>
                      <textarea class="form-control" rows="3" id="alamat" name="alamat" placeholder="masukan alamat" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 44 || event.charCode == 46" maxlength="500" disabled><?php echo $calonSiswa[0]->alamat ?></textarea><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="kecamatan">Kecamatan</label>
                    <?php if ($validUpdateData) {?>
                      <input type="text" class="form-control" id="kecamatan" name="kecamatan" placeholder="masukan kecamatan" value="<?php echo $calonSiswa[0]->kecamatan ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="50" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="kecamatan" name="kecamatan" placeholder="masukan kecamatan" value="<?php echo $calonSiswa[0]->kecamatan ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="50" disabled><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="kabupaten">Kabupaten</label>
                    <?php if ($validUpdateData) {?>
                      <input type="text" class="form-control" id="kabupaten" name="kabupaten" placeholder="masukan kabupaten" value="<?php echo $calonSiswa[0]->kabupaten ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="50" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="kabupaten" name="kabupaten" placeholder="masukan kabupaten" value="<?php echo $calonSiswa[0]->kabupaten ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="50" disabled><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="agama">Agama</label>
                    <?php if ($validUpdateData) {?>
                      <select class="form-control" id="agama" name="agama"><?php 
                        if ($calonSiswa[0]->agama == 'islam') { ?>
                          <option value="islam" selected>Islam</option><?php
                        } else { ?>
                          <option value="islam">Islam</option><?php
                        }
                        
                        if ($calonSiswa[0]->agama == 'protestan') { ?>
                          <option value="protestan" selected>Protestan</option><?php
                        } else { ?>
                          <option value="protestan">Protestan</option><?php
                        }
                        
                        if ($calonSiswa[0]->agama == 'katolik') { ?>
                          <option value="katolik" selected>Katolik</option><?php
                        } else { ?>
                          <option value="katolik">Katolik</option><?php
                        }
                        
                        if ($calonSiswa[0]->agama == 'hindu') { ?>
                          <option value="hindu" selected>Hindu</option><?php
                        } else { ?>
                          <option value="hindu">Hindu</option><?php
                        }
                        
                        if ($calonSiswa[0]->agama == 'buddha') { ?>
                          <option value="buddha" selected>Buddha</option><?php
                        } else { ?>
                          <option value="buddha">Buddha</option><?php
                        }
                        
                        if ($calonSiswa[0]->agama == 'khonghucu') { ?>
                          <option value="khonghucu" selected>Khonghucu</option><?php
                        } else { ?>
                          <option value="khonghucu">Khonghucu</option><?php
                        }?>
                      </select><?php
                    } else {?>
                      <input type="text" class="form-control" id="agama" name="agama" placeholder="masukan agama" value="<?php echo $calonSiswa[0]->agama ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="25" disabled><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="telepon">Telepon</label>
                    <?php if ($validUpdateData) {?>
                      <input type="text" class="form-control" id="telepon" name="telepon" placeholder="masukan nomor telepon" value="<?php echo $calonSiswa[0]->no_hp ?>" minlength="12" maxlength="15" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="telepon" name="telepon" placeholder="masukan nomor telepon" value="<?php echo $calonSiswa[0]->no_hp ?>" minlength="12" maxlength="15" onkeypress="return event.charCode >= 48 && event.charCode <=57" disabled><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="namaortu">Nama Ortu</label>
                    <?php if ($validUpdateData) {?>
                      <input type="text" class="form-control" id="namaortu" name="namaOrtu" placeholder="masukan nama orang tua" value="<?php echo $calonSiswa[0]->nama_ortu ?>" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="namaortu" name="namaOrtu" placeholder="masukan nama orang tua" value="<?php echo $calonSiswa[0]->nama_ortu ?>" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" disabled><?php
                    }?>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <?php if ($validUpdateData) {?>
                    <button type="submit" class="btn btn-primary">Simpan</button><?php
                  } else {?>
                    <button type="submit" class="btn btn-primary" disabled>Simpan</button><?php
                  }?>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');
?>