<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $activeMenu);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Beranda</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Beranda</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    



    <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <!-- /.card-header -->
          <div class="card-body">
          <label style="font-size: x-large;">Jadwal Pendaftaran dan Seleksi</label><br><br>
            <!-- Timelime example  -->
            <div class="row">
              <div class="col-md-10">
                <!-- The time line -->
                <div class="timeline">
                  <!-- timeline time label -->
                  <div class="time-label">
                    <span class="bg-green">Batas Pendaftaran : <?php echo $batasPendaftaran ?> </span>
                  </div>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <div>
                    <i class="fas fa-user bg-blue"></i>
                    <div class="timeline-item">
                      <h3 class="timeline-header"><a href="<?php echo base_url('siswa/datadiri') ?>">Lengkapi Data Diri</a></h3>
                      <div class="timeline-body">
                        Lengkapi data diri Anda, kemudian cek kembali dan pastikan data diri telah sesuai.
                      </div>
                    </div>
                  </div>
                  <!-- END timeline item -->
                  <!-- timeline item -->
                  <div>
                    <i class="fas fa-upload bg-blue"></i>
                    <div class="timeline-item">
                    <h3 class="timeline-header"><a href="<?php echo base_url('siswa/pendaftaran') ?>">Lengkapi Berkas</a></h3>
                    <h3 class="timeline-header no-border">Unggah berkas - berkas yang dibutuhkan meliputi scan kartu keluarga, ijazah, SKHUN, akta kelahiran dan piagam penghargaan (optional) dalam bentuk format .zip</h3>
                    </div>
                  </div>
                  <!-- END timeline item -->
                  <!-- timeline item -->
                  <?php if (!empty($pendaftaranOptional)) { ?>
                    <div>
                      <i class="fas fa-check bg-blue"></i>
                      <div class="timeline-item">
                      <h3 class="timeline-header no-border">Data Anda telah terkirim, silahkan menunggu pengumuman seleksi pada tanggal yang sudah ditetapkan.</h3>
                      </div>
                    </div><br><?php
                  }?>
                  <!-- END timeline item -->
                  <?php
                  if(!empty($statusSeleksiOptional2) && $validPendingView) { ?>
                    <div>
                      <i class="fas fa-bell bg-red"></i>
                      <div class="timeline-item">
                        <h3 class="timeline-header bg-red no-border">Terdapat kesalahan pada berkas pendaftaran anda segera kirim kembali paling lambat tanggal 
                          <?php echo $batasRevisi; ?> : <?php echo $statusSeleksiOptional2[0]->catatan ;?>
                        </h3>
                      </div>
                    </div><br><?php 
                  } ?>
                  <!-- END timeline item -->
                  <!-- timeline time label -->
                  <div class="time-label">
                    <?php $colorPengumumanSeleksi = "gray";
                    if (!empty($statusSeleksiOptional) && $pengumumanSeleksi) {
                      $colorPengumumanSeleksi = "green";
                    }?>
                    <span class="bg-<?php echo $colorPengumumanSeleksi ?>">Pengumuman Seleksi : <?php echo $tanggalPengumumanSeleksi ?></span>
                  </div>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <?php if (!empty($statusSeleksiOptional) && $pengumumanSeleksi) {
                    if ($statusSeleksiOptional[0]->status_diterima_atau_ditolak == 'TERIMA') {?>
                      <div>
                        <i class="fas fa-bell bg-blue"></i>
                        <div class="timeline-item">
                        <h3 class="timeline-header bg-blue no-border">Selamat Anda dinyatakan lulus seleksi, silahkan lakukan daftar ulang pada periode yang telah ditetapkan.</h3>
                        </div>
                      </div><?php
                    } else { ?>
                      <div>
                        <i class="fas fa-bell bg-red"></i>
                        <div class="timeline-item">
                        <h3 class="timeline-header bg-red no-border">Maaf Anda dinyatakan tidak lulus seleksi  dikarenakan <?php echo $statusSeleksiOptional[0]->catatan ?>.</h3>
                        </div>
                      </div><br><?php
                    }
                  } ?>
                  <!-- END timeline item -->
                  <!-- timeline time label -->
                  <div class="time-label">
                    <?php $colorDaftarUlang = "gray";
                    if (!empty($daftarUlangOptional) || $validDaftarUlang) {
                      $colorDaftarUlang = "green";
                    }?>
                    <span class="bg-<?php echo $colorDaftarUlang ?>">Batas Daftar Ulang : <?php echo $batasDaftarUlang ?></span>
                  </div>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <?php if(!empty($daftarUlangOptional) && $validDaftarUlang) { ?>
                    <div>
                      <i class="fas fa-upload bg-red"></i>
                      <div class="timeline-item">
                      <h3 class="timeline-header no-border">Terdapat kesalahan pada berkas daftar ulang anda segera kirim kembali paling lambat tanggal <?php echo $batasDaftarUlang; ?> : <?php echo $daftarUlangOptional[0]->catatan ?></h3>
                      </div>
                    </div><br><?php
                  }?>
                  <!-- END timeline item -->
                  <div>
                    <i class="fas fa-clock bg-gray"></i>
                  </div>
                </div>
              </div>
              <!-- /.col -->
            </div>
          <!-- /.timeline -->
          <!-- <center><label style="font-size: xx-large;">SMP Negeri 091410 Sarimatondang</label></center>
          <center><label style="font-size: large;">Sarimatondang, Kec. Sidamanik, Kab. Simalungun, Sumatra Utara</label></center><br> -->
            <!-- <div class="row">
              <div class="col-12">
                <h4>
                  Visi:
                </h4>
                <div class="row invoice-info">
                  <div class="col-sm-12 invoice-col">
                    Unggul dalam prestasi, beriman dan taqwa, cerdas, trampil dan berkarakter serta berwawasan lingkungan
                  </div>
                </div>
              </div> -->
              <!-- /.col -->
            <!-- </div><br>
            <div class="row">
              <div class="col-12">
                <h4>
                  Misi:
                </h4>
                <div class="row invoice-info">
                  <div class="col-sm-12 invoice-col">
                    1. Menyelenggarakan pendidikan dasar berbasis Teknologi Informasi dan Komunikasi (TIK) sehingga menghasilkan lulusan yang memiliki keunggulan akademik di tingkat internasional, berintegritas tinggi dan berwawasan kebangsaan dan budaya Indonesia.
                  </div>
                  <div class="col-sm-12 invoice-col">
                    2. Memberdayakan potensi kecerdasan intelektual, kecerdasan emosional, kecerdasan sosial, dan keceerdasan religius siswa.
                  </div>
                  <div class="col-sm-12 invoice-col">
                    3. Mendidik siswa yang menjunjung tinggi etika moral intelektual dan memiliki daya saing pada tingkat internasional.
                  </div>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<?php
  $this->load->view('base_template/script');

  if ($this->session->flashdata('success')) {
    echo $this->session->flashdata('success');
    unset($_SESSION['success']);
  }
?>