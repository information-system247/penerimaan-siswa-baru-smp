<?php $this->load->view('base_template/headwebsite'); ?>

<body data-spy="scroll" data-target=".navbar-default" data-offset="100">
<!--Preloader-->
<div id="preloader">
  <div id="pre-status">
    <div class="preload-placeholder"></div>
  </div>
</div>

<!--Modal Login-->
<!-- <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <div class="row">
          <div class="col-md-7">
            <h4 class="modal-title w-100 text-right font-weight-bold"><strong>Login</strong></h4>
          </div>
          <div class="col-md-5">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="text-right" aria-hidden="true">&times;</span></button>
            </button>
          </div>
        </div>
      </div>
      <div class="modal-body mx-3">
        <div class="md-form mb-5">
          <input type="email" id="defaultForm-email" class="form-control validate">
          <label data-error="wrong" data-success="right" for="defaultForm-username">Username </label>
        </div>
        <div class="md-form mb-4">
          <input type="password" id="defaultForm-pass" class="form-control validate">
          <label data-error="wrong" data-success="right" for="defaultForm-pass">Password</label>
        </div>
      </div><br>
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-default bg-info">Login</button>
      </div>
    </div>
  </div>
</div> -->

<!--Navigation-->
<header id="menu">
  <div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a class="navbar-brand" href="#menu"><img src="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/images/Logo/01.png" alt=""></a> </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="active"><a class="scroll" href="#menu">Beranda</a></li>
            <li><a class="scroll" href="#about">Profil Sekolah</a></li>
            <li><a class="scroll" href="#pricing">PPDB 2021/2022</a></li>
            <li><a class="scroll" href="#blog">Struktur Organisasi</a></li>
            <li><a class="scroll" href="#contact">Kontak</a></li>
            <li><a class="scroll" href="<?php echo base_url('siswa/register') ?>">Daftar</a></li>
            <!-- <li><a class="scroll" href="#" data-toggle="modal" data-target="#modalLoginForm">Login</a></li> -->
            <li><a class="scroll" href="<?php echo base_url('siswa/login') ?>">Login</a></li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
    </div>
  </div>
</header>
<!--Slider-Start-->
<section id="slider">
  <div id="home-carousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="item active" style="background-image:url(<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/images/sekolah1.png)">
        <div class="carousel-caption container">
          <div class="row">
            <div class="col-md-7 col-sm-12 col-xs-12">
              <h1>Selamat Datang di</h1>
              <h2>SMP NEGERI 1 DOLOK PARDAMEAN</h2>
            </div>
          </div>
        </div>
      </div>
      <div class="item" style="background-image:url(<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/images/sekolah2.png)">
        <div class="carousel-caption container">
          <div class="row">
            <div class="col-md-7 col-sm-12 col-xs-12">
              <h1>Pendaftaran Peserta Didik Baru 2021/2022</h1>
              <h2>1 Januari - 2 Februari 2022</h2>
            </div>
          </div>
        </div>
      </div>
      <a class="home-carousel-left" href="#home-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a> <a class="home-carousel-right" href="#home-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a> </div>
  </div>
  <!--/#home-carousel-->
</section>
<!--About-Section-Start-->
<section id="about">
  <div class="container">
    <div class="col-md-8 col-md-offset-2">
      <div class="heading">
        <h2>PROFIL SEKOLAH</h2>
        <div class="line"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 ab-sec">
        <div class="col-md-6">
          <h3 class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">Visi Sekolah</h3>
          <p>Unggul dalam prestasi, beriman dan taqwa, cerdas, trampil dan berkarakter serta berwawasan lingkungan.</p><br>
          <h3 class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">Misi Sekolah</h3>
          <p>
            <ul>
              <li>Menyelenggarakan pendidikan berbasis Teknologi Informasi dan Komunikasi (TIK) sehingga menghasilkan lulusan yang memiliki keunggulan akademik di tingkat internasional, berintegritas tinggi dan berwawasan kebangsaan dan budaya indonesia.</li> 
              <li>Memberdayakan potensi kecerdasan intelektual, kecerdasan emosional, kecerdasan sosial, dan kecerdasan religius siswa.</li>
              <li>Mendidik siswa yang menjunjung tinggi etika moral intelektual dan memiliki daya saing pada tingkat internasional.</li>
              <li>Mengembangkan pengetahuan dan ketrampilan siswa melalui kegiatan inovasi</li>
              <li>Membentuk siswa berkarakter religius, nasionalis, mandiri, gotong royong, dan berintegritas melalui kegiatan akademik dan non akademik.</li>
            </ul>
          </p>
          <br>
        </div>
        <div class="col-md-6 ab-sec-img wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
          <img src="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/images/tutwuri.png" class="img-responsive" alt="" width="400" height="500">
        </div>
      </div>
    </div>
  </div>
</section>
<!--Pricing-Section-Start-->
<section id="pricing">
  <div class="container">
    <div class="col-md-8 col-md-offset-2">
      <div class="heading">
        <h2>PPDB <span>2021/2022</span></h2>
        <div class="line"></div>
        <p>Tata cara pendaftaran peserta didik baru tahun ajaran 2021/2022 di SMP NEGERI 1 DOLOK PARDAMEAN adalah sebagai berikut :</p>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <div class="wow zoomIn" data-wow-duration="400ms" data-wow-delay="0ms">
          <ul class="pricing">
            <li class="plan-header">
              <div class="price-duration">
                <div class="price"> Step 1 </div>
              </div>
              <div class="plan-name"> Login Akun </div>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="wow zoomIn" data-wow-duration="400ms" data-wow-delay="200ms">
          <ul class="pricing">
            <li class="plan-header">
              <div class="price-duration">
                <div class="price"> Step 2 </div>
              </div>
              <div class="plan-name"> Lengkapi Data </div>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="wow zoomIn" data-wow-duration="400ms" data-wow-delay="400ms">
          <ul class="pricing">
            <li class="plan-header">
              <div class="price-duration">
                <div class="price"> Step 3 </div>
              </div>
              <div class="plan-name"> Unggah File Pendukung </div>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="wow zoomIn" data-wow-duration="400ms" data-wow-delay="600ms">
          <ul class="pricing">
            <li class="plan-header">
              <div class="price-duration">
                <div class="price"> Step 4 </div>
              </div>
              <div class="plan-name"> Tunggu Proses Seleksi </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

<!--Blog-Section-Start-->
<section id="blog">
  <div class="container">
    <div class="col-md-8 col-md-offset-2">
      <div class="heading">
        <h2>STRUKTUR ORGANISASI</h2>
        <div class="line"></div>
        <p>Berikut struktur organisasi di SMP Negeri 1 Dolok Pardamean</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <img src="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/images/struktur_organisasi.png" class="img-responsive" alt="">
      </div>
    </div>
  </div>
</section>
<!--Contact-Section-Start-->
<section id="contact">
  <div class="container">
    <div class="col-md-8 col-md-offset-2">
      <div class="heading">
        <h2>KONTAK</h2>
        <div class="line"></div>
      </div><br><br>
    </div>
    <div class="text-center">
      <div class="col-md-6 col-sm-6 contact-sec-1">
        <h4>Info Kontak</h4>
        <ul class="contact-form">
          <li><i class="fa fa-map-marker"></i>
            <h6><strong>Alamat: </strong>Jalan Sidamanik No.21 Sipintuangin, Pabrik Sabungan, Kec. Dolok Pardamean, Kab. Simalungun Prov. Sumatera Utara</h6>
          </li>
          <li><i class="fa fa-envelope"></i>
            <h6><strong>Email:</strong> <a href="#">smpn1dolokpardamean@gmail.com</a></h6>
          </li>
          <li><i class="fa fa-phone"></i>
            <h6><strong>Telp:</strong> 0812-7570-9527</h6>
          </li>
          <li><i class="fa fa-facebook"></i>
            <h6><strong>Facebook:</strong> <a href="#">Smp Negerii Dolok Pardamean </a> </h6>
          </li>
        </ul>
      </div>
      <div class="col-md-6 col-sm-6">
          <!--Google map-->
          <div id="map-container-google-1" class="z-depth-1-half map-container" style="height: 400px">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d127519.95074069235!2d98.73918078939883!3d2.8165793988988486!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3031be722d3845f9%3A0xe1afa4218a0b6155!2sSMP%20Negeri%201%20Dolok%20Pardamean!5e0!3m2!1sid!2sid!4v1657072771130!5m2!1sid!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
          </div>
      </div>
    </div>
  </div>
</section>
<footer id="footer-down">
  <h2>Ikuti Kami</h2>
  <ul class="social-icon">
    <li class="facebook hvr-pulse"><a href="https://www.facebook.com/smpnegeri.dolokpardamean"><i class="fa fa-facebook-f"></i></a></li>
  </ul>
</footer>

<?php $this->load->view('base_template/scriptwebsite'); ?>