<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $activeMenu);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Daftar Ulang</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Daftar Ulang</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12"> <?php 
              $color = 'primary';
              $text = 'Data Daftar Ulang';
              $isDaftar = false;
              if (!empty($daftarUlang)) {
                $color = 'success';
                $text = 'Terimakasih sudah melakukan daftar ulang. Silahkan datang ke sekolah untuk info lebih lanjut.';
                $isDaftar = true;
              } ?>
            <div class="card card-<?php echo $color ?>">
              <div class="card-header">
                <h3 class="card-title"><?php echo $text ?></h3>
              </div>
              <!-- /.card-header -->
              <form id="formPendaftaran" method="post" action="<?php echo base_url('siswa/daftarulang/store') ?>" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="form-group">
                    <label for="noPendaftaran">Nomor Pendaftaran</label>
                    <input type="text" class="form-control" id="noPendaftaran" name="noPendaftaran" value="<?php echo $pendaftaran[0]->no_pendaftaran ?>" disabled>
                  </div>
                  <div class="form-group">
                    <label for="namaLengkap">Nama Lengkap</label>
                    <?php if ($isDaftar) { ?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->nama_lengkap ?>" id="namaLengkap" name="namaLengkap" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required><?php
                    } else { ?>
                      <input type="text" class="form-control" id="namaLengkap" name="namaLengkap" value="<?php echo $this->session->userdata('nama') ?>" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="gender">Jeni Kelamin</label>
                    <?php if ($isDaftar) { ?>
                      <div class="row">
                        <?php if ($daftarUlang[0]->gender == 'L') { ?>
                          <div class="col-sm-2">
                            <div class="custom-control custom-radio">
                              <input class="custom-control-input" type="radio" id="customRadio1" name="gender" value="L" checked>
                              <label for="customRadio1" class="custom-control-label">Laki - Laki</label>
                            </div>
                          </div><?php
                        } else { ?>
                          <div class="col-sm-2">
                            <div class="custom-control custom-radio">
                              <input class="custom-control-input" type="radio" id="customRadio1" name="gender" value="L">
                              <label for="customRadio1" class="custom-control-label">Laki - Laki</label>
                            </div>
                          </div><?php
                        }

                        if ($daftarUlang[0]->gender == 'P') {?>
                          <div class="col-sm-2">
                            <div class="custom-control custom-radio">
                              <input class="custom-control-input" type="radio" id="customRadio2" name="gender" value="P" checked>
                              <label for="customRadio2" class="custom-control-label">Perempuan</label>
                            </div>
                          </div><?php
                        } else { ?>
                          <div class="col-sm-2">
                            <div class="custom-control custom-radio">
                              <input class="custom-control-input" type="radio" id="customRadio2" name="gender" value="P">
                              <label for="customRadio2" class="custom-control-label">Perempuan</label>
                            </div>
                          </div><?php
                        }?>
                      </div><?php
                    } else { ?>
                      <div class="row">
                        <?php if ($this->session->userdata('gender') == 'L') { ?>
                          <div class="col-sm-2">
                            <div class="custom-control custom-radio">
                              <input class="custom-control-input" type="radio" id="customRadio1" name="gender" value="L" checked>
                              <label for="customRadio1" class="custom-control-label">Laki - Laki</label>
                            </div>
                          </div><?php
                        } else { ?>
                          <div class="col-sm-2">
                            <div class="custom-control custom-radio">
                              <input class="custom-control-input" type="radio" id="customRadio1" name="gender" value="L">
                              <label for="customRadio1" class="custom-control-label">Laki - Laki</label>
                            </div>
                          </div><?php
                        }

                        if ($this->session->userdata('gender') == 'P') {?>
                          <div class="col-sm-2">
                            <div class="custom-control custom-radio">
                              <input class="custom-control-input" type="radio" id="customRadio2" name="gender" value="P" checked>
                              <label for="customRadio2" class="custom-control-label">Perempuan</label>
                            </div>
                          </div><?php
                        } else { ?>
                          <div class="col-sm-2">
                            <div class="custom-control custom-radio">
                              <input class="custom-control-input" type="radio" id="customRadio2" name="gender" value="P">
                              <label for="customRadio2" class="custom-control-label">Perempuan</label>
                            </div>
                          </div><?php
                        }?>
                      </div><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="nisn">NISN</label>
                    <input type="text" class="form-control" value="<?php echo $calonSiswa[0]->nisn ?>" disabled>
                  </div>
                  <div class="form-group">
                    <label for="nikSiswa">NIK Siswa</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->nik_siswa ?>" id="nikSiswa" name="nikSiswa" placeholder="masukan nik siswa" minlength="20" maxlength="20" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="nikSiswa" name="nikSiswa" placeholder="masukan nik siswa" minlength="20" maxlength="20" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="tempatLahirSiswa">Tempat Lahir Siswa</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->tmp_lhr_siswa ?>" id="tempatLahirSiswa" name="tempatLahirSiswa" placeholder="masukan tempat lahir Siswa" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="25" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="tempatLahirSiswa" name="tempatLahirSiswa" value="<?php echo $this->session->userdata('tempatLahir') ?>" placeholder="masukan tempat lahir Siswa" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="25" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="tanggalLahirSiswa">tanggal Lahir Siswa</label>
                    <?php if ($isDaftar) {?>
                      <input type="date" class="form-control" value="<?php echo $daftarUlang[0]->tgl_lhr_siswa ?>" id="tanggalLahirSiswa" name="tanggalLahirSiswa" placeholder="Masukkan tanggal lahir" required><?php
                    } else {?>
                      <input type="date" class="form-control" id="tanggalLahirSiswa" name="tanggalLahirSiswa" value="<?php echo $this->session->userdata('tanggalLahir') ?>" placeholder="Masukkan tanggal lahir" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="agama">Agama</label>
                    <?php if ($isDaftar) {?>
                      <select class="form-control" id="agama" name="agama"><?php 
                        if ($daftarUlang[0]->agama == 'islam') { ?>
                          <option value="islam" selected>Islam</option><?php
                        } else { ?>
                          <option value="islam">Islam</option><?php
                        }
                        
                        if ($daftarUlang[0]->agama == 'protestan') { ?>
                          <option value="protestan" selected>Protestan</option><?php
                        } else { ?>
                          <option value="protestan">Protestan</option><?php
                        }
                        
                        if ($daftarUlang[0]->agama == 'katolik') { ?>
                          <option value="katolik" selected>Katolik</option><?php
                        } else { ?>
                          <option value="katolik">Katolik</option><?php
                        }
                        
                        if ($daftarUlang[0]->agama == 'hindu') { ?>
                          <option value="hindu" selected>Hindu</option><?php
                        } else { ?>
                          <option value="hindu">Hindu</option><?php
                        }
                        
                        if ($daftarUlang[0]->agama == 'buddha') { ?>
                          <option value="buddha" selected>Buddha</option><?php
                        } else { ?>
                          <option value="buddha">Buddha</option><?php
                        }
                        
                        if ($daftarUlang[0]->agama == 'khonghucu') { ?>
                          <option value="khonghucu" selected>Khonghucu</option><?php
                        } else { ?>
                          <option value="khonghucu">Khonghucu</option><?php
                        }?>
                      </select><?php
                    } else {?>
                      <select class="form-control" id="agama" name="agama"><?php 
                        if ($calonSiswa[0]->agama == 'islam') { ?>
                          <option value="islam" selected>Islam</option><?php
                        } else { ?>
                          <option value="islam">Islam</option><?php
                        }
                        
                        if ($calonSiswa[0]->agama == 'protestan') { ?>
                          <option value="protestan" selected>Protestan</option><?php
                        } else { ?>
                          <option value="protestan">Protestan</option><?php
                        }
                        
                        if ($calonSiswa[0]->agama == 'katolik') { ?>
                          <option value="katolik" selected>Katolik</option><?php
                        } else { ?>
                          <option value="katolik">Katolik</option><?php
                        }
                        
                        if ($calonSiswa[0]->agama == 'hindu') { ?>
                          <option value="hindu" selected>Hindu</option><?php
                        } else { ?>
                          <option value="hindu">Hindu</option><?php
                        }
                        
                        if ($calonSiswa[0]->agama == 'buddha') { ?>
                          <option value="buddha" selected>Buddha</option><?php
                        } else { ?>
                          <option value="buddha">Buddha</option><?php
                        }
                        
                        if ($calonSiswa[0]->agama == 'khonghucu') { ?>
                          <option value="khonghucu" selected>Khonghucu</option><?php
                        } else { ?>
                          <option value="khonghucu">Khonghucu</option><?php
                        }?>
                      </select><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="kewarganegaraan">Kewarganegaraan</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->kewarganegaan ?>" id="kewarganegaraan" name="kewarganegaraan" placeholder="Masukkan kewarganegaraan" onkeypress="return event.charCode >= 65 && event.charCode <= 90" minlength="3" maxlength="3" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="kewarganegaraan" name="kewarganegaraan" placeholder="Masukkan kewarganegaraan" onkeypress="return event.charCode >= 65 && event.charCode <= 90" minlength="3" maxlength="3" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->alamat ?>" id="alamat" name="alamat" placeholder="masukan alamat" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="25" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="alamat" name="alamat" value="<?php echo $this->session->userdata('alamat') ?>" placeholder="masukan alamat" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="25" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="rt">RT</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->rt ?>" id="rt" name="rt" placeholder="masukan RT" maxlength="3" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="rt" name="rt" placeholder="masukan RT" maxlength="3" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="rw">RW</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->rw ?>" id="rw" name="rw" placeholder="masukan RW" maxlength="3" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="rw" name="rw" placeholder="masukan RW" maxlength="3" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="namaDusun">Nama Dusun</label>
                    <?php if ($isDaftar) { ?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->nama_dusun ?>" id="namaDusun" name="namaDusun" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="namaDusun" name="namaDusun" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="namaKelurahan">Nama Kelurahan</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->nama_kelurahan ?>" id="namaKelurahan" name="namaKelurahan" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="namaKelurahan" name="namaKelurahan" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="kecamatan">Kecamatan</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->kecamatan ?>" id="kecamatan" name="kecamatan" placeholder="masukan kecamatan" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="25" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="kecamatan" name="kecamatan" value="<?php echo $this->session->userdata('kecamatan') ?>" placeholder="masukan kecamatan" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="25" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="kabupaten">Kabupaten</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->kabupaten ?>" id="kabupaten" name="kabupaten" placeholder="masukan kabupaten" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="25" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="kabupaten" name="kabupaten" value="<?php echo $this->session->userdata('kabupaten') ?>" placeholder="masukan kabupaten" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 32 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122" maxlength="25" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="kodePos">Kode Pos</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->kode_pos ?>" id="kodePos" name="kodePos" placeholder="masukan kode pos" maxlength="5" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="kodePos" name="kodePos" placeholder="masukan kode pos" maxlength="5" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="namaAyah">Nama Ayah</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->nama_ayah ?>" id="namaAyah" name="namaAyah" placeholder="masukkan nama ayah" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="namaAyah" name="namaAyah" placeholder="masukkan nama ayah" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="nikAyah">NIK Ayah</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->nik_ayah ?>" id="nikAyah" name="nikAyah" placeholder="masukan nik ayah" minlength="20" maxlength="20" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="nikAyah" name="nikAyah" placeholder="masukan nik ayah" minlength="20" maxlength="20" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="tahunLahirAyah">Tahun Lahir Ayah</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->thn_lhr_ayah ?>" id="tahunLahirAyah" name="tahunLahirAyah" placeholder="masukan tahun lahir ayah" maxlength="4" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="tahunLahirAyah" name="tahunLahirAyah" placeholder="masukan tahun lahir ayah" maxlength="4" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="namaIbu">Nama Ibu</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->nama_ibu ?>" id="namaIbu" name="namaIbu" placeholder="masukkan nama ibu" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="namaIbu" name="namaIbu" placeholder="masukkan nama ibu" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="nikIbu">NIK Ibu</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->nik_ibu ?>" id="nikIbu" name="nikIbu" placeholder="masukan nik ibu" minlength="20" maxlength="20" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="nikIbu" name="nikIbu" placeholder="masukan nik ibu" minlength="20" maxlength="20" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="tahunLahirIbu">Tahun Lahir Ibu</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->thn_lhr_ibu ?>" id="tahunLahirIbu" name="tahunLahirIbu" placeholder="masukan tahun lahir ibu" maxlength="4" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="tahunLahirIbu" name="tahunLahirIbu" placeholder="masukan tahun lahir ibu" maxlength="4" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <label for="telepon">Telepon</label>
                    <?php if ($isDaftar) {?>
                      <input type="text" class="form-control" value="<?php echo $daftarUlang[0]->no_hp_ortu ?>" id="telepon" name="telepon" placeholder="masukan nomor telepon" minlength="12" maxlength="15" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="telepon" name="telepon" value="<?php echo $this->session->userdata('noHp') ?>" placeholder="masukan nomor telepon" minlength="12" maxlength="15" onkeypress="return event.charCode >= 48 && event.charCode <=57" required><?php
                    }?>
                  </div>
                  <div class="form-group">
                    <?php if ($isDaftar) {?>
                      <div class="row mt-2">
                        <div class="col d-flex align-items-center">
                          <p class="mb-0">
                            <span class="lead" data-dz-name><?php echo $daftarUlang[0]->upload_dokumen ?></span>
                          </p>
                        </div>
                        <div class="col-7 d-flex align-items-center">
                          <div class="progress progress-striped active w-100" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                          </div>
                        </div>
                        <div class="col-auto d-flex align-items-center">
                          <div class="btn-group">
                            <?php $path = 'assert/dokumen/daftar_ulang/'.$tahunAjaran.'/'.$daftarUlang[0]->upload_dokumen;?>
                            <a href="<?php echo base_url($path) ?>" class="submit btn btn-success">
                              <i class="fas fa-download mr-2"></i>Download
                            </a>
                          </div>
                        </div>
                      </div><?php
                    } else {?>
                      <label for="uploadDokumen">Unggah Dokumen</label>
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="uploadDokumen" name="uploadDokumen" accept=".zip" required>
                        <label class="custom-file-label" for="uploadDokumen">Pilih File</label>
                      </div><?php
                    }?>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <?php if (($isDaftar && $reUploadDocument) || !$canDaftarUlang) { ?>
                    <button type="submit" class="btn btn-primary" disabled>Simpan</button><?php
                  } else if($reUploadDocument) { ?>
                    <input type="text" id="id" name="id" value="<?php echo $daftarUlang[0]->id_daftar_ulang ?>" hidden>
                    <input type="text" id="catatan" name="catatan" value="<?php echo $daftarUlang[0]->catatan ?>" hidden>
                    <input type="text" id="reUploadDocument" name="reUploadCodument" value="Y" hidden>
                    <button type="submit" class="btn btn-primary">Simpan</button><?php
                  } else { ?>
                    <input type="text" id="reUploadDocument" name="reUploadCodument" value="N" hidden>
                    <button type="submit" class="btn btn-primary">Simpan</button><?php
                  } ?>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <button type="submit" class="btn btn-primary swalDefaultSuccess" style="visibility: hidden">success</button>
  <button type="submit" class="btn btn-primary swalDefaultError" style="visibility: hidden">failed</button>
  <button type="submit" class="btn btn-primary swalDefaultWarning" style="visibility: hidden">warning</button>

<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');

  if ($this->session->flashdata('success')) {
    echo $this->session->flashdata('success');
    unset($_SESSION['success']);
  }
  
  if ($this->session->flashdata('failed')) {
    echo $this->session->flashdata('failed');
    unset($_SESSION['failed']);
  }
  
  if ($this->session->flashdata('warning')) {
    echo $this->session->flashdata('warning');
    unset($_SESSION['warning']);
  }
?>