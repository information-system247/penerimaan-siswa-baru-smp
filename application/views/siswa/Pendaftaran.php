<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $activeMenu);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Pendaftaran</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12"> <?php 
            $color = 'primary';
            $text = 'Pendaftaran Siswa Baru';
            $isDaftar = false;
            if (!empty($statusSeleksi) && $pengumumanSeleksi && !empty($pendaftaran)) {
              $color = 'warning';
              $text = 'Seleksi pendaftaran sudah dilakukan. Silahkan cek hasil seleksi.';
              $isDaftar = true;
            } else if (!empty($pendaftaran)) {
              $color = 'success';
              $text = 'Terimakasih sudah melakukan pendaftaran. Silahkan menuggu pengumuman seleksi.';
              $isDaftar = true;
            } else if (empty($pendaftaran) && !$canRegister){
              $color = 'warning';
              $text = 'Pendaftaran sudah ditutup. Silahkan melakukan pendaftaran di tahun ajaran yang selanjutnya.';
              $isDaftar = false;
            } ?>
            <div class="card card-<?php echo $color; ?>">
              <div class="card-header">
                <h3 class="card-title"><?php echo $text ?></h3>
              </div>
              <!-- /.card-header -->
              <form id="formPendaftaran" method="post" action="<?php echo base_url('siswa/pendaftaran/store') ?>" enctype="multipart/form-data">
                <div class="card-body">
                  <input type="text" id="id" name="id" hidden>
                  <div class="form-group">
                    <label for="noPendaftaran">Nomor Pendaftaran</label>
                    <?php if ($isDaftar) {
                      $noPendaftaran = $pendaftaran[0]->no_pendaftaran;
                    } ?>
                    <input type="text" class="form-control" id="noPendaftaran" name="noPendaftaran" value="<?php echo $noPendaftaran ?>" required disabled>
                  </div>
                  <div class="form-group">
                    <label for="tanggalDaftar">Tanggal Daftar</label>
                    <?php if ($isDaftar) {
                      $tanggalDaftar = explode(' ', $pendaftaran[0]->tanggal_daftar);
                      $date = explode('-', $tanggalDaftar[0]);
                      $tanggalDaftar = $date[2].'-'.$date[1].'-'.$date[0];
                    }?>
                    <input type="text" class="form-control" id="tanggalDaftar" name="tanggalDaftar" value="<?php echo $tanggalDaftar ?>" required disabled>
                  </div>
                  <div class="form-group">
                    <label for="tahunAjaran">Tahun Ajaran</label>
                    <input type="text" class="form-control" id="tahunAjaran" name="tahunAjaran" value="<?php echo $tahunAjaran ?>" required disabled>
                  </div>
                  <div class="form-group">
                    <label for="noIjazah">Nomor Ijazah</label><?php 
                    if ($isDaftar) { ?>
                      <input type="text" class="form-control" id="noIjazah" name="noIjazah" placeholder="Masukkan nomor ijazah" value="<?php echo $pendaftaran[0]->no_ijazah ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode >= 65 && event.charCode <= 90" minlength="20" maxlength="20" required><?php
                    } else {?>
                      <input type="text" class="form-control" id="noIjazah" name="noIjazah" placeholder="Masukkan nomor ijazah" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode >= 65 && event.charCode <= 90" minlength="20" maxlength="20" required><?php
                    } ?>
                  </div>
                  <div class="form-group">
                    <input type="text" id="calonSiswaId" name="calonSiswaId" value="<?php echo $this->session->userdata('id') ?>" hidden>
                  </div>
                  <div class="form-group"><?php
                    if ($isDaftar) { ?>
                      <div class="table table-striped files" id="previews">
                        <div class="row mt-2">
                          <div class="col d-flex align-items-center">
                            <p class="mb-0">
                              <span class="lead" data-dz-name><?php echo $pendaftaran[0]->upload_ijazah ?></span>
                            </p>
                          </div>
                          <div class="col-5 d-flex align-items-center">
                            <div class="progress progress-striped active w-100" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                              <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                            </div>
                          </div>
                          <div class="col-auto d-flex align-items-center">
                            <div class="btn-group">
                              <?php $path = 'assert/dokumen/pendaftaran/'.$pendaftaran[0]->tahun_ajaran.'/'.$pendaftaran[0]->upload_ijazah;?>
                              <a href="<?php echo base_url($path) ?>" class="submit btn btn-success">
                                <i class="fas fa-download mr-2"></i>Download
                              </a>
                              <a href="<?php echo base_url('siswa/pendaftaran/reuploaddocument') ?>" class="submit btn btn-primary">
                                <i class="fas fa-plus mr-2"></i>Upload ulang
                              </a>
                            </div>
                          </div>
                        </div>
                      </div><?php
                    } else { ?>
                      <label for="uploadIjazah">Unggah Ijazah</label>
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="uploadIjazah" name="uploadIjazah" accept=".pdf" required>
                        <label class="custom-file-label" for="uploadIjazah">Pilih File</label>
                      </div><?php
                    } ?>
                  </div>
                  <div class="form-group"><?php
                    if ($isDaftar) { ?>
                      <div class="table table-striped files" id="previews">
                        <div class="row mt-2">
                          <div class="col d-flex align-items-center">
                            <p class="mb-0">
                              <span class="lead" data-dz-name><?php echo $pendaftaran[0]->upload_akta ?></span>
                            </p>
                          </div>
                          <div class="col-5 d-flex align-items-center">
                            <div class="progress progress-striped active w-100" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                              <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                            </div>
                          </div>
                          <div class="col-auto d-flex align-items-center">
                            <div class="btn-group">
                              <?php $path = 'assert/dokumen/pendaftaran/'.$pendaftaran[0]->tahun_ajaran.'/'.$pendaftaran[0]->upload_akta;?>
                              <a href="<?php echo base_url($path) ?>" class="submit btn btn-success">
                                <i class="fas fa-download mr-2"></i>Download
                              </a>
                              <a href="<?php echo base_url('siswa/pendaftaran/reuploaddocumentijazah') ?>" class="submit btn btn-primary">
                                <i class="fas fa-plus mr-2"></i>Upload ulang
                              </a>
                            </div>
                          </div>
                        </div>
                      </div><?php
                    } else { ?>
                      <label for="uploadAkta">Unggah Akta Kelahiran</label>
                        <div class="custom-file">
                          <input type="file" class="cutom-file-input" id="uploadAkta" name="uploadAkta" accept=".pdf" required>
                          <label class="custom-file-label" for="uploadAkta">Pilih File</label>
                        </div><?php
                    } ?>
                  </div>
                  <div class="form-group"><?php
                    if ($isDaftar) { ?>
                      <div class="table table-striped files" id="previews">
                        <div class="row mt-2">
                          <div class="col d-flex align-items-center">
                            <p class="mb-0">
                              <span class="lead" data-dz-name><?php echo $pendaftaran[0]->upload_kk ?></span>
                            </p>
                          </div>
                          <div class="col-5 d-flex align-items-center">
                            <div class="progress progress-striped active w-100" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                              <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                            </div>
                          </div>
                          <div class="col-auto d-flex align-items-center">
                            <div class="btn-group">
                              <?php $path = 'assert/dokumen/pendaftaran/'.$pendaftaran[0]->tahun_ajaran.'/'.$pendaftaran[0]->upload_kk;?>
                              <a href="<?php echo base_url($path) ?>" class="submit btn btn-success">
                                <i class="fas fa-download mr-2"></i>Download
                              </a>
                              <a href="<?php echo base_url('siswa/pendaftaran/reuploaddocumentijazah') ?>" class="submit btn btn-primary">
                                <i class="fas fa-plus mr-2"></i>Upload ulang
                              </a>
                            </div>
                          </div>
                        </div>
                      </div><?php
                    } else { ?>
                      <label for="uploadKk">Unggah Kartu Keluarga</label>
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="uploadKk" name="uploadKk" accept=".pdf" required>
                        <label for="uploadKk" class="custom-file-label">Pilih File</label>
                      </div><?php
                    } ?>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <?php if (!$canRegister || (!$reUploadDocument && $isDaftar)) {
                    if (!empty($statusSeleksiOptional2) && $validPendingView) { ?>
                      <button type="submit" class="btn btn-primary">Upload</button><?php
                    } else { ?>
                      <button type="submit" class="btn btn-primary" disabled>Daftar</button><?php
                    }
                  } else {?>
                    <button type="submit" class="btn btn-primary">Daftar</button><?php
                  }?>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <button type="submit" class="btn btn-primary swalDefaultSuccess" style="visibility: hidden">success</button>
  <button type="submit" class="btn btn-primary swalDefaultError" style="visibility: hidden">failed</button>
  <button type="submit" class="btn btn-primary swalDefaultWarning" style="visibility: hidden">warning</button>

<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');

  if ($this->session->flashdata('success')) {
    echo $this->session->flashdata('success');
    unset($_SESSION['success']);
  }
  
  if ($this->session->flashdata('failed')) {
    echo $this->session->flashdata('failed');
    unset($_SESSION['failed']);
  }
  
  if ($this->session->flashdata('warning')) {
    echo $this->session->flashdata('warning');
    unset($_SESSION['warning']);
  }
?>