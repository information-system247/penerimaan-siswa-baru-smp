<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $file_pdf ?></title>
    <style>
      #table {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }

      #table td, #table th {
        border: 1px solid #ddd;
        padding: 8px;
      }

      #table tr:nth-child(even){background-color: #f2f2f2;}

      #table tr:hover {background-color: #ddd;}

      #table th {
        padding-top: 10px;
        padding-bottom: 10px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
      }
    </style>
  </head>
  <body>
    <div style="text-align:center">
        <h3> Laporan Data Pendaftaran Tahun (...) </h3>
    </div>
    <table id="table">
      <thead>
        <tr>
          <th style="text-align: center;">No</th>
          <th style="text-align: center;">Tahun Ajaran</th>
          <th style="text-align: center;">No. Pendaftaran</th>
          <th style="text-align: center;">NISN</th>
          <th style="text-align: center;">Nama Lengkap</th>
          <th style="text-align: center;">Gender</th>
          <th style="text-align: center;">Tempat Lahir</th>
          <th style="text-align: center;">Tanggal Lahir</th>
          <th style="text-align: center;">Agama</th>
          <th style="text-align: center;">Alamat</th>
        </tr>
      </thead>
        <tbody>
          <tr>
            <?php if (count($pendaftaranList) == 0) {
              ?><td colspan="8" style="text-align: center;">Data Masih Kosong ...</td><?php
            } else { 
              $no = 1;
              foreach ($pendaftaranList as $data) {
                $agama = "-";
                $alamat = "-";
                $nisn = "-";
                $namaLengkap = "-";
                $gender = "-";
                $tempatLahir = "-";
                $tglLahir = "-";
                if (!empty($calonSiswaList)) {
                  foreach ($calonSiswaList as $calonSiswa) {
                    if ($data->calon_siswa_id == $calonSiswa->id_calon_siswa) {
                      $agama = $calonSiswa->agama;
                      $alamat = $calonSiswa->alamat;
                      $nisn = $calonSiswa->nisn;
                      $namaLengkap = $calonSiswa->nama_lengkap;
                      $gender = $calonSiswa->gender;
                      $tempatLahir = $calonSiswa->tempat_lahir;
                      $tglLahir = $calonSiswa->tanggal_lahir;
                    }
                  }
                } ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $data->tahun_ajaran ?></td>
                  <td><?php echo $data->no_pendaftaran ?></td>
                  <td><?php echo $nisn ?></td>
                  <td><?php echo $namaLengkap ?></td>
                  <td><?php echo PpdbUtils::getGender($gender) ?></td>
                  <td><?php echo $tempatLahir ?></td>
                  <td><?php echo DateUtils::dateFormatIdn($tglLahir) ?></td>
                  <td><?php echo $agama ?></td>
                  <td><?php echo $alamat ?></td>
                </tr> <?php
              }
            }?>
          </tr>
        </tbody>
    </table>
  </body>
</html>