<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $activeMenu);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Laporan Daftar Ulang</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Laporan</a></li>
              <li class="breadcrumb-item active">Daftar Ulang</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div class="row">
          <div class="col-2">
            <div class="card">
              <a href="<?php echo base_url('export/pdf/pendaftaran') ?>" type="button" target="_blank" class="btn btn-block btn-primary">Export PDF</a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="dataTable" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th style="text-align: center;">No</th>
                      <th style="text-align: center;">Tanggal Daftar Ulang</th>
                      <th style="text-align: center;">No. Pendaftaran</th>
                      <th style="text-align: center;">NISN</th>
                      <th style="text-align: center;">Nama Lengkap</th>
                      <th style="text-align: center;">Status Review</th>
                      <th style="width: 70px; text-align: center;">Aksi</th>
                    </tr>
                  </thead>
                  <tbody><?php $no = 1;
                    if (count($daftarUlangList) == 0) { ?>
                      <tr>
                        <td colspan="7" style="text-align: center;">Data Masih Kosong ...</td>
                      </tr><?php
                    } else {
                      foreach ($daftarUlangList as $data) {
                        $daftarUlang = null;
                        foreach ($pendaftaranList as $pendaftaran) {
                          if ($pendaftaran->no_pendaftaran == $data->no_pendaftaran) {
                            $daftarUlang = array(
                              'daftarUlang' => $data,
                              'pendaftaran' => $pendaftaran
                            );
                          }
                        }
                         ?>
                        <tr>
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $data->tgl_daftar_ulang ?></td>
                          <td><?php echo $data->no_pendaftaran ?></td>
                          <td><?php echo "NISN" ?></td>
                          <td><?php echo $data->nama_lengkap ?></td>
                          <td><?php echo $data->status_review ?></td>
                          <td>
                            <button type="button" class="btn btn-block bg-gradient-info btn btn-m" data-toggle="modal" data-target="#modalDetail<?php echo $data->id_daftar_ulang ?>"><i class="nav-icon fas fa-info mr-2"></i>Detail</button>
                          </td>
                        </tr>
                        <?php $this->load->view('laporan/daftar_ulang/detail', $daftarUlang);
                      }
                    } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <button type="submit" class="btn btn-primary swalDefaultSuccess" style="visibility: hidden">success</button>
  <button type="submit" class="btn btn-primary swalDefaultError" style="visibility: hidden">failed</button>
  <button type="submit" class="btn btn-primary swalDefaultWarning" style="visibility: hidden">warning</button>

<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');

  if ($this->session->flashdata('success')) {
    echo $this->session->flashdata('success');
    unset($_SESSION['success']);
  }
  
  if ($this->session->flashdata('failed')) {
    echo $this->session->flashdata('failed');
    unset($_SESSION['failed']);
  }
  
  if ($this->session->flashdata('warning')) {
    echo $this->session->flashdata('warning');
    unset($_SESSION['warning']);
  }
?>
