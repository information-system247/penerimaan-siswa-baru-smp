<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $activeMenu);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Laporan Siswa Diterima</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Laporan</a></li>
              <li class="breadcrumb-item active">Seleksi</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div class="row">
          <div class="col-2">
            <div class="card">
              <a href="<?php echo base_url('export/pdf/terima') ?>" type="button" target="_blank" class="btn btn-block btn-primary">Export PDF</a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="dataTable" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th style="text-align: center;">No</th>
                      <th style="text-align: center;">Tahun Ajaran</th>
                      <th style="text-align: center;">No. Pendaftaran</th>
                      <th style="text-align: center;">NISN</th>
                      <th style="text-align: center;">Nama Lengkap</th>
                      <th style="text-align: center;">Gender</th>
                      <th style="text-align: center;">Tempat Lahir</th>
                      <th style="text-align: center;">Tanggal Lahir</th>
                      <th style="text-align: center;">Agama</th>
                      <th style="text-align: center;">Alamat</th>
                    </tr>
                  </thead>
                  <tbody><?php $no = 1;
                    if (count($pendaftaranList) == 0) { ?>
                      <tr>
                        <td colspan="11" style="text-align: center;">Data Masih Kosong ...</td>
                      </tr><?php
                    } else {
                      foreach ($pendaftaranList as $pendaftaran) {
                        $agama = "-";
                        $alamat = "-";
                        $nisn = "-";
                        $namaLengkap = "-";
                        $gender = "-";
                        $tempatLahir = "-";
                        $tglLahir = "-";
                        if (!empty($calonSiswaList)) {
                          foreach ($calonSiswaList as $calonSiswa) {
                            if ($pendaftaran->calon_siswa_id == $calonSiswa->id_calon_siswa) {
                              $agama = $calonSiswa->agama;
                              $alamat = $calonSiswa->alamat;
                              $nisn = $calonSiswa->nisn;
                              $namaLengkap = $calonSiswa->nama_lengkap;
                              $gender = $calonSiswa->gender;
                              $tempatLahir = $calonSiswa->tempat_lahir;
                              $tglLahir = $calonSiswa->tanggal_lahir;
                            }
                          }
                        }
                        ?>
                        <tr>
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $pendaftaran->tahun_ajaran ?></td>
                          <td><?php echo $pendaftaran->no_pendaftaran ?></td>
                          <td><?php echo $nisn ?></td>
                          <td><?php echo $namaLengkap ?></td>
                          <td><?php echo PpdbUtils::getGender($gender) ?></td>
                          <td><?php echo $tempatLahir ?></td>
                          <td><?php echo DateUtils::dateFormatIdn($tglLahir) ?></td>
                          <td><?php echo $agama ?></td>
                          <td><?php echo $alamat ?></td>
                        </tr><?php
                      }
                    } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <button type="submit" class="btn btn-primary swalDefaultSuccess" style="visibility: hidden">success</button>
  <button type="submit" class="btn btn-primary swalDefaultError" style="visibility: hidden">failed</button>
  <button type="submit" class="btn btn-primary swalDefaultWarning" style="visibility: hidden">warning</button>

<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');

  if ($this->session->flashdata('success')) {
    echo $this->session->flashdata('success');
    unset($_SESSION['success']);
  }
  
  if ($this->session->flashdata('failed')) {
    echo $this->session->flashdata('failed');
    unset($_SESSION['failed']);
  }
  
  if ($this->session->flashdata('warning')) {
    echo $this->session->flashdata('warning');
    unset($_SESSION['warning']);
  }
?>
