<!--Jquery-->
<script type="text/javascript" src="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/js/jquery.min.js"></script>
<!--Boostrap-Jquery-->
<script type="text/javascript" src="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/js/bootstrap.js"></script>
<!--Preetyphoto-Jquery-->
<script type="text/javascript" src="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/js/jquery.prettyPhoto.js"></script>
<!--NiceScroll-Jquery-->
<script type="text/javascript" src="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/js/jquery.nicescroll.js"></script>
<script type="text/javascript" src="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/js/waypoints.min.js"></script>
<!--Isotopes-->
<script type="text/javascript" src="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/js/jquery.isotope.js"></script>
<!--Wow-Jquery-->
<script type="text/javascript" src="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/js/wow.js"></script>
<!--Count-Jquey-->
<script type="text/javascript" src="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/js/jquery.countTo.js"></script>
<script type="text/javascript" src="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/js/jquery.inview.min.js"></script>
<!--Owl-Crousels-Jqury-->
<script type="text/javascript" src="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/js/owl.carousel.js"></script>
<!--Main-Scripts-->
<script type="text/javascript" src="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/js/script.js"></script>
</body>
</html>

<!-- Hosting24 Analytics Code -->
<script type="text/javascript" src="http://stats.hosting24.com/count.php"></script>
<!-- End Of Analytics Code -->
