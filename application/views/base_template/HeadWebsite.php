<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Pendaftaran Siswa Baru</title>

  <!--Bootstrap-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/css/bootstrap.css">
  <!--Stylesheets-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/css/style.css">
  <!--Responsive-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/css/responsive.css">
  <!--Animation-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/css/animate.css">
  <!--Prettyphoto-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/css/prettyPhoto.css">
  <!--Font-Awesome-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/css/font-awesome.css">
  <!--Owl-Slider-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/css/owl.carousel.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/css/owl.theme.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(BaseUrl::ASSERT_URL_WEBSITE) ?>/css/owl.transitions.css">
</head>