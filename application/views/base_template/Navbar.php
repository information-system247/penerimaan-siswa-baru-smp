<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed"> <!-- dark-mode -->
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__wobble" src="<?php echo base_url(BaseUrl::ASSERT_URL) ?>/dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
  </div>

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light"> <!-- navbar-white navbar-light -->
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <?php $path = 'index';
        if ($this->session->userdata('status') == LoginSessionConstant::SISWA) {
          $path = 'siswa/index';
        }?>
        <a href="<?php echo base_url($path) ?>" class="nav-link text-bold">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <?php if ($this->session->userdata('status') == LoginSessionConstant::PANITIA) { ?>
          <a href="#modalPengaturan" class="nav-link text-bold" data-toggle="modal">Pengaturan</a><?php
        } else { ?>
          <a href="#modalPengaturanSiswa" class="nav-link text-bold" data-toggle="modal">Pengaturan</a><?php
        } ?>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

<?php if ($this->session->userdata('status') == LoginSessionConstant::PANITIA) { ?>
  <!-- modal Pengaturan panitia -->
  <div class="modal fade" id="modalPengaturan">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-info">
          <h4 class="modal-title">Pengaturan</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form class="needs-validation" action="<?php echo base_url('master/panitiapendaftaran/update') ?>" method="POST" novalidate>
          <div class="modal-body">
            <input type="text" name="id" id="id" value="<?php echo $panitia[0]->id_admin_pendaftaran ?>" hidden>
            <input type="text" name="hakAkses" id="hakAkses" value="<?php echo $panitia[0]->hak_akses ?>" hidden>
            <div class="form-group">
              <label for="nik">NIK</label>
              <input type="text" name="nik" id="nik" value="<?php echo $panitia[0]->nik ?>" hidden>
              <input type="text" class="form-control" id="nikView" name="nikView" placeholder="masukan nik" value="<?php echo $panitia[0]->nik ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="25" disabled>
            </div>
            <div class="form-group">
              <label for="namaLengkap">Nama lengkap</label>
              <input type="text" class="form-control" id="namaLengkap" name="namaLengkap" placeholder="masukan nama lengkap" value="<?php echo $panitia[0]->nama ?>" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required>
            </div>
            <div class="form-group">
              <label for="gender">Jeni Kelamin</label>
              <div class="row">
                <?php if ($panitia[0]->gender == 'L') { ?>
                  <div class="col-sm-3">
                    <div class="custom-control custom-radio">
                      <input class="custom-control-input" type="radio" id="customRadio1" name="gender" value="L" checked>
                      <label for="customRadio1" class="custom-control-label">Laki - Laki</label>
                    </div>
                  </div><?php
                } else { ?>
                  <div class="col-sm-2">
                    <div class="custom-control custom-radio">
                      <input class="custom-control-input" type="radio" id="customRadio1" name="gender" value="L">
                      <label for="customRadio1" class="custom-control-label">Laki - Laki</label>
                    </div>
                  </div><?php
                }

                if ($panitia[0]->gender == 'P') {?>
                  <div class="col-sm-2">
                    <div class="custom-control custom-radio">
                      <input class="custom-control-input" type="radio" id="customRadio2" name="gender" value="P" checked>
                      <label for="customRadio2" class="custom-control-label">Perempuan</label>
                    </div>
                  </div><?php
                } else { ?>
                  <div class="col-sm-2">
                    <div class="custom-control custom-radio">
                      <input class="custom-control-input" type="radio" id="customRadio2" name="gender" value="P">
                      <label for="customRadio2" class="custom-control-label">Perempuan</label>
                    </div>
                  </div><?php
                }?>
              </div>
            </div>
            <div class="form-group">
              <label for="email">Email</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan email address" value="<?php echo $panitia[0]->email ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 64" maxlength="50" required>
            </div>
            <div class="form-group">
              <label for="telepon">Telepon</label>
              <input type="text" class="form-control" id="telepon" name="telepon" placeholder="masukan nomor telepon" value="<?php echo $panitia[0]->no_hp ?>" minlength="12" maxlength="15" onkeypress="return event.charCode >= 48 && event.charCode <=57" required>
            </div>
            <div class="form-group">
              <label for="username">Username</label>
              <input type="text" class="form-control" id="username" name="username" placeholder="Masukkan username" value="<?php echo $panitia[0]->username ?>" maxlength="50" required>
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan password" value="<?php echo $this->encryption->decrypt($panitia[0]->password) ?>" maxlength="50" required>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal --><?php
} else{ ?>
  <!-- modal Pengaturan panitia -->
  <div class="modal fade" id="modalPengaturanSiswa">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-info">
          <h4 class="modal-title">Pengaturan</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form class="needs-validation" action="#" method="POST" novalidate>
          <div class="modal-body">
            <input type="text" name="id" id="id" value="<?php echo $calonSiswa[0]->id_calon_siswa ?>" hidden>
            <div class="form-group">
              <label for="username">Username</label>
              <input type="text" class="form-control" id="username" name="username" placeholder="Masukkan username" value="<?php echo $calonSiswa[0]->username ?>" maxlength="50" required>
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan password" value="<?php echo $this->encryption->decrypt($calonSiswa[0]->password) ?>" maxlength="50" required>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal --><?php
} ?>
