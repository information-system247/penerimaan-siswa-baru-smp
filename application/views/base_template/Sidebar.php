<?php 
  $this->load->view('base_template/Navbar');
?>

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="<?php echo base_url(BaseUrl::ASSERT_URL) ?>/dist/img/ppdbLogo.png" alt="PPDB Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light text-bold">PPDB</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url(BaseUrl::ASSERT_URL) ?>/dist/img/userLogo.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $this->session->userdata('nama'); ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <?php if ($this->session->userdata('status') == LoginSessionConstant::PANITIA) { ?>
            <li class="nav-item">
              <?php $isActive = ($activeMenu == MenuConstant::BERANDA) ? "active" : ""; ?>
              <a href="<?php echo base_url('index') ?>" class="nav-link <?php echo $isActive ?>">
                <i class="nav-icon fas fa-house-user"></i>
                <p>Beranda</p>
              </a>
            </li>
            <?php if ($this->session->userdata('hak_akses') == HakAksesConstant::ADMIN) { ?>
              <li class="nav-item">
                <?php $isActive = ($activeMenu == MenuConstant::PANITIA || $activeMenu == MenuConstant::CALON_SISWA) ? "active" : ""; ?>
                <a href="#" class="nav-link <?php echo $isActive ?>">
                  <i class="nav-icon fas fa-folder"></i>
                  <p>Master Data<i class="fas fa-angle-left right"></i></p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <?php $isActive = ($activeMenu == MenuConstant::PANITIA) ? "active" : ""; ?>
                    <a href="<?php echo base_url('master/panitiapendaftaran') ?>" class="nav-link <?php echo $isActive ?>">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Panitia</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <?php $isActive = ($activeMenu == MenuConstant::CALON_SISWA) ? "active" : ""; ?>
                    <a href="<?php echo base_url('master/calonsiswa') ?>" class="nav-link <?php echo $isActive ?>">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Pendaftar</p>
                    </a>
                  </li>
                </ul>
              </li><?php
            } ?>
            <li class="nav-item">
              <?php $isActive = ($activeMenu == MenuConstant::SELEKSI || $activeMenu == MenuConstant::DAFTAR_ULANG || $activeMenu == MenuConstant::PENDAFTARAN) ? "active" : ""; ?>
              <a href="#" class="nav-link <?php echo $isActive ?>">
                <i class="nav-icon fas fa-money-bill"></i>
                <p>
                  Proses Penerimaan
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <?php $isActive = ($activeMenu == MenuConstant::PENDAFTARAN) ? "active" : ""; ?>
                  <a href="<?php echo base_url('master/pendaftaran') ?>" class="nav-link <?php echo $isActive ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Berkas Pendaftar</p>
                  </a>
                </li>
                <li class="nav-item">
                  <?php $isActive = ($activeMenu == MenuConstant::SELEKSI) ? "active" : ""; ?>
                  <a href="<?php echo base_url('master/seleksi') ?>" class="nav-link <?php echo $isActive ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Seleksi</p>
                  </a>
                </li>
                <li class="nav-item">
                  <?php $isActive = ($activeMenu == MenuConstant::DAFTAR_ULANG) ? "active" : ""; ?>
                  <a href="<?php echo base_url('master/daftarulang') ?>" class="nav-link <?php echo $isActive ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Daftar Ulang</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <?php $isActive = ($activeMenu == MenuConstant::LAPORAN_PENDAFTARAN || $activeMenu == MenuConstant::LAPORAN_SELEKSI_DITERIMA || $activeMenu == MenuConstant::LAPORAN_SELEKSI_TIDAK_DITERIMA || $activeMenu == MenuConstant::LAPORAN_DAFTAR_ULANG) ? "active" : ""; ?>
              <a href="#" class="nav-link <?php echo $isActive ?>">
                <i class="nav-icon fas fa-book"></i>
                <p>
                  Laporan
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <?php $isActive = ($activeMenu == MenuConstant::LAPORAN_PENDAFTARAN) ? "active" : ""; ?>
                  <a href="<?php echo base_url('laporan/pendaftaran') ?>" class="nav-link <?php echo $isActive ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Siswa Pendaftar</p>
                  </a>
                </li>
                <li class="nav-item">
                  <?php $isActive = ($activeMenu == MenuConstant::LAPORAN_SELEKSI_DITERIMA) ? "active" : ""; ?>
                  <a href="<?php echo base_url('laporan/seleksi/diterima') ?>" class="nav-link <?php echo $isActive ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Siswa Diterima</p>
                  </a>
                </li>
                <li class="nav-item">
                  <?php $isActive = ($activeMenu == MenuConstant::LAPORAN_SELEKSI_TIDAK_DITERIMA) ? "active" : ""; ?>
                  <a href="<?php echo base_url('laporan/seleksi/tidakditerima') ?>" class="nav-link <?php echo $isActive ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Siswa Tidak Diterima</p>
                  </a>
                </li>
                <li class="nav-item">
                  <?php $isActive = ($activeMenu == MenuConstant::LAPORAN_DAFTAR_ULANG) ? "active" : ""; ?>
                  <a href="<?php echo base_url('laporan/daftarulang') ?>" class="nav-link <?php echo $isActive ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Daftar Ulang</p>
                  </a>
                </li>
              </ul>
            </li>
            <?php if ($this->session->userdata('hak_akses') == HakAksesConstant::ADMIN) { ?>
              <li class="nav-item">
                <?php $isActive = ($activeMenu == MenuConstant::KONFIGURASI) ? "active" : ""; ?>
                <a href="#modalKonfigurasi" data-toggle="modal" class="nav-link">
                  <i class="nav-icon fas fa-address-card"></i>
                  <p>Konfigurasi</p>
                </a>
              </li><?php
            }?>

            <li class="nav-item">
              <a href="<?php echo base_url('logout') ?>" class="nav-link">
                <i class="nav-icon fas fa-power-off"></i>
                <p>Keluar</p>
              </a>
            </li><?php
          } else { ?>
            <li class="nav-item">
              <?php $isActive = ($activeMenu == MenuConstant::BERANDA) ? "active" : ""; ?>
              <a href="<?php echo base_url('siswa/index') ?>" class="nav-link <?php echo $isActive ?>">
                <i class="nav-icon fas fa-house-user"></i>
                <p>Beranda</p>
              </a>
            </li>
            <li class="nav-item">
              <?php $isActive = ($activeMenu == MenuConstant::DATA_DIRI) ? "active" : ""; ?>
              <a href="<?php echo base_url('siswa/datadiri') ?>" class="nav-link <?php echo $isActive ?>">
                <i class="nav-icon fas fa-address-card"></i>
                <p>Data Diri</p>
              </a>
            </li>
            <li class="nav-item">
              <?php $isActive = ($activeMenu == MenuConstant::PENDAFTARAN) ? "active" : ""; ?>
              <a href="<?php echo base_url('siswa/pendaftaran') ?>" class="nav-link <?php echo $isActive ?>">
                <i class="nav-icon fas fa-address-card"></i>
                <p>Pendaftaran</p>
              </a>
            </li>
            <?php if (!empty($statusSeleksiOptional) && $pengumumanSeleksi) {?>
              <!-- <li class="nav-item">
                <?php $isActive = ($activeMenu == MenuConstant::SELEKSI) ? "active" : ""; ?>
                <a href="<?php echo base_url('siswa/seleksi') ?>" class="nav-link <?php echo $isActive ?>">
                  <i class="nav-icon fas fa-address-card"></i>
                  <p>Seleksi Pendaftaran</p>
                </a>
              </li>-->
              <?php
            }

            if (!empty($statusSeleksiOptional) && $statusSeleksiOptional[0]->status_diterima_atau_ditolak == 'TERIMA') { ?>
              <li class="nav-item">
                <?php $isActive = ($activeMenu == MenuConstant::DAFTAR_ULANG) ? "active" : ""; ?>
                <a href="<?php echo base_url('siswa/daftarulang') ?>" class="nav-link <?php echo $isActive ?>">
                  <i class="nav-icon fas fa-address-card"></i>
                  <p>Daftar Ulang</p>
                </a>
              </li><?php
            } ?>
            <li class="nav-item">
              <a href="<?php echo base_url('siswa/logout') ?>" class="nav-link">
                <i class="nav-icon fas fa-power-off"></i>
                <p>Keluar</p>
              </a>
            </li>
            <?php
          } ?>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

<!-- modal konfigurasi -->
<div class="modal fade" id="modalKonfigurasi">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title">Konfirmasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="needs-validation" action="<?php echo base_url('master/konfigurasi/store') ?>" method="POST" novalidate>
        <div class="modal-body">
          <div class="form-group">
            <label for="tahunAjaran">Tahun Ajaran</label>
            <?php if (!empty($konfigurasiList[0]->module == ModuleConstant::TAHUN_AJARAN)) {?>
              <input type="text" class="form-control" id="tahunAjaran" name="tahunAjaran" value="<?php echo $konfigurasiList[0]->value ?>" placeholder="masukan tahun ajaran" onkeypress="return code.event >= 48 && code.event <= 57" maxlength="4" required><?php
            } else {?>
              <input type="text" class="form-control" id="tahunAjaran" name="tahunAjaran" placeholder="masukan tahun ajaran" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="4" required><?php
            }?>
          </div>
          <div class="form-group">
            <label for="jangkaWaktuPendaftaran">Batas Waktu Pendaftaran</label>
            <?php if (!empty($konfigurasiList[1]->module == ModuleConstant::BATAS_PENDAFTARAN)) {?>
              <input type="date" class="form-control" id="jangkaWaktuPendaftaran" name="jangkaWaktuPendaftaran" value="<?php echo $konfigurasiList[1]->value ?>" required><?php
            } else {?>
              <input type="date" class="form-control" id="jangkaWaktuPendaftaran" name="jangkaWaktuPendaftaran" required><?php
            }?>
            
          </div>
          <div class="form-group">
            <label for="jangkaWaktuSeleksi">Pengumuman Seleksi</label>
            <?php if (!empty($konfigurasiList[2]->module == ModuleConstant::PENGUMUMAN_SELEKSI)) {?>
              <input type="date" class="form-control" id="jangkaWaktuSeleksi" name="jangkaWaktuSeleksi" value="<?php echo $konfigurasiList[2]->value ?>" required><?php
            } else {?>
              <input type="date" class="form-control" id="jangkaWaktuSeleksi" name="jangkaWaktuSeleksi" required><?php
            }?>
          </div>
          <div class="form-group">
            <label for="jangkaWaktuDaftarUlang">Batas Waktu Daftar Ulang</label>
            <?php if (!empty($konfigurasiList[3]->module == ModuleConstant::BATAS_DAFTAR_ULANG)) {?>
              <input type="date" class="form-control" id="jangkaWaktuDaftarUlang" name="jangkaWaktuDaftarUlang" value="<?php echo $konfigurasiList[3]->value ?>" required><?php
            } else {?>
              <input type="date" class="form-control" id="jangkaWaktuDaftarUlang" name="jangkaWaktuDaftarUlang" required><?php
            }?>
          </div>
          <div class="form-group">
            <label for="zonasi">Zonasi</label>
            <?php if (!empty($konfigurasiList[4]->module == ModuleConstant::ZONASI)) {?>
              <input type="text" class="form-control" id="zonasi" name="zonasi" value="<?php echo $konfigurasiList[4]->value ?>" required><?php
            } else {?>
              <input type="text" class="form-control" id="zonasi" name="zonasi" required><?php
            }?>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->