<!-- modal konfigurasi -->
<div class="modal fade" id="modalPendaftarInBeranda">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title">Data Pendaftar Tahun Ajaran <?php echo $tahunAjaran; ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="dataTable" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th style="text-align: center;">No</th>
                      <th style="text-align: center;">Tahun Ajaran</th>
                      <th style="text-align: center;">No. Pendaftaran</th>
                      <th style="text-align: center;">Tanggal Daftar</th>
                      <th style="text-align: center;">Nama Lengkap</th>
                      <th style="text-align: center;">Gender</th>
                      <th style="text-align: center;">No. Ijazah</th>
                      <!-- <th style="width: 80px; text-align: center;">Aksi</th> -->
                    </tr>
                  </thead>
                  <tbody><?php $no = 1;
                    if (count($pendaftaranList) == 0) { ?>
                      <tr>
                        <td colspan="8" style="text-align: center;">Data Masih Kosong ...</td>
                      </tr><?php
                    } else {
                      foreach ($pendaftaranList as $data) {
                        $pendaftaranId = array('id' => $data->id_pendaftaran); 
                        $namaLengkap = "-";
                        $gender = "-";
                        if (!empty($calonSiswaList)) {
                          foreach ($calonSiswaList as $calonSiswa) {
                            $namaLengkap = $calonSiswa->nama_lengkap;
                            $gender = $calonSiswa->gender;
                          }
                        }?>
                        <tr>
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $data->tahun_ajaran ?></td>
                          <td><?php echo $data->no_pendaftaran ?></td>
                          <td><?php echo $data->tanggal_daftar ?></td>
                          <td><?php echo $namaLengkap ?></td>
                          <td><?php echo $gender ?></td>
                          <td><?php echo $data->no_ijazah ?></td>
                        </tr><?php
                      }
                    } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->