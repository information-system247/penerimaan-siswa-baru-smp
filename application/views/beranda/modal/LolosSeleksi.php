<!-- modal konfigurasi -->
<div class="modal fade" id="modalLolosSeleksiInBeranda">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title">Data Siswa Lolos Seleksi Tahun Ajaran <?php echo $tahunAjaran; ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
              <table id="dataTable" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th style="text-align: center;">No</th>
                      <th style="text-align: center;">Tahun Ajaran</th>
                      <th style="text-align: center;">No. Pendaftaran</th>
                      <th style="text-align: center;">NISN</th>
                      <th style="text-align: center;">Nama Lengkap</th>
                      <th style="text-align: center;">Gender</th>
                      <th style="text-align: center;">Tempat Lahir</th>
                      <th style="text-align: center;">Tanggal Lahir</th>
                      <th style="text-align: center;">Agama</th>
                      <th style="text-align: center;">Alamat</th>
                    </tr>
                  </thead>
                  <tbody><?php $no = 1;
                    if (count($pendaftaranLolosSeleksiList) == 0) { ?>
                      <tr>
                        <td colspan="11" style="text-align: center;">Data Masih Kosong ...</td>
                      </tr><?php
                    } else {
                      foreach ($pendaftaranLolosSeleksiList as $pendaftaran) {
                        $agama = "-";
                        $alamat = "-";
                        $nisn = "-";
                        $namaLengkap = "-";
                        $gender = "-";
                        $tempatLahir = "-";
                        $tglLahir = "-";
                        if (!empty($calonSiswaLolosSeleksiList)) {
                          foreach ($calonSiswaLolosSeleksiList as $calonSiswa) {
                            if ($pendaftaran->calon_siswa_id == $calonSiswa->id_calon_siswa) {
                              $agama = $calonSiswa->agama;
                              $alamat = $calonSiswa->alamat;
                              $nisn = $calonSiswa->nisn;
                              $namaLengkap = $calonSiswa->nama_lengkap;
                              $gender = $calonSiswa->gender;
                              $tempatLahir = $calonSiswa->tempat_lahir;
                              $tglLahir = $calonSiswa->tanggal_lahir;
                            }
                          }
                        }
                        ?>
                        <tr>
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $pendaftaran->tahun_ajaran ?></td>
                          <td><?php echo $pendaftaran->no_pendaftaran ?></td>
                          <td><?php echo $nisn ?></td>
                          <td><?php echo $namaLengkap ?></td>
                          <td><?php echo PpdbUtils::getGender($gender) ?></td>
                          <td><?php echo $tempatLahir ?></td>
                          <td><?php echo DateUtils::dateFormatIdn($tglLahir) ?></td>
                          <td><?php echo $agama ?></td>
                          <td><?php echo $alamat ?></td>
                        </tr><?php
                      }
                    } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->