<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $activeMenu);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Seleksi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Proses Penerimaan</a></li>
              <li class="breadcrumb-item active">Seleksi</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-2">
            <div class="card">
              <?php if ($needSeleksi) {?>
                <a href="<?php echo base_url('master/seleksi/allselection') ?>" type="button" class="btn btn-block btn-primary">Seleksi</a><?php
              } else {?>
                <button class="btn btn-block btn-primary" disabled>Seleksi</button><?php
              }?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Status Seleksi Calon Siswa</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="dataTable" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                    <th style="text-align: center;">No.</th>
                      <th style="text-align: center;">No. Pendaftaran</th>
                      <th style="text-align: center;">NISN</th>
                      <th style="text-align: center;">Nama Lengkap</th>
                      <th style="text-align: center;">Umur</th>
                      <th style="text-align: center;">Status Seleksi</th>
                      <th style="text-align: center;">Status Review</th>
                      <!-- <th style="width: 110px; text-align: center;">Aksi</th> -->
                    </tr>
                  </thead>
                  <tbody><?php $no = 1;
                    if (count($seleksiList) == 0) { ?>
                      <tr>
                        <td colspan="6" style="text-align: center;">Data Masih Kosong ...</td>
                      </tr><?php
                    } else {
                      foreach ($seleksiList as $data) {
                        $path = null;
                        $calonSiswaId = null;
                        $seleksi = array('seleksi' => $data);
                        foreach ($pendaftaranList as $pendaftaran) {
                          if ($pendaftaran->no_pendaftaran == $data->no_pendaftaran) {
                            $calonSiswaId = $pendaftaran->calon_siswa_id;
                          }
                        }

                        $nisn = "-";
                        $namaLengkap = "-";
                        if (!empty($calonSiswaList)) {
                          if ($calonSiswaId != null) {
                            foreach ($calonSiswaList as $calonSiswa) {
                              if ($calonSiswa->id_calon_siswa == $calonSiswaId) {
                                $nisn = $calonSiswa->nisn;
                                $namaLengkap = $calonSiswa->nama_lengkap;
                              }
                            }
                          }
                        }
                         ?>
                        <tr>
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $data->no_pendaftaran ?></td>
                          <td><?php echo $nisn ?></td>
                          <td><?php echo $namaLengkap ?></td>
                          <td><?php echo $data->umur ?></td>
                          <td><?php echo $data->status_diterima_atau_ditolak ?></td>
                          <td><?php echo $data->status_review ?></td>
                          <?php if ($needSeleksi) { ?>
                            <td>
                              <div class="margin">
                                <div class="btn-group">
                                  <button type="button" class="btn btn-info">Aksi</button>
                                  <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                    <span class="sr-only">Toggle Dropdown</span>
                                  </button>
                                  <div class="dropdown-menu" role="menu">
                                    <button type="button" class="btn btn-block btn-light btn-m text-left" data-toggle="modal" data-target="#modalTerima<?php echo $data->no_pendaftaran ?>">Terima</button>
                                    <button type="button" class="btn btn-block btn-light btn-m text-left" data-toggle="modal" data-target="#modalPending<?php echo $data->no_pendaftaran ?>">Pending</button>
                                    <button type="button" class="btn btn-block btn-light btn-m text-left" data-toggle="modal" data-target="#modalTolak<?php echo $data->no_pendaftaran ?>">Tolak</button>
                                  </div>
                                </div>
                              </div>
                            </td><?php
                          } ?>
                        </tr>
                        <?php $this->load->view('seleksi/modalpending', $seleksi);
                        $this->load->view('seleksi/modalterima', $seleksi);
                        $this->load->view('seleksi/modaltolak', $seleksi);
                      }
                    } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <button type="submit" class="btn btn-primary swalDefaultSuccess" style="visibility: hidden">success</button>
  <button type="submit" class="btn btn-primary swalDefaultError" style="visibility: hidden">failed</button>
  <button type="submit" class="btn btn-primary swalDefaultWarning" style="visibility: hidden">warning</button>

<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');

  if ($this->session->flashdata('success')) {
    echo $this->session->flashdata('success');
    unset($_SESSION['success']);
  }
  
  if ($this->session->flashdata('failed')) {
    echo $this->session->flashdata('failed');
    unset($_SESSION['failed']);
  }
  
  if ($this->session->flashdata('warning')) {
    echo $this->session->flashdata('warning');
    unset($_SESSION['warning']);
  }
?>
