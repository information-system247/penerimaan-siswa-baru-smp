<!-- modal -->
<div class="modal fade" id="modalTolak<?php echo $seleksi->no_pendaftaran ?>">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title">Konfirmasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="needs-validation" action="<?php echo base_url('master/seleksi/seleksipersonal') ?>" method="POST" novalidate>
        <div class="modal-body">
          <input type="text" id="noPendaftaran" name="noPendaftaran" value="<?php echo $seleksi->no_pendaftaran ?>" hidden>
          <input type="text" id="statusSeleksi" name="statusSeleksi" value="<?php echo StatusSeleksiConstant::TOLAK ?>" hidden>
          <?php if (!empty($seleksi->catatan) && $seleksi->status_diterima_atau_ditolak == StatusSeleksiConstant::TOLAK) { ?>
            <input type="text" class="form-control" id="catatan" name="catatan" placeholder="masukan catatan" value="<?php echo $seleksi->catatan ?>" required><?php
          } else {?>
            <input type="text" class="form-control" id="catatan" name="catatan" placeholder="masukan catatan" required><?php
          }?>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
