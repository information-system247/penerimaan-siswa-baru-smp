<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $activeMenu);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Panitia</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Master</a></li>
              <li class="breadcrumb-item active">Panitia</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-2">
            <div class="card">
              <a href="<?php echo base_url('master/panitiapendaftaran/add') ?>" type="button" class="btn btn-block btn-primary">Tambah</a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="dataTable" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th style="text-align: center;">No</th>
                      <th style="text-align: center;">NIK</th>
                      <th style="text-align: center;">Nama Lengkap</th>
                      <th style="text-align: center;">Gender</th>
                      <th style="text-align: center;">Email</th>
                      <th style="text-align: center;">Username</th>
                      <th style="text-align: center;">Telepon</th>
                      <th style="text-align: center;">Hak Akses</th>
                      <!-- <th style="width: 80px; text-align: center;">Aksi</th> -->
                    </tr>
                  </thead>
                  <tbody><?php $no = 1;
                    if (count($panitiaList) == 0) { ?>
                      <tr>
                        <td colspan="10" style="text-align: center;">Data Masih Kosong ...</td>
                      </tr><?php
                    } else {
                      foreach ($panitiaList as $data) {
                        $panitiaId = array('id' => $data->id_admin_pendaftaran); ?>
                        <tr>
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $data->nik ?></td>
                          <td><?php echo $data->nama ?></td>
                          <td><?php echo $data->gender ?></td>
                          <td><?php echo $data->email ?></td>
                          <td><?php echo $data->username ?></td>
                          <td><?php echo $data->no_hp ?></td>
                          <td><?php echo $data->hak_akses ?></td>
                          <!-- <td>
                            <div class="row">
                              <div class="col-6">
                                <a type="submit" href="<?php echo base_url('master/panitiapendaftaran/get/'. $data->id_admin_pendaftaran) ?>" class="btn btn-block bg-gradient-warning btn btn-sm"><i class="nav-icon fas fa-pen"></i></a>
                              </div>
                              <div class="col-6">
                                <button type="button" class="btn btn-block bg-gradient-danger btn btn-sm" data-toggle="modal" data-target="#modalDelete<?php echo $data->id_admin_pendaftaran ?>"><i class="nav-icon fas fa-trash"></i></button>
                              </div>
                            </div>
                          </td> -->
                        </tr>
                        <?php $this->load->view('panitia/delete', $panitiaId);
                      }
                    } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <button type="submit" class="btn btn-primary swalDefaultSuccess" style="visibility: hidden">success</button>
  <button type="submit" class="btn btn-primary swalDefaultError" style="visibility: hidden">failed</button>
  <button type="submit" class="btn btn-primary swalDefaultWarning" style="visibility: hidden">warning</button>

<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');

  if ($this->session->flashdata('success')) {
    echo $this->session->flashdata('success');
    unset($_SESSION['success']);
  }
  
  if ($this->session->flashdata('failed')) {
    echo $this->session->flashdata('failed');
    unset($_SESSION['failed']);
  }
  
  if ($this->session->flashdata('warning')) {
    echo $this->session->flashdata('warning');
    unset($_SESSION['warning']);
  }
?>
