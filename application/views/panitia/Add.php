<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $activeMenu);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Panitia</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Master</a></li>
              <li class="breadcrumb-item active">Panitia</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Panitia</h3>
              </div>
              <!-- /.card-header -->
              <form id="formPanitia" method="post" action="<?php echo base_url('master/panitiapendaftaran/store') ?>">
                <div class="card-body">
                  <input type="text" id="id" name="id" hidden>
                  <div class="form-group">
                    <label for="nik">NIK</label>
                    <input type="text" class="form-control" id="nik" name="nik" placeholder="masukan nik" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="25" required>
                  </div>
                  <div class="form-group">
                    <label for="namaLengkap">Nama lengkap</label>
                    <input type="text" class="form-control" id="namaLengkap" name="namaLengkap" placeholder="masukan nama lengkap" onkeypress="return event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 39 || event.charCode == 32 || event.charCode == 96" maxlength="50" required>
                  </div>
                  <div class="form-group">
                    <label for="gender">Jeni Kelamin</label>
                    <div class="row">
                      <div class="col-sm-2">
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="customRadio1" name="gender" value="L" checked>
                          <label for="customRadio1" class="custom-control-label">Laki - Laki</label>
                        </div>
                      </div>
                      <div class="col-sm-2">
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="customRadio2" name="gender" value="P">
                          <label for="customRadio2" class="custom-control-label">Perempuan</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan email address" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode >= 65 && event.charCode <= 90 || event.charCode >= 97 && event.charCode <= 122 || event.charCode == 46 || event.charCode == 64" maxlength="50" required>
                  </div>
                  <div class="form-group">
                    <label for="telepon">Telepon</label>
                    <input type="text" class="form-control" id="telepon" name="telepon" placeholder="masukan nomor telepon" minlength="12" maxlength="15" onkeypress="return event.charCode >= 48 && event.charCode <=57" required>
                  </div>
                  <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" placeholder="Masukkan username" maxlength="50" required>
                  </div>
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan password" maxlength="50" required>
                  </div>
                  <div class="form-group">
                    <label for="hakAkses">Hak Akses</label>
                    <div class="row">
                      <div class="col-sm-2">
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="customRadio1" name="hakAkses" value="ADMIN">
                          <label for="customRadio1" class="custom-control-label">Admin</label>
                        </div>
                      </div>
                      <div class="col-sm-2">
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="customRadio2" name="hakAkses" value="PANITIA" checked>
                          <label for="customRadio2" class="custom-control-label">Panitia</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');
?>