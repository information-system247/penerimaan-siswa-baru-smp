<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $activeMenu);
  $this->load->view('beranda/modal/pendaftar');
  $this->load->view('beranda/modal/lolosseleksi');
  $this->load->view('beranda/modal/tidaklolosseleksi')
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Beranda</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Beranda</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user-graduate"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Pendaftar Tahun Ajaran <?php echo $tahunAjaran; ?></span>
                <span class="info-box-number"><?php echo $totalResultPendaftaran; ?> Pendaftar</span>
                <span><a href="#modalPendaftarInBeranda" class="small-box-footer float-right" data-toggle="modal"><i class="fas fa-arrow-circle-right"></i></a></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fas fa-user-graduate"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Lolos Tahun Ajaran <?php echo $tahunAjaran; ?></span>
                <span class="info-box-number"><?php echo $totalResultStatusSeleksiLolos; ?> Pendaftar</span>
                <span><a href="#modalLolosSeleksiInBeranda" class="small-box-footer float-right" data-toggle="modal"><i class="fas fa-arrow-circle-right"></i></a></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-user-graduate"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Tidak Lolos Tahun Ajaran <?php echo $tahunAjaran; ?></span>
                <span class="info-box-number"><?php echo $totalResultSTatusSeleksiTidakLolos; ?> Pendaftar</span>
                <span><a href="#modalTidakLolosSeleksiInBeranda" class="small-box-footer float-right" data-toggle="modal"><i class="fas fa-arrow-circle-right"></i></a></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Permintaan & Retur Produk</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Rendering engine</th>
                      <th>Browser</th>
                      <th>Platform(s)</th>
                      <th>Engine version</th>
                      <th>CSS grade</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Trident</td>
                      <td>Internet
                        Explorer 4.0
                      </td>
                      <td>Win 95+</td>
                      <td> 4</td>
                      <td>X</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <button type="submit" class="btn btn-primary swalDefaultSuccess" style="visibility: hidden">success</button>

<?php
  //$this->load->view('base_template/footer');
  $this->load->view('base_template/script');

  if ($this->session->flashdata('success')) {
    echo $this->session->flashdata('success');
    unset($_SESSION['success']);
  }
?>


<!-- modal -->
<div class="modal fade" id="modalKonfigurasi">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title">Konfirmasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="needs-validation" action="<?php echo base_url('master/konfigurasi/store') ?>" method="POST" novalidate>
        <div class="modal-body">
          <div class="form-group">
            <label for="tahunAjaran">Tahun Ajaran</label>
            <?php if (!empty($konfigurasiList[0]->module == ModuleConstant::TAHUN_AJARAN)) {?>
              <input type="text" class="form-control" id="tahunAjaran" name="tahunAjaran" value="<?php echo $konfigurasiList[0]->value ?>" placeholder="masukan tahun ajaran" onkeypress="return code.event >= 48 && code.event <= 57" maxlength="4" required><?php
            } else {?>
              <input type="text" class="form-control" id="tahunAjaran" name="tahunAjaran" placeholder="masukan tahun ajaran" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="4" required><?php
            }?>
          </div>
          <div class="form-group">
            <label for="jangkaWaktuPendaftaran">Batas Waktu Pendaftaran</label>
            <?php if (!empty($konfigurasiList[1]->module == ModuleConstant::BATAS_PENDAFTARAN)) {?>
              <input type="date" class="form-control" id="jangkaWaktuPendaftaran" name="jangkaWaktuPendaftaran" value="<?php echo $konfigurasiList[1]->value ?>" required><?php
            } else {?>
              <input type="date" class="form-control" id="jangkaWaktuPendaftaran" name="jangkaWaktuPendaftaran" required><?php
            }?>
            
          </div>
          <div class="form-group">
            <label for="jangkaWaktuSeleksi">Pengumuman Seleksi</label>
            <?php if (!empty($konfigurasiList[2]->module == ModuleConstant::PENGUMUMAN_SELEKSI)) {?>
              <input type="date" class="form-control" id="jangkaWaktuSeleksi" name="jangkaWaktuSeleksi" value="<?php echo $konfigurasiList[2]->value ?>" required><?php
            } else {?>
              <input type="date" class="form-control" id="jangkaWaktuSeleksi" name="jangkaWaktuSeleksi" required><?php
            }?>
          </div>
          <div class="form-group">
            <label for="jangkaWaktuDaftarUlang">Batas Waktu Daftar Ulang</label>
            <?php if (!empty($konfigurasiList[3]->module == ModuleConstant::BATAS_DAFTAR_ULANG)) {?>
              <input type="date" class="form-control" id="jangkaWaktuDaftarUlang" name="jangkaWaktuDaftarUlang" value="<?php echo $konfigurasiList[3]->value ?>" required><?php
            } else {?>
              <input type="date" class="form-control" id="jangkaWaktuDaftarUlang" name="jangkaWaktuDaftarUlang" required><?php
            }?>
          </div>
          <div class="form-group">
            <label for="zonasi">Zonasi</label>
            <?php if (!empty($konfigurasiList[4]->module == ModuleConstant::ZONASI)) {?>
              <input type="text" class="form-control" id="zonasi" name="zonasi" value="<?php echo $konfigurasiList[4]->value ?>" required><?php
            } else {?>
              <input type="text" class="form-control" id="zonasi" name="zonasi" required><?php
            }?>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
