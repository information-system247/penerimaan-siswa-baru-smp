<!-- modal -->
<div class="modal fade" id="modalDetail<?php echo $daftarUlang->id_daftar_ulang ?>">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-info">
        <h4 class="modal-title">Data Daftar Ulang Calon Siswa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="needs-validation" action="<?php echo base_url('master/daftarulang/approved') ?>" method="POST">
        <div class="modal-body">
          <input class="form-control" type="text" name="id" id="id" value="<?php echo $daftarUlang->id_daftar_ulang ?>" hidden>
          <div class="form-group">
            <label for="tglDaftarUlang">Tanggal Daftar Ulang</label>
            <input type="text" class="form-control" id="tglDaftarUlang" name="tglDaftarUlang" value="<?php echo $daftarUlang->tgl_daftar_ulang ?>" disabled>
          </div>
          <div class="form-group">
            <label for="NoPendaftaran">No. Pendaftaran</label>
            <input type="text" class="form-control" id="NoPendaftaran" name="NoPendaftaran" value="<?php echo $daftarUlang->no_pendaftaran ?>" disabled>
          </div>
          <div class="form-group">
            <label for="namaLengkap">Nama Lengkap</label>
            <input type="text" class="form-control" id="namaLengkap" name="namaLengkap" value="<?php echo $daftarUlang->nama_lengkap ?>" disabled>
          </div>
          <div class="form-group">
            <label for="gender">Gender</label>
            <input type="text" class="form-control" id="gender" name="gender" value="<?php echo $daftarUlang->gender ?>" disabled>
          </div>
          <div class="form-group">
            <label for="nisn">NISN</label>
            <input type="text" class="form-control" id="nisn" name="nisn" value="<?php echo $daftarUlang->nisn ?>" disabled>
          </div>
          <div class="form-group">
            <label for="nikSiswa">Nik Calon Siswa</label>
            <input type="text" class="form-control" id="nikSiswa" name="nikSiswa" value="<?php echo $daftarUlang->nik_siswa ?>" disabled>
          </div>
          <div class="form-group">
            <label for="tempatTanggalLahir">Tempat Tanggal Lahir Calon Siswa</label>
            <input type="text" class="form-control" id="tempatTanggalLahir" name="tempatTanggalLahir" value="<?php echo $daftarUlang->tmp_lhr_siswa ?>" disabled>
          </div>
          <div class="form-group">
            <label for="tanggalLahir">Tanggal Lahir Calon Siswa</label>
            <input type="text" class="form-control" id="tanggalLahir" name="tanggalLahir" value="<?php echo $daftarUlang->tgl_lhr_siswa ?>" disabled>
          </div>
          <div class="form-group">
            <label for="tanggalLahir">Tanggal Lahir Calon Siswa</label>
            <input type="text" class="form-control" id="tanggalLahir" name="tanggalLahir" value="<?php echo $daftarUlang->tgl_lhr_siswa ?>" disabled>
          </div>
          <div class="form-group">
            <label for="agama">Agama</label>
            <input type="text" class="form-control" id="agama" name="agama" value="<?php echo $daftarUlang->agama ?>" disabled>
          </div>
          <div class="form-group">
            <label for="kewarganegaraan">Kewarganegaraan</label>
            <input type="text" class="form-control" id="kewarganegaraan" name="kewarganegaraan" value="<?php echo $daftarUlang->kewarganegaan ?>" disabled>
          </div>
          <div class="form-group">
            <label for="alamat">Alamat</label>
            <input type="text" class="form-control" id="alamat" name="alamat" value="<?php echo $daftarUlang->alamat ?>" disabled>
          </div>
          <div class="form-group">
            <label for="rtrw">RT/RW</label>
            <input type="text" class="form-control" id="rtrw" name="rtrw" value="<?php echo $daftarUlang->rt.'/'.$daftarUlang->rw ?>" disabled>
          </div>
          <div class="form-group">
            <label for="namaDusun">Nama Dusun</label>
            <input type="text" class="form-control" id="namaDusun" name="namaDusun" value="<?php echo $daftarUlang->nama_dusun ?>" disabled>
          </div>
          <div class="form-group">
            <label for="namaKelurahan">Nama Kelurahan</label>
            <input type="text" class="form-control" id="namaKelurahan" name="namaKelurahan" value="<?php echo $daftarUlang->nama_kelurahan ?>" disabled>
          </div>
          <div class="form-group">
            <label for="kecapatan">Kecamatan</label>
            <input type="text" class="form-control" id="kecapatan" name="kecapatan" value="<?php echo $daftarUlang->kecamatan ?>" disabled>
          </div>
          <div class="form-group">
            <label for="kabupaten">Kabupaten</label>
            <input type="text" class="form-control" id="kabupaten" name="kabupaten" value="<?php echo $daftarUlang->kabupaten ?>" disabled>
          </div>
          <div class="form-group">
            <label for="kodepos">Kode Pos</label>
            <input type="text" class="form-control" id="kodepos" name="kodepos" value="<?php echo $daftarUlang->kode_pos ?>" disabled>
          </div>
          <div class="form-group">
            <label for="namaAyah">Nama Ayah</label>
            <input type="text" class="form-control" id="namaAyah" name="namaAyah" value="<?php echo $daftarUlang->nama_ayah ?>" disabled>
          </div>
          <div class="form-group">
            <label for="nikAyah">NIK Ayah</label>
            <input type="text" class="form-control" id="nikAyah" name="nikAyah" value="<?php echo $daftarUlang->nik_ayah ?>" disabled>
          </div>
          <div class="form-group">
            <label for="tahunLahirAyah">Tahun Lahir Ayah</label>
            <input type="text" class="form-control" id="tahunLahirAyah" name="tahunLahirAyah" value="<?php echo $daftarUlang->thn_lhr_ayah ?>" disabled>
          </div>
          <div class="form-group">
            <label for="namaIbu">Nama Ibu</label>
            <input type="text" class="form-control" id="namaIbu" name="namaIbu" value="<?php echo $daftarUlang->nama_ibu ?>" disabled>
          </div>
          <div class="form-group">
            <label for="nikIbu">NIK Ibu</label>
            <input type="text" class="form-control" id="nikIbu" name="nikIbu" value="<?php echo $daftarUlang->nik_ibu ?>" disabled>
          </div>
          <div class="form-group">
            <label for="tahunLahirIbu">Tahun Lahir Ibu</label>
            <input type="text" class="form-control" id="tahunLahirIbu" name="tahunLahirIbu" value="<?php echo $daftarUlang->thn_lhr_ibu ?>" disabled>
          </div>
          <div class="form-group">
            <label for="noHp">Telepon</label>
            <input type="text" class="form-control" id="noHp" name="noHp" value="<?php echo $daftarUlang->no_hp_ortu ?>" disabled>
          </div>
          <div>
            <label for="statusApproved">Status Approved</label>
            <select class="form-control" id="statusApproved" name="statusApproved">
              <?php if ($daftarUlang->status_approved == StatusApprovedConstant::APPROVED) { ?>
                <option value="<?php echo StatusApprovedConstant::APPROVED ?>" selected><?php echo StatusApprovedConstant::APPROVED ?></option><?php
              } else { ?>
                <option value="<?php echo StatusApprovedConstant::APPROVED ?>"><?php echo StatusApprovedConstant::APPROVED ?></option><?php
              }

              if ($daftarUlang->status_approved == StatusApprovedConstant::PENDING) { ?>
                <option value="<?php echo StatusApprovedConstant::PENDING ?>" selected><?php echo StatusApprovedConstant::PENDING ?></option><?php
              } else { ?>
                <option value="<?php echo StatusApprovedConstant::PENDING ?>"><?php echo StatusApprovedConstant::PENDING ?></option><?php
              } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="catatan">Catatan</label>
            <?php if ($daftarUlang->status_approved == StatusApprovedConstant::APPROVED) {?>
              <input type="text" class="form-control" id="catatan" name="catatan" value="<?php echo $daftarUlang->catatan ?>" disabled><?php
            } else if(!empty($daftarUlang->catatan)){ ?>
              <input type="text" class="form-control" id="catatan" name="catatan" value="<?php echo $daftarUlang->catatan ?>" required><?php
            } else {?>
              <input type="text" class="form-control" id="catatan" name="catatan" required><?php
            }?>
          </div>
          <div class="form-group">
            <div class="row mt-2">
              <div class="col d-flex align-items-center">
                <p class="mb-0">
                  <span class="lead" data-dz-name><?php echo $daftarUlang->upload_dokumen ?></span>
                </p>
              </div>
              <div class="col-7 d-flex align-items-center">
                <div class="progress progress-striped active w-100" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                  <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                </div>
              </div>
              <div class="col-auto d-flex align-items-center">
                <div class="btn-group">
                  <?php $path = 'assert/dokumen/daftar_ulang/'.$pendaftaran->tahun_ajaran.'/'.$daftarUlang->upload_dokumen;?>
                  <a href="<?php echo base_url($path) ?>" class="submit btn btn-success">
                    <i class="fas fa-download mr-2"></i>Download
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <?php if ($daftarUlang->status_review == 'REVIEWED' && $daftarUlang->status_approved == StatusApprovedConstant::APPROVED) {?>
            <button type="submit" class="btn btn-primary" disabled>Simpan</button><?php
          } else {?>
            <button type="submit" class="btn btn-primary">Simpan</button><?php
          }?>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
