<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $activeMenu);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Pendaftaran</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Master</a></li>
              <li class="breadcrumb-item active">Pendaftaran</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Pendaftaran</h3>
              </div>
              <!-- /.card-header -->
              <form id="formPendaftaran" method="post" action="<?php echo base_url('master/pendaftaran/store') ?>" enctype="multipart/form-data">
                <div class="card-body">
                  <input type="text" id="id" name="id" hidden>
                  <div class="form-group">
                    <label for="noPendaftaran">Nomor Pendaftaran</label>
                    <input type="text" class="form-control" id="noPendaftaran" name="noPendaftaran" value="<?php echo $noPendaftaran ?>" required disabled>
                  </div>
                  <div class="form-group">
                    <label for="tanggalDaftar">Tanggal Daftar</label>
                    <input type="text" class="form-control" id="tanggalDaftar" name="tanggalDaftar" value="<?php echo $tanggalDaftar ?>" required disabled>
                  </div>
                  <div class="form-group">
                    <label for="tahunAjaran">Tahun Ajaran</label>
                    <input type="text" class="form-control" id="tahunAjaran" name="tahunAjaran" value="<?php echo $tahunAjaran ?>" required disabled>
                  </div>
                  <div class="form-group">
                    <label for="noIjazah">Nomor Ijazah</label>
                    <input type="text" class="form-control" id="noIjazah" name="noIjazah" placeholder="Masukkan nomor ijazah" onkeypress="return event.charCode >= 48 && event.charCode <= 57 || event.charCode >= 65 && event.charCode <= 90" minlength="20" maxlength="20" required>
                  </div>
                  <div class="form-group">
                    <label for="calonSiswaId">Calon Siswa</label>
                    <select class="form-control" id="calonSiswaId" name="calonSiswaId"><?php 
                      foreach ($calonSiswaList as $data) { ?>
                        <option value="<?php echo $data->id_calon_siswa ?>"><?php echo $data->nisn.' - '.$data->nama_lengkap ?></option><?php
                      } ?>
                    </select>
                  </div>
                  <div class="form-group">
                      <label for="uploadIjazah">Unggah Ijazah</label>
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="uploadIjazah" name="uploadIjazah" accept=".pdf" required>
                        <label for="uploadIjazah" class="custom-file-label">Pilih File</label>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="uploadAkta">Unggah Akta Kelahiran</label>
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="uploadAkta" name="uploadAkta" accept=".pdf" required>
                        <label for="uploadAkta" class="custom-file-label">Pilih File</label>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="uploadKk">Unggah Karta Keluarga</label>
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="uploadKk" name="uploadKk" accept=".pdf" required>
                        <label for="uploadKk" class="custom-file-label">Pilih File</label>
                      </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');
?>