<?php 
  $this->load->view('base_template/head');
  $this->load->view('base_template/sidebar', $activeMenu);
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Berkas Pendaftar</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Master</a></li>
              <li class="breadcrumb-item active">Pendaftaran</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-2">
            <div class="card">
              <a href="<?php echo base_url('master/pendaftaran/add') ?>" type="button" class="btn btn-block btn-primary">Tambah</a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="dataTable" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th style="text-align: center;">No</th>
                      <th style="text-align: center;">Tahun Ajaran</th>
                      <th style="text-align: center;">No. Pendaftaran</th>
                      <th style="text-align: center;">Tanggal Daftar</th>
                      <th style="text-align: center;">Nama Lengkap</th>
                      <th style="text-align: center;">Gender</th>
                      <th style="text-align: center;">No. Ijazah</th>
                      <!-- <th style="width: 80px; text-align: center;">Aksi</th> -->
                    </tr>
                  </thead>
                  <tbody><?php $no = 1;
                    if (count($pendaftaranList) == 0) { ?>
                      <tr>
                        <td colspan="8" style="text-align: center;">Data Masih Kosong ...</td>
                      </tr><?php
                    } else {
                      foreach ($pendaftaranList as $data) {
                        $pendaftaranId = array('id' => $data->id_pendaftaran); 
                        $namaLengkap = "-";
                        $gender = "-";
                        if (!empty($calonSiswaList)) {
                          foreach ($calonSiswaList as $calonSiswa) {
                            $namaLengkap = $calonSiswa->nama_lengkap;
                            $gender = $calonSiswa->gender;
                          }
                        }?>
                        <tr>
                          <td><?php echo $no++; ?></td>
                          <td><?php echo $data->tahun_ajaran ?></td>
                          <td><?php echo $data->no_pendaftaran ?></td>
                          <td><?php echo $data->tanggal_daftar ?></td>
                          <td><?php echo $namaLengkap ?></td>
                          <td><?php echo $gender ?></td>
                          <td><?php echo $data->no_ijazah ?></td>
                          <!-- <td>
                            <div class="row">
                              <div class="col-6">
                                <a type="submit" href="<?php echo base_url('master/pendaftaran/get/'. $data->id_pendaftaran) ?>" class="btn btn-block bg-gradient-warning btn btn-m"><i class="nav-icon fas fa-pen"></i></a>
                              </div>
                              <div class="col-6">
                                <button type="button" class="btn btn-block bg-gradient-danger btn btn-m" data-toggle="modal" data-target="#modalDelete<?php echo $data->id_pendaftaran ?>"><i class="nav-icon fas fa-trash"></i></button>
                              </div>
                            </div>
                          </td> -->
                        </tr>
                        <?php $this->load->view('pendaftaran/delete', $pendaftaranId);
                      }
                    } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <button type="submit" class="btn btn-primary swalDefaultSuccess" style="visibility: hidden">success</button>
  <button type="submit" class="btn btn-primary swalDefaultError" style="visibility: hidden">failed</button>
  <button type="submit" class="btn btn-primary swalDefaultWarning" style="visibility: hidden">warning</button>

<?php
  $this->load->view('base_template/footer');
  $this->load->view('base_template/script');

  if ($this->session->flashdata('success')) {
    echo $this->session->flashdata('success');
    unset($_SESSION['success']);
  }
  
  if ($this->session->flashdata('failed')) {
    echo $this->session->flashdata('failed');
    unset($_SESSION['failed']);
  }
  
  if ($this->session->flashdata('warning')) {
    echo $this->session->flashdata('warning');
    unset($_SESSION['warning']);
  }
?>
