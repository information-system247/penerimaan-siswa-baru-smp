<?php 

class StatusApprovedConstant
{
  const APPROVED = "APPROVED";
  const WAITING_APPROVED = "WAITING_APPROVED";
  const PENDING = "PENDING";
}
