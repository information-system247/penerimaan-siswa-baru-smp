<?php 

class PpdbUtils
{
  public static function getGender($gender)
  {
    if (GenderConstant::PRIA == $gender) {
      return "Laki-Laki";
    } else if (GenderConstant::WANITA == $gender) {
      return "Perempuan";
    }
    
    return "-";
  }
}
