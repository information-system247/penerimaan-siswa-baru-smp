<?php 

class StatusReviewConstant
{
  const REVIEWED = "REVIEWED";
  const WAITING_REVIEW = "WAITING_REVIEW";
}
