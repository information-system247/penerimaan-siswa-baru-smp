<?php 

class ModuleConstant
{
  const TAHUN_AJARAN = "Tahun Ajaran";
  const BATAS_PENDAFTARAN = "Batas Waktu Pendaftaran";
  const PENGUMUMAN_SELEKSI = "Pengumuman Seleksi";
  const BATAS_DAFTAR_ULANG = "Batas Waktu Daftar Ulang";
  const ZONASI = "Zonasi";
}
