<?php
class MenuConstant
{
  const BERANDA = "Beranda";
  const PANITIA = "panitia";
  const PENDAFTARAN = "Pendaftaran";
  const CALON_SISWA = "CalonSiswa";
  const SELEKSI = "Seleksi";
  const DAFTAR_ULANG = "Daftar Ulang";
  const KONFIGURASI = 'Konfigurasi';
  const DATA_DIRI = "Data Diri";
  const LAPORAN_PENDAFTARAN = "Laporan Pendfatran";
  const LAPORAN_SELEKSI_TIDAK_DITERIMA = "Laporan Seleksi Tidak Diterima";
  const LAPORAN_SELEKSI_DITERIMA = "Laporan Seleksi Diterima";
  const LAPORAN_DAFTAR_ULANG = "Laporan Daftar Ulang";
  const LOGOUT = "Logout";
}
