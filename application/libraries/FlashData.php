<?php

class FlashData
{
  public static function successSaved()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultSuccess').click(function() {
                Toast.fire({
                  icon: 'success',
                  title: 'Berhasil simpan data.'
                })
            });
            $('.swalDefaultSuccess').trigger('click');
        });
      </script>";
  }

  public static function failedSaved()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: 'Gagal simpan data.'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }

  public static function dataNotFound()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultWarning').click(function() {
                Toast.fire({
                  icon: 'warning',
                  title: 'Data tidak ditemukan.'
                })
            });
            $('.swalDefaultWarning').trigger('click');
        });
      </script>";
  }

  public static function deletedSuccess()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultSuccess').click(function() {
                Toast.fire({
                  icon: 'success',
                  title: 'Berhasil hapus data.'
                })
            });
            $('.swalDefaultSuccess').trigger('click');
        });
      </script>";
  }

  public static function deletedFailed()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: 'Gagal hapus data.'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }

  public static function loginFailedUsernameWrong()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: 'Gagal login. Username anda salah.'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }

  public static function loginFailedPasswordWrong()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: 'Gagal login. Password anda salah.'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }

  public static function loginSuccess()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: 'berhasil login.'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }

  public static function nikAlreadyExist()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: 'Gagal simpan data. Nik sudah digunakan'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }

  public static function emailAlreadyExist()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: 'Gagal simpan data. Email sudah digunakan'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }

  public static function phoneNumberAlreadyExist()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: 'Gagal simpan data. Nomor hp sudah digunakan'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }

  public static function usernameAlreadyExist()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: 'Gagal simpan data. Nama pengguna sudah digunakan'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }

  public static function hakAksesAlreadyExist()
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: 'Gagal simpan data. Hak akses sudah digunakan'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }

  public static function failedDeleteData($message)
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: '".$message."'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }

  public static function DataFound($message)
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: '".$message."'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }

  public static function generalError($message)
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultError').click(function() {
                Toast.fire({
                  icon: 'error',
                  title: '".$message."'
                })
            });
            $('.swalDefaultError').trigger('click');
        });
      </script>";
  }

  public static function success($message)
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultSuccess').click(function() {
                Toast.fire({
                  icon: 'success',
                  title: '".$message."'
                })
            });
            $('.swalDefaultSuccess').trigger('click');
        });
      </script>";
  }

  public static function warning($message)
  {
    return "
      <script>
        $(function() {
            var Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000
            });

            $('.swalDefaultWarning').click(function() {
                Toast.fire({
                  icon: 'warning',
                  title: '".$message."'
                })
            });
            $('.swalDefaultWarning').trigger('click');
        });
      </script>";
  }
}