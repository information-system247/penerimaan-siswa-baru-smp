<?php 

class DataDiri extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('CalonSiswaModel', 'calonSiswa');
    $this->load->model('PendaftaranModel', 'pendaftaran');
    $this->load->model('StatusSeleksiPendaftaranCalonSiswaModel', 'statusSeleksi');
    $this->load->model('KonfigurasiModel', 'konfigurasi');
  }

  public function isLogin()
  {
    if ($this->session->userdata('status') == LoginSessionConstant::SISWA) {
      return true;
    }
    redirect('siswa/login');
  }

  public function index()
  {
    if ($this->isLogin()) {
      $konfigOptional = $this->konfigurasi->findByModule(ModuleConstant::TAHUN_AJARAN);
      $calonSiswaId = $this->session->userdata('id');
      $pendaftaranOptional = $this->pendaftaran->findByTahunAjaranAndCalonSiswaId($konfigOptional[0]->value, $calonSiswaId);
      $statusSeleksiOptional = null;
      if (!empty($pendaftaranOptional)) {
        $statusSeleksiOptional = $this->statusSeleksi->findByNoPendaftaran($pendaftaranOptional[0]->no_pendaftaran);
        if (!empty($statusSeleksiOptional)) {
          $batasSeleksi = $statusSeleksiOptional[0]->batas_waktu_seleksi;
          $batasSeleksiLong = strtotime($batasSeleksi);
          $dateNowLong = strtotime(Date('d-m-Y'));
          $selisih = $batasSeleksiLong - $dateNowLong;
          if ($selisih >= 0) {
            $statusSeleksiOptional = null;
          }
        }
      }

      $calonSiswaOptional = $this->calonSiswa->findById($calonSiswaId);
      $konfigBatasPendaftaran = $this->konfigurasi->findByModule(ModuleConstant::BATAS_PENDAFTARAN);
      $validUpdateData = false;
      $batasPendaftaranLong = strtotime($konfigBatasPendaftaran[0]->value);
      $dateNowLong = strtotime(Date('d-m-Y'));
      $selisih = $batasPendaftaranLong - $dateNowLong;
      if ($selisih >= 0) {
        $statusSeleksiUser = null;
        if (!empty($pendaftaranOptional)) {
          $statusSeleksiUser = $this->statusSeleksi->findByNoPendaftaran($pendaftaranOptional[0]->no_pendaftaran);
        }
        if (!empty($statusSeleksiUser)) {
          $statusSeleksi = $statusSeleksiUser[0]->status_diterima_atau_ditolak;
          if ($statusSeleksi == 'TERIMA') {
            $validUpdateData = false;
          }
        } else {
          $validUpdateData = true;
        }
      }

      $pengumumanSeleksiKonfig = $this->konfigurasi->findByModule(ModuleConstant::PENGUMUMAN_SELEKSI);
      $viewPengumumanSeleksi = false;
      if (!empty($pengumumanSeleksiKonfig)) {
        $pengumumanSeleksiKonfigLong = strtotime($pengumumanSeleksiKonfig[0]->value);
        if ($pengumumanSeleksiKonfigLong <= $dateNowLong) {
          $viewPengumumanSeleksi = true;
        }
      }

      $data = array(
          "activeMenu" => MenuConstant::DATA_DIRI,
          'statusSeleksiOptional' => $statusSeleksiOptional,
          'calonSiswa' => $calonSiswaOptional,
          'validUpdateData' => $validUpdateData,
          'pengumumanSeleksi' => $viewPengumumanSeleksi
      );
      
      $this->load->view('siswa/datadiri', $data);
    }
  }

  public function store()
  {
    if ($this->isLogin()) {
      $id = $this->input->post('id');
      $namaLengkap = $this->input->post('namaLengkap');
      $gender = $this->input->post('gender');
      $tempatLahir = $this->input->post('tempatLahir');
      $tanggalLahir = $this->input->post('tanggalLahir');
      $alamat = $this->input->post('alamat');
      $kecamatan = $this->input->post('kecamatan');
      $kabupaten = $this->input->post('kabupaten');
      $agama = $this->input->post('agama');
      $noHp = $this->input->post('telepon');
      $username = $this->input->post('username');
      $password = $this->encryption->encrypt($this->input->post('password'));
      $namaOrtu = $this->input->post('namaOrtu');
      $nisn = $this->input->post('nisn');

      $calonSiswaOptional = $this->calonSiswa->findByUsername($username);
      $siswaOptional = $this->calonSiswa->findById($id);
      if ($siswaOptional[0]->username != $username && !empty($calonSiswaOptional)) {
        $this->session->set_flashdata('failed', FlashData::usernameAlreadyExist());
        redirect('siswa/datadiri');
      }

      $siswaOptional2 = $this->calonSiswa->findByNisn($nisn);
      if (!empty($siswaOptional2) && $siswaOptional2[0]->nisn != $nisn) {
        $this->session->set_flashdata('failed', FlashData::DataFound('NISN sudah terdaftar.'));
        redirect('siswa/datadiri');
      }

      $siswaOptional2 = $this->calonSiswa->findById($id);
      $data = array(
        'id_calon_siswa' => $id,
        'nama_lengkap' => $namaLengkap,
        'gender' => $gender,
        'agama' => $agama,
        'alamat' => $alamat,
        'kecamatan' => $kecamatan,
        'kabupaten' => $kabupaten,
        'tempat_lahir' => $tempatLahir,
        'tanggal_lahir' => $tanggalLahir,
        'no_hp' => $noHp,
        'nama_ortu' => $namaOrtu,
        'username' => $siswaOptional2[0]->username,
        'password' => $siswaOptional2[0]->password,
        'nisn' => $nisn,
        'status' => $siswaOptional2[0]->status
      );

      $store = $this->calonSiswa->update($data);
      if ($store) {
        $this->session->set_flashdata('success', FlashData::successSaved());
        redirect('siswa/datadiri');
      } else {
        $this->session->set_flashdata('failed', FlashData::failedSaved());
        redirect('siswa/datadiri');
      }
    }
  }

  public function updateSiswa()
  {
    $id = $this->input->post('id');
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $calonSiswaOptional = $this->calonSiswa->findByUsername($username);
    $siswaOptional = $this->calonSiswa->findById($id);
    if ($siswaOptional[0]->username != $username && !empty($calonSiswaOptional)) {
      $this->session->set_flashdata('failed', FlashData::usernameAlreadyExist());
      redirect('siswa/datadiri');
    }

    $siswaOptional2 = $this->calonSiswa->findById($id);
    $data = array(
      'id_calon_siswa' => $id,
      'nama_lengkap' => $siswaOptional2[0]->nama_lengkap,
      'gender' => $siswaOptional2[0]->gender,
      'agama' => $siswaOptional2[0]->agama,
      'alamat' => $siswaOptional2[0]->alamat,
      'kecamatan' => $siswaOptional2[0]->kecamatan,
      'kabupaten' => $siswaOptional2[0]->kabupaten,
      'tempat_lahir' => $siswaOptional2[0]->tempat_lahir,
      'tanggal_lahir' => $siswaOptional2[0]->tanggal_lahir,
      'no_hp' => $siswaOptional2[0]->no_hp,
      'nama_ortu' => $siswaOptional2[0]->nama_ortu,
      'username' => $username,
      'password' => $this->encryption->encrypt($password),
      'nisn' => $siswaOptional2[0]->nisn,
      'status' => $siswaOptional2[0]->status
    );

    $store = $this->calonSiswa->update($data);
    if ($store) {
      $this->session->set_flashdata('success', FlashData::successSaved());
      redirect('siswa/logout');
    } else {
      $this->session->set_flashdata('failed', FlashData::failedSaved());
      redirect('siswa/index');
    }
  }
}
