<?php 

class Pendaftaran extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('PendaftaranModel', 'pendaftaran');
    $this->load->model('StatusSeleksiPendaftaranCalonSiswaModel', 'statusSeleksi');
    $this->load->model('KonfigurasiModel', 'konfigurasi');
    $this->load->model('CalonSiswaModel','calonSiswa');
  }

  public function getCalonSiswa()
  {
    return $this->calonSiswa->findById($this->session->userdata('id'));
  }

  public function isLogin()
  {
    if ($this->session->userdata('status') == LoginSessionConstant::SISWA) {
      return true;
    }

    redirect('siswa/login');
  }

  public function index($reUploadDocument = false)
  {
    if ($this->isLogin()) {
      $calonSiswaId = $this->session->userdata('id');
      $pendaftaranOptional = $this->pendaftaran->findByCalonSiswaId($calonSiswaId);
      $statusSeleksiOptional = null;
      if (!empty($pendaftaranOptional)) {
        $statusSeleksiOptional = $this->statusSeleksi->findByNoPendaftaran($pendaftaranOptional[0]->no_pendaftaran);
        if (!empty($statusSeleksiOptional)) {
          $batasPengumumanSeleksi = $statusSeleksiOptional[0]->batas_waktu_seleksi;
          $batasPengumumanSeleksiLong = strtotime($batasPengumumanSeleksi);
          $dateNowLong = strtotime(Date('d-m-Y'));
          $selisih = $batasPengumumanSeleksiLong - $dateNowLong;
          if ($selisih >= 0) {
            $statusSeleksiOptional = null;
          }
        }
      }
      
      $pendaftaranOptional = $this->pendaftaran->findByTahunAjaranAndCalonSiswaId($this->getTahunAjaran(), $calonSiswaId);
      $statusSeleksiOptional2 = null;
      if (!empty($pendaftaranOptional)) {
        $statusSeleksiOptional2 = $this->statusSeleksi->findByNoPendaftaran($pendaftaranOptional[0]->no_pendaftaran);
        if (!empty($statusSeleksiOptional2)) {
          $batasSeleksi = $statusSeleksiOptional2[0]->batas_waktu_seleksi;
          $batasSeleksiLong = strtotime($batasSeleksi);
          $dateNowLong = strtotime(Date('d-m-Y'));
          $selisih = $batasSeleksiLong - $dateNowLong;
          if ($selisih >= 0) {
            $statusSeleksiOptional2 = null;
          }
        }
      }
      $batasPendaftaran = $this->getBatasWaktuPendaftaran();
      $batasPendaftaranLong = strtotime($batasPendaftaran);
      $dateNowLong = strtotime(Date('d-m-Y'));
      $selisih = $batasPendaftaranLong - $dateNowLong;
      $valid = false;
      if ($selisih >= 0) {
        $valid = true;
      }

      $pengumumanSeleksiKonfig = $this->konfigurasi->findByModule(ModuleConstant::PENGUMUMAN_SELEKSI);
      $viewPengumumanSeleksi = false;
      if (!empty($pengumumanSeleksiKonfig)) {
        $pengumumanSeleksiKonfigLong = strtotime($pengumumanSeleksiKonfig[0]->value);
        if ($pengumumanSeleksiKonfigLong <= $dateNowLong) {
          $viewPengumumanSeleksi = true;
        }
      }

      $validPendingView = false;
      $statusSeleksiOptional2 = array();
      if (!empty($pendaftaranOptional)) {
          $statusSeleksiOptional2 = $this->statusSeleksi->findByStatusDiterimaAtauDitolakAndNoPendaftaran(StatusSeleksiConstant::PENDING, $pendaftaranOptional[0]->no_pendaftaran);
          if (!empty($statusSeleksiOptional2)) {
              $pengumumanSeleksi = $statusSeleksiOptional2[0]->batas_waktu_seleksi;
              $batasRevisiDate = date('Y-m-d', strtotime('-7 days', strtotime($pengumumanSeleksi)));
              $batasRevisiLong = strtotime($batasRevisiDate);
              $validPendingView = ($dateNowLong - $batasRevisiLong) < 0;
          }
      }

      $data = array(
        'activeMenu' => MenuConstant::PENDAFTARAN,
        'tanggalDaftar' => Date('d-m-Y'),
        'noPendaftaran' => $this->getNoPendaftaran(),
        'tahunAjaran' => $this->getTahunAjaran(),
        'pendaftaran' => $pendaftaranOptional,
        'statusSeleksi' => $statusSeleksiOptional,
        'statusSeleksiOptional' => $statusSeleksiOptional2,
        'canRegister' => $valid,
        'pengumumanSeleksi' => $viewPengumumanSeleksi,
        'reUploadDocument' => $reUploadDocument,
        'validPendingView' => $validPendingView,
        'statusSeleksiOptional2' => $statusSeleksiOptional2,
        'calonSiswa' => $this->getCalonSiswa()
      );
      
      $this->load->view('siswa/pendaftaran', $data);
    }
  }

  private function getNoPendaftaran()
  {
    $dateNow = Date('dmY');
    $pendaftaranOptional = $this->pendaftaran->findOrderByIdDescLimit1();
    if (empty($pendaftaranOptional)) {
      return $this->getTahunAjaran().$dateNow.'0000001';
    } else {
      $counter = (int) substr($pendaftaranOptional[0]->no_pendaftaran, 13);
      $counter++;
      return $this->getTahunAjaran().$dateNow.sprintf('%07s', $counter);
    }
  }

  public function getBatasWaktuPendaftaran()
  {
    $konfigOptional = $this->konfigurasi->findByModule(ModuleConstant::BATAS_PENDAFTARAN);
    return $konfigOptional[0]->value;
  }

  public function getTahunAjaran()
  {
    $konfigOptional = $this->konfigurasi->findByModule(ModuleConstant::TAHUN_AJARAN);
    return $konfigOptional[0]->value;
  }

  public function store()
  {
    if ($this->isLogin()) {
      $calonSiswaId = $this->input->post('calonSiswaId');
      $tahunAjaran = $this->getTahunAjaran();
      $noPendaftaran = $this->getNoPendaftaran();
      $tanggalDaftar = Date('Y-m-d h:i:s');
      $pendaftaranOptional = $this->pendaftaran->findByTahunAjaranAndCalonSiswaId($tahunAjaran, $calonSiswaId);
      if (!empty($pendaftaranOptional)) {
        $noPendaftaran = $pendaftaranOptional[0]->no_pendaftaran;
        $tanggalDaftar = $pendaftaranOptional[0]->tanggal_daftar;
      }
      $noIjazah = $this->input->post('noIjazah');
      $path = './assert/dokumen/pendaftaran/'.$tahunAjaran.'/';
      if((!file_exists($path)) && (!is_dir($path))) {
        mkdir ($path);
      }
  
      $config['upload_path'] = $path;
      $config['allowed_types'] = 'pdf';
      $config['max_size'] = 2000;
      $config['file_name'] = $noPendaftaran.'_IJAZAH.pdf';
      $config['overwrite'] = true;
      $this->load->library('upload', $config);
      if (! $this->upload->do_upload('uploadIjazah')) {
        $this->session->set_flashdata('failed', FlashData::generalError($this->upload->display_errors()));
        redirect('siswa/pendaftaran');
      }
      $ijazah = $this->upload->data();

      $config['file_name'] = $noPendaftaran.'_AKTA.pdf';
      $this->load->library('upload', $config);
      if (! $this->upload->do_upload('uploadAkta')) {
        $this->session->set_flashdata('failed', FlashData::generalError($this->upload->display_errors()));
        redirect('siswa/pendaftaran');
      }
      $akta = $this->upload->data();

      $config['file_name'] = $noPendaftaran.'_KK.pdf';
      $this->load->library('upload', $config);
      if (! $this->upload->do_upload('uploadKk')) {
        $this->session->set_flashdata('failed', FlashData::generalError($this->upload->display_errors()));
        redirect('siswa/pendaftaran');
      }
      $kk = $this->upload->data();

      $pendaftaranOptional = $this->pendaftaran->findByNoIjazah($noIjazah);
      $update = false;
      if (!empty($pendaftaranOptional)) {
        if ($pendaftaranOptional[0]->calon_siswa_id != $calonSiswaId) {
          $this->session->set_flashdata('failed', FlashData::DataFound('Gagal simpan data. Nomor ijazah sudah digunakan.'));
          redirect('siswa/pendaftaran');
        } else {
          $update = true;
        }
      }

      $data = array();
      if ($update) {
        $data = array(
          'id_pendaftaran' => $pendaftaranOptional[0]->id_pendaftaran,
          'no_pendaftaran' => $noPendaftaran,
          'tanggal_daftar' => $tanggalDaftar,
          'tahun_ajaran' => $tahunAjaran,
          'no_ijazah' => $noIjazah,
          'calon_siswa_id' => $calonSiswaId,
          'upload_ijazah' => $noPendaftaran.'_IJAZAH.pdf',
          'upload_kk' => $noPendaftaran.'_KK.pdf',
          'upload_akta' => $noPendaftaran.'_AKTA.pdf'
        );
      } else {
        $data = array(
          'no_pendaftaran' => $noPendaftaran,
          'tanggal_daftar' => $tanggalDaftar,
          'tahun_ajaran' => $tahunAjaran,
          'no_ijazah' => $noIjazah,
          'calon_siswa_id' => $calonSiswaId,
          'upload_ijazah' => $noPendaftaran.'_IJAZAH.pdf',
          'upload_kk' => $noPendaftaran.'_KK.pdf',
          'upload_akta' => $noPendaftaran.'_AKTA.pdf'
        );
      }
      
      $store = false;
      if ($update) {
        $store = $this->pendaftaran->update($data);
      } else {
        $store = $this->pendaftaran->save($data);
      }
      
      if ($store) {
        if ($update) {
          $seleksiOptional = $this->statusSeleksi->findByNoPendaftaran($noPendaftaran);
          if (!empty($seleksiOptional)) {
            $data = array(
              'id_status' => $seleksiOptional[0]->id_status,
              'no_pendaftaran' => $seleksiOptional[0]->no_pendaftaran,
              'tgl_diterima_atau_ditolak' => $seleksiOptional[0]->tgl_diterima_atau_ditolak,
              'umur' => $seleksiOptional[0]->umur,
              'status_diterima_atau_ditolak' => $seleksiOptional[0]->status_diterima_atau_ditolak,
              'admin_pendaftaran_id' => $seleksiOptional[0]->admin_pendaftaran_id,
              'status_review' => StatusReviewConstant::WAITING_REVIEW,
              'batas_waktu_seleksi' => $seleksiOptional[0]->batas_waktu_seleksi,
              'catatan' => $seleksiOptional[0]->catatan
            );

            $this->statusSeleksi->update($data);
          }
        }
        
        $this->session->set_flashdata('success', FlashData::successSaved());
        redirect('siswa/pendaftaran');
      } else {
        $this->session->set_flashdata('failed', FlashData::failedSaved());
        redirect('siswa/pendaftaran');
      }
    }
  }

  public function reUploadDocument()
  {
    return $this->index(true);
  }
}
