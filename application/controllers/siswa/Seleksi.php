<?php 

class Seleksi extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('PendaftaranModel', 'pendaftaran');
    $this->load->model('StatusSeleksiPendaftaranCalonSiswaModel', 'statusSeleksi');
    $this->load->model('CalonSiswaModel', 'calonSiswa');
    $this->load->model('KonfigurasiModel', 'konfigurasi');
  }

  public function getTahunAjaran()
  {
    $konfigOptional = $this->konfigurasi->findByModule(ModuleConstant::TAHUN_AJARAN);
    return $konfigOptional[0]->value;
  }

  public function isLogin()
  {
    if ($this->session->userdata('status') == 'login_siswa') {
      return true;
    }
    redirect('siswa/login');
  }

  public function index()
  {
    if ($this->isLogin()) {
      $calonSiswaId = $this->session->userdata('id');
      $pendaftaranByUserList = $this->pendaftaran->findByCalonSiswaIdOrderByIdDesc($calonSiswaId);
      $tahunAjaran = null;
      if (!empty($pendaftaranByUserList)) {
        $tahunAjaran = $pendaftaranByUserList[0]->tahun_ajaran;
      }
      
      $pendaftaranList = $this->pendaftaran->findByTahunAjaran($tahunAjaran);
      $noPendaftaranList = array();
      $calonSiswaIdList = array();
      if (!empty($pendaftaranList)) {
        foreach ($pendaftaranList as $pendaftaran) {
          $noPendaftaranList[] = $pendaftaran->no_pendaftaran;
          $calonSiswaIdList[] = $pendaftaran->calon_siswa_id;
        }
      }

      $pendaftaranList = $this->pendaftaran->findByNoPendaftaranIn($noPendaftaranList);
      $calonSiswaList = $this->calonSiswa->findByIdIn($calonSiswaIdList);
      $statusSeleksiList = $this->statusSeleksi->findByNoPendaftaranIn($noPendaftaranList);
      $pendaftaranOptional = $this->pendaftaran->findByTahunAjaranAndCalonSiswaId($this->getTahunAjaran(), $calonSiswaId);
      $statusSeleksiOptional = null;
      if (!empty($pendaftaranOptional)) {
          $statusSeleksiOptional = $this->statusSeleksi->findByNoPendaftaran($pendaftaranOptional[0]->no_pendaftaran);
      }

      if (!empty($statusSeleksiList)) {
        $batasWaktuPengumuman = $statusSeleksiList[0]->batas_waktu_seleksi;
        $batasWaktuPengumumanLong = strtotime($batasWaktuPengumuman);
        $dateNowLong = strtotime(Date('d-m-Y'));
        $selisih = $batasWaktuPengumumanLong - $dateNowLong;
        if ($selisih >= 0) {
          $statusSeleksiList = array();
          $statusSeleksiOptional = null;
        }
      }

      $pengumumanSeleksiKonfig = $this->konfigurasi->findByModule(ModuleConstant::PENGUMUMAN_SELEKSI);
      $viewPengumumanSeleksi = false;
      if (!empty($pengumumanSeleksiKonfig)) {
        $pengumumanSeleksiKonfigLong = strtotime($pengumumanSeleksiKonfig[0]->value);
        if ($pengumumanSeleksiKonfigLong <= $dateNowLong) {
          $viewPengumumanSeleksi = true;
        }
      }

      $data = array(
        'activeMenu' => MenuConstant::SELEKSI,
        'seleksiList' => $statusSeleksiList,
        'pendaftaranList' => $pendaftaranList,
        'calonSiswaList' => $calonSiswaList,
        'statusSeleksiOptional' => $statusSeleksiOptional,
        'pengumumanSeleksi' => $viewPengumumanSeleksi
      );
      $this->load->view('siswa/seleksi', $data);
    }
  }
}
