<?php 

class Register extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('CalonSiswaModel', 'calonSiswa');
  }

  public function index()
  {
    $this->load->view('siswa/register');
  }

  public function store()
  {
    $namaLengkap = $this->input->post('namaLengkap');
    $gender = $this->input->post('gender');
    $tempatLahir = $this->input->post('tempatLahir');
    $tanggalLahir = $this->input->post('tanggalLahir');
    $alamat = $this->input->post('alamat');
    $kecamatan = $this->input->post('kecamatan');
    $kabupaten = $this->input->post('kabupaten');
    $agama = $this->input->post('agama');
    $noHp = $this->input->post('telepon');
    $username = $this->input->post('username');
    $password2 = $this->input->post('password2');
    $password = $this->input->post('password');
    $namaOrtu = $this->input->post('namaOrtu');
    $nisn = $this->input->post('nisn');

    if ($password != $password2) {
      $this->session->set_flashdata('failed', FlashData::generalError('Password yang anda masukkan tidak sama.'));
      redirect('siswa/register');
    }

    $calonSiswaOptional = $this->calonSiswa->findByUsername($username);
    if (!empty($calonSiswaOptional)) {
      $this->session->set_flashdata('failed', FlashData::usernameAlreadyExist());
      redirect('siswa/register');
    }

    $calonSiswaOptional2 = $this->calonSiswa->findByNisn($nisn);
    if (!empty($calonSiswaOptional2)) {
      $this->session->set_flashdata('failed', FlashData::DataFound('NISN sudah terdaftar.'));
      redirect('siswa/register');
    }

    $data = array(
      'nama_lengkap' => $namaLengkap,
      'gender' => $gender,
      'agama' => $agama,
      'alamat' => $alamat,
      'kecamatan' => $kecamatan,
      'kabupaten' => $kabupaten,
      'tempat_lahir' => $tempatLahir,
      'tanggal_lahir' => $tanggalLahir,
      'no_hp' => $noHp,
      'nama_ortu' => $namaOrtu,
      'username' => $username,
      'password' => $this->encryption->encrypt($password),
      'nisn' => $nisn,
      'status' => StatusPendaftarConstant::AKTIF
    );

    $store = $this->calonSiswa->save($data);
    if ($store) {
      $this->session->set_flashdata('success', FlashData::success('Berhasil membuat akun.'));
      redirect('siswa/login');
    } else {
      $this->session->set_flashdata('failed', FlashData::generalError('gagal membuat akun.'));
      redirect('siswa/register');
    }
  }
}
