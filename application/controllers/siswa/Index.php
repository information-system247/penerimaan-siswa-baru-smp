<?php
class Index extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('PendaftaranModel', 'pendaftaran');
        $this->load->model('StatusSeleksiPendaftaranCalonSiswaModel', 'statusSeleksi');
        $this->load->model('KonfigurasiModel', 'konfigurasi');
        $this->load->model('DaftarUlangModel', 'daftarUlang');
        $this->load->model('CalonSiswaModel','calonSiswa');
    }

    public function getCalonSiswa()
    {
        return $this->calonSiswa->findById($this->session->userdata('id'));
    }

    public function isLogin()
    {
        if ($this->session->userdata('status') == LoginSessionConstant::SISWA) {
            return true;
        }
        redirect('siswa/login');
    }

    public function index()
    {
        if ($this->isLogin()) {
            $konfigBatasPendaftaran = $this->konfigurasi->findByModule(ModuleConstant::BATAS_PENDAFTARAN);
            $konfigPengumumanSeleksi = $this->konfigurasi->findByModule(ModuleConstant::PENGUMUMAN_SELEKSI);
            $konfigBatasDaftarUlang = $this->konfigurasi->findByModule(ModuleConstant::BATAS_DAFTAR_ULANG);
            $konfigOptional = $this->konfigurasi->findByModule(ModuleConstant::TAHUN_AJARAN);
            $calonSiswaId = $this->session->userdata('id');
            $pendaftaranOptional = $this->pendaftaran->findByTahunAjaranAndCalonSiswaId($konfigOptional[0]->value, $calonSiswaId);
            $statusSeleksiOptional = null;
            $dateNowLong = strtotime(Date('d-m-Y'));
            if (!empty($pendaftaranOptional)) {
                $statusSeleksiOptional = $this->statusSeleksi->findByNoPendaftaran($pendaftaranOptional[0]->no_pendaftaran);
                if (!empty($statusSeleksiOptional)) {
                    $batasSeleksi = $statusSeleksiOptional[0]->batas_waktu_seleksi;
                    $batasSeleksiLong = strtotime($batasSeleksi);
                    $selisih = $batasSeleksiLong - $dateNowLong;
                    if ($selisih > 0) {
                        $statusSeleksiOptional = null;
                    }
                }
            }

            $viewPengumumanSeleksi = false;
            if (!empty($konfigPengumumanSeleksi)) {
                $pengumumanSeleksiKonfigLong = strtotime($konfigPengumumanSeleksi[0]->value);
                if ($pengumumanSeleksiKonfigLong <= $dateNowLong) {
                    $viewPengumumanSeleksi = true;
                }
            }

            $validPendingView = false;
            $statusSeleksiOptional2 = array();
            $batasRevisiDate = null;
            $statusSeleksiOptional3 = array();
            if (!empty($pendaftaranOptional)) {
                $statusSeleksiOptional2 = $this->statusSeleksi->findByStatusDiterimaAtauDitolakAndNoPendaftaran(StatusSeleksiConstant::PENDING, $pendaftaranOptional[0]->no_pendaftaran);
                $statusSeleksiOptional3 = $this->statusSeleksi->findByStatusDiterimaAtauDitolakAndNoPendaftaran(StatusSeleksiConstant::TERIMA, $pendaftaranOptional[0]->no_pendaftaran);
                if (!empty($statusSeleksiOptional2)) {
                    $pengumumanSeleksi = $statusSeleksiOptional2[0]->batas_waktu_seleksi;
                    $batasRevisiDate = date('Y-m-d', strtotime('-7 days', strtotime($pengumumanSeleksi)));
                    $batasRevisiLong = strtotime($batasRevisiDate);
                    $validPendingView = ($dateNowLong - $batasRevisiLong) < 0;
                }
            }

            if ($batasRevisiDate != null) {
                $batasRevisi = explode('-', $batasRevisiDate);
                $batasRevisiDate = $batasRevisi[2].'-'.$batasRevisi[1].'-'.$batasRevisi[0];
            }
            
            $validDaftarUlang = false;
            $daftarUlangOptional = array();
            if (!empty($statusSeleksiOptional3)) {
                $batasPengumumanSeleksi = $statusSeleksiOptional3[0]->batas_waktu_seleksi;
                $daftarUlangOptional = $this->daftarUlang->findByNoPendaftaran($statusSeleksiOptional3[0]->no_pendaftaran);
                if (!empty($daftarUlangOptional) && $daftarUlangOptional[0]->status_approved == StatusApprovedConstant::PENDING) {
                    $validDaftarUlang = $dateNowLong - strtotime($batasPengumumanSeleksi) >= 0;
                }
            }

            $batasPendaftaran = explode('-', $konfigBatasPendaftaran[0]->value);
            $batasPendaftaran = $batasPendaftaran[2].'-'.$batasPendaftaran[1].'-'.$batasPendaftaran[0];
            $tglPengumumanSeleksi = explode('-', $konfigPengumumanSeleksi[0]->value);
            $tglPengumumanSeleksi = $tglPengumumanSeleksi[2].'-'.$tglPengumumanSeleksi[1].'-'.$tglPengumumanSeleksi[0];
            $batasDaftarUlang = explode('-', $konfigBatasDaftarUlang[0]->value);
            $batasDaftarUlang = $batasDaftarUlang[2].'-'.$batasDaftarUlang[1].'-'.$batasDaftarUlang[0];

            if (!$validDaftarUlang) {
                $validDaftarUlang = ($dateNowLong - strtotime($konfigPengumumanSeleksi[0]->value)) >= 0;
            }

            $data = array(
                "activeMenu" => MenuConstant::BERANDA,
                'statusSeleksiOptional' => $statusSeleksiOptional,
                'batasPendaftaran' => $batasPendaftaran,
                'batasDaftarUlang' => $batasDaftarUlang,
                'tanggalPengumumanSeleksi' => $tglPengumumanSeleksi,
                'pengumumanSeleksi' => $viewPengumumanSeleksi,
                'validPendingView' => $validPendingView,
                'statusSeleksiOptional2' => $statusSeleksiOptional2,
                'batasRevisi' => $batasRevisiDate,
                'validDaftarUlang' => $validDaftarUlang,
                'daftarUlangOptional' => $daftarUlangOptional,
                'pendaftaranOptional' => $pendaftaranOptional,
                'calonSiswa' => $this->getCalonSiswa()
            );

            $this->load->view('siswa/index', $data);
        }
    }

    public function prosesAfterDeadlineDaftarUlang()
    {
        if ($this->isLogin()) {
            $konfigBatasDaftarUlang = $this->konfigurasi->findByModule(ModuleConstant::BATAS_DAFTAR_ULANG);
            $konfigTahunAjaran = $this->konfigurasi->findByModule(ModuleConstant::TAHUN_AJARAN);
            $pendaftaranList = $this->pendaftaran->findByTahunAjaran($konfigTahunAjaran[0]->value);
            $noPendaftaranList = array();
            if (!empty($pendaftaranList)) {
                foreach ($pendaftaranList as $pendaftaran) {
                    $noPendaftaranList[] = $pendaftaran->no_pendaftaran;
                }
            }

            $dateNowLong = strtotime(Date('d-m-Y'));
            $konfigBatasDaftarUlangLong = strtotime($konfigBatasDaftarUlang[0]->value);
            if ($dateNowLong > $konfigBatasDaftarUlangLong) {
                if (!empty($noPendaftaranList)) {
                    $statusSeleksiList = $this->statusSeleksi->findByNoPendaftaranIn($noPendaftaranList);
                    $noPendaftaranAcceptedList = array();
                    if (!empty($statusSeleksiList)) {
                        foreach ($statusSeleksiList as $statusSeleksi) {
                            if ($statusSeleksi->status_diterima_atau_ditolak == 'TERIMA') {
                                $noPendaftaranAcceptedList[] = $statusSeleksi->no_pendaftaran;
                            }
                        }
        
                        if (!empty($noPendaftaranAcceptedList)) {
                            $daftarUlangList = $this->daftarUlang->findByNoPendaftaranIn($noPendaftaranAcceptedList);
        
                            $noPendaftaranDaftarUlangList = array();
                            if (!empty($daftarUlangList)) {
                                foreach ($daftarUlangList as $daftarUlang) {
                                    $noPendaftaranDaftarUlangList[] = $daftarUlang->no_pendaftaran;
                                }
        
                                $noPendaftaranDidNotDaftarUlangList = array_diff($noPendaftaranDaftarUlangList, $noPendaftaranAcceptedList);
                                if (!empty($noPendaftaranDidNotDaftarUlangList)) {
                                    foreach ($noPendaftaranDidNotDaftarUlangList as $noPendaftaran) {
                                        $this->updateStatusSeleksi($noPendaftaran);
                                    }
                                }
                            } else {
                                foreach ($noPendaftaranAcceptedList as $noPendaftaran) {
                                    $this->updateStatusSeleksi($noPendaftaran);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private function updateStatusSeleksi($noPendaftaran)
    {
        if ($this->isLogin()) {
            $statusSeleksiOptional = $this->statusSeleksi->findByNoPendaftaran($noPendaftaran);
            $data = array(
                'id_status' => $statusSeleksiOptional[0]->id_status,
                'no_pendaftaran' => $statusSeleksiOptional[0]->no_pendaftaran,
                'tgl_diterima_atau_ditolak' => $statusSeleksiOptional[0]->tgl_diterima_atau_ditolak,
                'umur' => $statusSeleksiOptional[0]->umur,
                'status_diterima_atau_ditolak' => 'TOLAK',
                'admin_pendaftaran_id' => $statusSeleksiOptional[0]->admin_pendaftaran_id,
                'status_review' => $statusSeleksiOptional[0]->status_review,
                'batas_waktu_seleksi' => $statusSeleksiOptional[0]->batas_waktu_seleksi,
                'catatan' => 'tidak segera melakukan daftar ulang'
            );
            $this->statusSeleksi->update($data);
        }
    }

    public function home()
    {
        $this->load->view('siswa/home');
    }
}
