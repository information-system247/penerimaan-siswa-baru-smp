<?php 

class Login extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('CalonSiswaModel', 'calonSiswa');
    $this->load->model('PendaftaranModel', 'pendaftaran');
    $this->load->model('KonfigurasiModel', 'konfigurasi');
    $this->load->model('StatusSeleksiPendaftaranCalonSiswaModel', 'statusSeleksi');
    $this->load->model('DaftarUlangModel', 'daftarUlang');
  }

  public function index()
  {
    $this->load->view('siswa/login');
  }

  public function aksiLogin()
  {
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $calonSiswaOptional = $this->calonSiswa->findByUsername($username);
    if (empty($calonSiswaOptional)) {
      $this->session->set_flashdata('failed', FlashData::loginFailedUsernameWrong());
      $this->load->view('siswa/login');
    } else {
      $passwordDb = $this->encryption->decrypt($calonSiswaOptional[0]->password);
      if ($password != $passwordDb) {
        $this->session->set_flashdata('failed', FlashData::loginFailedPasswordWrong());
        $this->load->view('siswa/login');
      } else {
        $status = $calonSiswaOptional[0]->status;
        if ($status == StatusPendaftarConstant::BLACKLIST) {
          $this->session->set_flashdata('failed', FlashData::generalError("Akun anda tidak diperbolehkan untuk login."));
          $this->load->view('siswa/login');
        } else {
          $dataSession = array(
            'id' => $calonSiswaOptional[0]->id_calon_siswa,
            'nama' => $calonSiswaOptional[0]->nama_lengkap,
            'gender' => $calonSiswaOptional[0]->gender,
            'agama' => $calonSiswaOptional[0]->agama,
            'alamat' => $calonSiswaOptional[0]->alamat,
            'kecamatan' => $calonSiswaOptional[0]->kecamatan,
            'kabupaten' => $calonSiswaOptional[0]->kabupaten,
            'tempatLahir' => $calonSiswaOptional[0]->tempat_lahir,
            'tanggalLahir' => $calonSiswaOptional[0]->tanggal_lahir,
            'noHp' => $calonSiswaOptional[0]->no_hp,
            'namaOrtu' => $calonSiswaOptional[0]->nama_ortu,
            'username' => $calonSiswaOptional[0]->username,
            'password' => $calonSiswaOptional[0]->password,
            'status' => LoginSessionConstant::SISWA
          );
          
          $this->session->set_userdata($dataSession);
          $this->session->set_flashdata('success', FlashData::loginSuccess());
          $konfigBatasPendaftaran = $this->konfigurasi->findByModule(ModuleConstant::BATAS_PENDAFTARAN);
          $konfigPengumumanSeleksi = $this->konfigurasi->findByModule(ModuleConstant::PENGUMUMAN_SELEKSI);
          $konfigBatasDaftarUlang = $this->konfigurasi->findByModule(ModuleConstant::BATAS_DAFTAR_ULANG);
          $konfigTahunAjaran = $this->konfigurasi->findByModule(ModuleConstant::TAHUN_AJARAN);
          $pendaftaranOptional = $this->pendaftaran->findByTahunAjaranAndCalonSiswaId($konfigTahunAjaran[0]->value, $calonSiswaOptional[0]->id_calon_siswa);
          $statusSeleksiOptional = null;
          $dateNowLong = strtotime(Date('d-m-Y'));
          if (!empty($pendaftaranOptional)) {
            $statusSeleksiOptional = $this->statusSeleksi->findByNoPendaftaran($pendaftaranOptional[0]->no_pendaftaran);
            if (!empty($statusSeleksiOptional)) {
              $batasSeleksi = $statusSeleksiOptional[0]->batas_waktu_seleksi;
              $batasSeleksiLong = strtotime($batasSeleksi);
              $selisih = $batasSeleksiLong - $dateNowLong;
              if ($selisih > 0) {
                $statusSeleksiOptional = null;
              }
            }
          }
  
          $validPendingView = false;
          $statusSeleksiOptional2 = array();
          $batasRevisiDate = null;
          $statusSeleksiOptional3 = array();
          if (!empty($pendaftaranOptional)) {
            $statusSeleksiOptional2 = $this->statusSeleksi->findByStatusDiterimaAtauDitolakAndNoPendaftaran(StatusSeleksiConstant::PENDING, $pendaftaranOptional[0]->no_pendaftaran);
            if (!empty($statusSeleksiOptional2)) {
              $pengumumanSeleksi = $statusSeleksiOptional2[0]->batas_waktu_seleksi;
              $batasRevisiDate = date('Y-m-d', strtotime('-7 days', strtotime($pengumumanSeleksi)));
              $batasRevisiLong = strtotime($batasRevisiDate);
              $validPendingView = ($dateNowLong - $batasRevisiLong) < 0;
            }
          }

          if ($batasRevisiDate != null) {
            $batasRevisi = explode('-', $batasRevisiDate);
            $batasRevisiDate = $batasRevisi[2].'-'.$batasRevisi[1].'-'.$batasRevisi[0];
          }
        
          $validDaftarUlang = false;
          $daftarUlangOptional = array();
          if (!empty($statusSeleksiOptional3)) {
            $daftarUlangOptional = $this->daftarUlang->findByNoPendaftaran($statusSeleksiOptional3[0]->no_pendaftaran);
            if (!empty($daftarUlangOptional) && $daftarUlangOptional[0]->status_approved == StatusApprovedConstant::PENDING) {
              $validDaftarUlang = true;
            }
          }

          $batasPendaftaran = explode('-', $konfigBatasPendaftaran[0]->value);
          $batasPendaftaran = $batasPendaftaran[2].'-'.$batasPendaftaran[1].'-'.$batasPendaftaran[0];
          $tglPengumumanSeleksi = explode('-', $konfigPengumumanSeleksi[0]->value);
          $tglPengumumanSeleksi = $tglPengumumanSeleksi[2].'-'.$tglPengumumanSeleksi[1].'-'.$tglPengumumanSeleksi[0];
          $batasDaftarUlang = explode('-', $konfigBatasDaftarUlang[0]->value);
          $batasDaftarUlang = $batasDaftarUlang[2].'-'.$batasDaftarUlang[1].'-'.$batasDaftarUlang[0];

          if (!$validDaftarUlang) {
            $validDaftarUlang = ($dateNowLong - strtotime($konfigPengumumanSeleksi[0]->value)) >= 0;
          }

          $data = array(
            "activeMenu" => MenuConstant::BERANDA,
            'batasPendaftaran' => $konfigBatasPendaftaran[0]->value,
            'batasDaftarUlang' => $konfigBatasDaftarUlang[0]->value,
            'pengumumanSeleksi' => $konfigPengumumanSeleksi[0]->value,
            'statusSeleksiOptional' => $statusSeleksiOptional,
            'tanggalPengumumanSeleksi' => $konfigPengumumanSeleksi[0]->value,
            'validPendingView' => $validPendingView,
            'statusSeleksiOptional2' => $statusSeleksiOptional2,
            'batasRevisi' => $batasRevisiDate,
            'validDaftarUlang' => $validDaftarUlang,
            'daftarUlangOptional' => $daftarUlangOptional,
            'pendaftaranOptional' => $pendaftaranOptional,
            'calonSiswa' => $calonSiswaOptional
          );
          
          $this->load->view('siswa/index', $data);
        }
      } 
    }
  }
}
