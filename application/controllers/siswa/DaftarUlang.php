<?php 

class DaftarUlang extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('PendaftaranModel', 'pendaftaran');
    $this->load->model('StatusSeleksiPendaftaranCalonSiswaModel', 'statusSeleksi');
    $this->load->model('CalonSiswaModel', 'calonSiswa');
    $this->load->model('DaftarUlangModel', 'daftarUlang');
    $this->load->model('KonfigurasiModel', 'konfigurasi');
  }

  public function isLogin()
  {
    if ($this->session->userdata('status') == LoginSessionConstant::SISWA) {
      return true;
    }

    redirect('siswa/login');
  }

  public function index()
  {
    if ($this->isLogin()) {
      $konfigPengumumanSeleksi = $this->konfigurasi->findByModule(ModuleConstant::PENGUMUMAN_SELEKSI);
      $konfigTahunAjaran = $this->konfigurasi->findByModule(ModuleConstant::TAHUN_AJARAN);
      $calonSiswaId = $this->session->userdata('id');
      $pendaftaranOptional = $this->pendaftaran->findByTahunAjaranAndCalonSiswaId($konfigTahunAjaran[0]->value, $calonSiswaId);
      $statusSeleksiOptional = null;
      $daftarUlangOptional = null;
      $tahunAjaran = null;
      if (!empty($pendaftaranOptional)) {
          $statusSeleksiOptional = $this->statusSeleksi->findByNoPendaftaran($pendaftaranOptional[0]->no_pendaftaran);
          $daftarUlangOptional = $this->daftarUlang->findByNoPendaftaran($pendaftaranOptional[0]->no_pendaftaran);
          $tahunAjaran = $pendaftaranOptional[0]->tahun_ajaran;
      }

      $batasDaftarUlang = $this->getBatasWaktuDaftarUlang();
      $batasDaftarUlangLong = strtotime($batasDaftarUlang);
      $dateNowLong = strtotime(Date('d-m-Y'));
      $selisih = $batasDaftarUlangLong - $dateNowLong;
      $valid = false;
      if ($selisih >= 0) {
        $valid = true;
      }

      $calonSiswaOptional = $this->calonSiswa->findById($calonSiswaId);
      $reUploadDocument = false;
      if (!empty($daftarUlangOptional) && $daftarUlangOptional[0]->status_approved == StatusApprovedConstant::PENDING) {
        $reUploadDocument = true;
      }
      
      $viewPengumumanSeleksi = false;
      if (!empty($konfigPengumumanSeleksi)) {
        $pengumumanSeleksiKonfigLong = strtotime($konfigPengumumanSeleksi[0]->value);
        if ($pengumumanSeleksiKonfigLong <= $dateNowLong) {
          $viewPengumumanSeleksi = true;
        }
      }

      $data = array(
          "activeMenu" => MenuConstant::DAFTAR_ULANG,
          'statusSeleksiOptional' => $statusSeleksiOptional,
          'pendaftaran' => $pendaftaranOptional,
          'daftarUlang' => $daftarUlangOptional,
          'tahunAjaran' => $tahunAjaran,
          'canDaftarUlang' => $valid,
          'calonSiswa' => $calonSiswaOptional,
          'reUploadDocument' => $reUploadDocument,
          'pengumumanSeleksi' => $viewPengumumanSeleksi
      );

      $this->load->view('siswa/daftarulang', $data);
    }
  }

  public function getBatasWaktuDaftarUlang()
  {
    $konfigOptional = $this->konfigurasi->findByModule(ModuleConstant::BATAS_DAFTAR_ULANG);
    return $konfigOptional[0]->value;
  }

  private function getTahunAjaran()
  {
    $konfigurasiOptional = $this->konfigurasi->findByModule(ModuleConstant::TAHUN_AJARAN);
    return $konfigurasiOptional[0]->value;
  }

  public function store()
  {
    if ($this->isLogin()) {
      $calonSiswaId = $this->session->userdata('id');
      $pendaftaranOptional = $this->pendaftaran->findByTahunAjaranAndCalonSiswaId($this->getTahunAjaran(), $calonSiswaId);
      $noPendaftaran = $pendaftaranOptional[0]->no_pendaftaran;
      $namaLengkap = $this->input->post('namaLengkap');
      $gender = $this->input->post('gender');
      $nisn = $this->input->post('nisn');
      $nikSiswa = $this->input->post('nikSiswa');
      $tempatLahirSiswa = $this->input->post('tempatLahirSiswa');
      $tanggalLahirSiswa = $this->input->post('tanggalLahirSiswa');
      $agama = $this->input->post('agama');
      $kewarganegaraan = $this->input->post('kewarganegaraan');
      $alamat = $this->input->post('alamat');
      $rt = $this->input->post('rt');
      $rw = $this->input->post('rw');
      $namaDusun = $this->input->post('namaDusun');
      $namaKelurahan = $this->input->post('namaKelurahan');
      $kecamatan = $this->input->post('kecamatan');
      $kabupaten = $this->input->post('kabupaten');
      $kodePos = $this->input->post('kodePos');
      $namaAyah = $this->input->post('namaAyah');
      $nikAyah = $this->input->post('nikAyah');
      $tahunLahirAyah = $this->input->post('tahunLahirAyah');
      $namaIbu = $this->input->post('namaIbu');
      $nikIbu = $this->input->post('nikIbu');
      $tahunLahirIbu = $this->input->post('tahunLahirIbu');
      $telepon = $this->input->post('telepon');
      $reUploadDoc = $this->input->post('reUploadDocument');
      
      $path = './assert/dokumen/daftar_ulang/'.$this->getTahunAjaran().'/';
      if((!file_exists($path))&&(!is_dir($path))) {
        mkdir ($path);
      }

      $config['upload_path'] = $path;
      $config['allowed_types'] = 'zip';
      $config['max_size'] = 5000;
      $config['file_name'] = $noPendaftaran.'_DAFTAR_ULANG.zip';
      $config['overwrite'] = true;
      $this->load->library('upload', $config);
      if (! $this->upload->do_upload('uploadDokumen')) {
        $this->session->set_flashdata('failed', FlashData::generalError($this->upload->display_errors()));
        redirect('siswa/daftarulang');
      }

      if ($reUploadDoc == "N") {
        $daftarUlangOptional = $this->daftarUlang->findByNisn($nisn);
        if (!empty($daftarUlangOptional)) {
          $this->session->set_flashdata('failed', FlashData::DataFound('NISN sudah ada.'));
          redirect('siswa/daftarulang');
        }

        $daftarUlangOptional = $this->daftarUlang->findByNikSiswa($nikSiswa);
        if (!empty($daftarUlangOptional)) {
          $this->session->set_flashdata('failed', FlashData::DataFound('NIK siwa sudah ada.'));
          redirect('siswa/daftarulang');
        }

        $statusSeleksiOptional = $this->statusSeleksi->findByNoPendaftaran($noPendaftaran);
        $statusId = null;
        if (!empty($statusSeleksiOptional)) {
          $statusId = $statusSeleksiOptional[0]->id_status;
        }

        $daftarUlangOptional = $this->daftarUlang->findByStatusId($statusId);
        if (!empty($daftarUlangOptional)) {
          $this->session->set_flashdata('failed', FlashData::DataFound('Data sudah ada.'));
          redirect('siswa/daftarulang');
        }

        $data = array(
          'tgl_daftar_ulang' => Date('Y-m-d h:i:s'),
          'status_id' => $statusId,
          'no_pendaftaran' => $noPendaftaran,
          'nama_lengkap' => $namaLengkap,
          'gender' => $gender,
          'nisn' => $nisn,
          'nik_siswa' => $nikSiswa,
          'tmp_lhr_siswa' => $tempatLahirSiswa,
          'tgl_lhr_siswa' => $tanggalLahirSiswa,
          'agama' => $agama,
          'kewarganegaan' => $kewarganegaraan,
          'alamat' => $alamat,
          'rt' => $rt,
          'rw' => $rw,
          'nama_dusun' => $namaDusun,
          'nama_kelurahan' => $namaKelurahan,
          'kecamatan' => $kecamatan,
          'kabupaten' => $kabupaten,
          'kode_pos' => $kodePos,
          'nama_ayah' => $namaAyah,
          'nik_ayah' => $nikAyah,
          'thn_lhr_ayah' => $tahunLahirAyah,
          'nama_ibu' => $namaIbu,
          'nik_ibu' => $nikIbu,
          'thn_lhr_ibu' => $tahunLahirIbu,
          'no_hp_ortu' => $telepon,
          'upload_dokumen' => $noPendaftaran.'_DAFTAR_ULANG.zip',
          'status_review' => StatusReviewConstant::WAITING_REVIEW,
          'catatan' => null,
          'status_approved' => StatusApprovedConstant::WAITING_APPROVED
        );

        $store = $this->daftarUlang->save($data);
      } else {
        $catatan = $this->input->post('catatan');
        $id = $this->input->post('id');
        $daftarUlangOptional2 = $this->daftarUlang->findById($id);
        $daftarUlangOptional = $this->daftarUlang->findByNisn($nisn);
        if (!empty($daftarUlangOptional) && $daftarUlangOptional2[0]->no_pendaftaran != $noPendaftaran) {
          $this->session->set_flashdata('failed', FlashData::DataFound('NISN sudah ada.'));
          redirect('siswa/daftarulang');
        }

        $daftarUlangOptional = $this->daftarUlang->findByNikSiswa($nikSiswa);
        if (!empty($daftarUlangOptional) && $daftarUlangOptional2[0]->no_pendaftaran != $noPendaftaran) {
          $this->session->set_flashdata('failed', FlashData::DataFound('NIK siwa sudah ada.'));
          redirect('siswa/daftarulang');
        }

        $statusSeleksiOptional = $this->statusSeleksi->findByNoPendaftaran($noPendaftaran);
        $statusId = null;
        if (!empty($statusSeleksiOptional)) {
          $statusId = $statusSeleksiOptional[0]->id_status;
        }

        $daftarUlangOptional = $this->daftarUlang->findByStatusId($statusId);
        if (!empty($daftarUlangOptional) && $daftarUlangOptional2[0]->no_pendaftaran != $noPendaftaran) {
          $this->session->set_flashdata('failed', FlashData::DataFound('Data sudah ada.'));
          redirect('siswa/daftarulang');
        }

        $data = array(
          'id_daftar_ulang' => $id,
          'tgl_daftar_ulang' => Date('Y-m-d h:i:s'),
          'status_id' => $statusId,
          'no_pendaftaran' => $noPendaftaran,
          'nama_lengkap' => $namaLengkap,
          'gender' => $gender,
          'nisn' => $nisn,
          'nik_siswa' => $nikSiswa,
          'tmp_lhr_siswa' => $tempatLahirSiswa,
          'tgl_lhr_siswa' => $tanggalLahirSiswa,
          'agama' => $agama,
          'kewarganegaan' => $kewarganegaraan,
          'alamat' => $alamat,
          'rt' => $rt,
          'rw' => $rw,
          'nama_dusun' => $namaDusun,
          'nama_kelurahan' => $namaKelurahan,
          'kecamatan' => $kecamatan,
          'kabupaten' => $kabupaten,
          'kode_pos' => $kodePos,
          'nama_ayah' => $namaAyah,
          'nik_ayah' => $nikAyah,
          'thn_lhr_ayah' => $tahunLahirAyah,
          'nama_ibu' => $namaIbu,
          'nik_ibu' => $nikIbu,
          'thn_lhr_ibu' => $tahunLahirIbu,
          'no_hp_ortu' => $telepon,
          'upload_dokumen' => $noPendaftaran.'_DAFTAR_ULANG.zip',
          'status_review' => StatusReviewConstant::WAITING_REVIEW,
          'catatan' => $catatan,
          'status_approved' => StatusApprovedConstant::WAITING_APPROVED
        );

        $store = $this->daftarUlang->update($data);
      }
      
      if ($store) {
        $this->upload->data();
        $this->session->set_flashdata('success', FlashData::successSaved());
        redirect('siswa/daftarulang');
      } else {
        $this->session->set_flashdata('failed', FlashData::failedSaved());
        redirect('siswa/daftarulang');
      }
    }
  }
}
