<?php 

class Seleksi extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('StatusSeleksiPendaftaranCalonSiswaModel', 'statusSeleksi');
    $this->load->model('PendaftaranModel', 'pendaftaran');
    $this->load->model('KonfigurasiModel', 'konfigurasi');
    $this->load->model('CalonSiswaModel', 'calonSiswa');
    $this->load->model('PanitiaModel','panitia');
  }

  public function getKonfigurasiList()
  {
    return $this->konfigurasi->findAll();
  }

  public function getPanitia()
  {
    return $this->panitia->findById($this->session->userdata('id'));
  }

  public function isLogin()
  {
    if ($this->session->userdata('status') == LoginSessionConstant::PANITIA) {
      return true;
    }
    redirect('index');
  }

  public function diterima()
  {
    if ($this->isLogin()) {
      $seleksiList = $this->statusSeleksi->findByStatusDiterimaAtauDitolak('TERIMA');
      $noPendaftaranList = array();
      if (!empty($seleksiList)) {
        foreach ($seleksiList as $seleksi) {
          $noPendaftaranList[] = $seleksi->no_pendaftaran;
        }
      }

      $pendaftaranList = array();
      if (!empty($noPendaftaranList)) {
        $pendaftaranList = $this->pendaftaran->findByNoPendaftaranIn($noPendaftaranList);
      }

      $calonsiswaIdList = array();
      if (!empty($pendaftaranList)) {
        foreach ($pendaftaranList as $pendaftaran) {
          $calonsiswaIdList[] = $pendaftaran->calon_siswa_id;
        }
      }

      $calonSiswaList = array();
      if (!empty($calonsiswaIdList)) {
        $calonSiswaList = $this->calonSiswa->findByIdIn($calonsiswaIdList);
      }

      $data = array(
        "activeMenu" => MenuConstant::LAPORAN_SELEKSI_DITERIMA,
        'pendaftaranList' => $pendaftaranList,
        'konfigurasiList' => $this->getKonfigurasiList(),
        'calonSiswaList' => $calonSiswaList,
        'panitia' => $this->getPanitia()
      );

      $this->load->view('laporan/seleksi_diterima/list', $data);
    }
  }

  public function tidakditerima()
  {
    if ($this->isLogin()) {
      $seleksiList = $this->statusSeleksi->findByStatusDiterimaAtauDitolak('TOLAK');
      $noPendaftaranList = array();
      if (!empty($seleksiList)) {
        foreach ($seleksiList as $seleksi) {
          $noPendaftaranList[] = $seleksi->no_pendaftaran;
        }
      }

      $pendaftaranList = array();
      if (!empty($noPendaftaranList)) {
        $pendaftaranList = $this->pendaftaran->findByNoPendaftaranIn($noPendaftaranList);
      }

      $calonsiswaIdList = array();
      if (!empty($pendaftaranList)) {
        foreach ($pendaftaranList as $pendaftaran) {
          $calonsiswaIdList[] = $pendaftaran->calon_siswa_id;
        }
      }

      $calonSiswaList = array();
      if (!empty($calonsiswaIdList)) {
        $calonSiswaList = $this->calonSiswa->findByIdIn($calonsiswaIdList);
      }
      $data = array(
        "activeMenu" => MenuConstant::LAPORAN_SELEKSI_TIDAK_DITERIMA,
        'pendaftaranList' => $pendaftaranList,
        'konfigurasiList' => $this->getKonfigurasiList(),
        'calonSiswaList' => $calonSiswaList,
        'panitia' => $this->getPanitia()
      );

      $this->load->view('laporan/seleksi_ditolak/list', $data);
    }
  }
}
