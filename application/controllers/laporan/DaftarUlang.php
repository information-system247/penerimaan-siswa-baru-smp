<?php 

class DaftarUlang extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('DaftarUlangModel', 'daftarUlang');
    $this->load->model('PendaftaranModel', 'pendaftaran');
    $this->load->model('KonfigurasiModel','konfigurasi');
    $this->load->model('PanitiaModel','panitia');
  }

  public function getKonfigurasiList()
  {
    return $this->konfigurasi->findAll();
  }

  public function getPanitia()
  {
    return $this->panitia->findById($this->session->userdata('id'));
  }

  public function isLogin()
  {
    if ($this->session->userdata('status') == LoginSessionConstant::PANITIA) {
      return true;
    }
    redirect('index');
  }

  public function index()
  {
    if ($this->isLogin()) {
      $daftarUlangList = $this->daftarUlang->findAll();
      $noPendaftaranList = array();
      if (!empty($daftarUlangList)) {
        foreach ($daftarUlangList as $daftarUlang) {
          $noPendaftaranList[] = $daftarUlang->no_pendaftaran;
        }
      }

      $pendaftaranList = array();
      if (!empty($noPendaftaranList)) {
        $pendaftaranList = $this->pendaftaran->findByNoPendaftaranIn($noPendaftaranList);
      }
      $data = array(
        "activeMenu" => MenuConstant::LAPORAN_DAFTAR_ULANG,
        "daftarUlangList" => $daftarUlangList,
        'pendaftaranList' => $pendaftaranList,
        'konfigurasiList'=>$this->getKonfigurasiList(),
        'panitia' => $this->getPanitia()
      );

      $this->load->view('laporan/daftar_ulang/list', $data);
    }
  }
}
