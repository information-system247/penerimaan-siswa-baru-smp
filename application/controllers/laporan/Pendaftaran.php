<?php 

class Pendaftaran extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('PendaftaranModel', 'pendaftaran');
    $this->load->model('CalonSiswaModel', 'calonSiswa');
    $this->load->model('KonfigurasiModel', 'konfigurasi');
    $this->load->model('PanitiaModel','panitia');
  }

  public function getKonfigurasiList()
  {
    return $this->konfigurasi->findAll();
  }

  public function getPanitia()
  {
    return $this->panitia->findById($this->session->userdata('id'));
  }

  public function isLogin()
  {
    if ($this->session->userdata('status') == LoginSessionConstant::PANITIA) {
      return true;
    }
    redirect('index');
  }

  public function index()
  {
    if ($this->isLogin()) {
      $pendaftaranList = $this->pendaftaran->findAll();
      $calonSiswaIdList = array();
      if (!empty($pendaftaranList)) {
        foreach ($pendaftaranList as $pendaftaran) {
          $calonSiswaIdList[] = $pendaftaran->calon_siswa_id;
        }
      }

      $calonSiswaList = array();
      if (!empty($calonSiswaIdList)) {
        $calonSiswaList = $this->calonSiswa->findByIdIn($calonSiswaIdList);
      }
      $data = array(
        "activeMenu" => MenuConstant::LAPORAN_PENDAFTARAN,
        "pendaftaranList" => $pendaftaranList,
        "calonSiswaList" => $calonSiswaList,
        'konfigurasiList' => $this->getKonfigurasiList(),
        'panitia' => $this->getPanitia()
      );

      $this->load->view('laporan/pendaftaran/list', $data);
    }
  }
}
