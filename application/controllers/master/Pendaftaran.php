<?php 

class Pendaftaran extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('StatusSeleksiPendaftaranCalonSiswaModel', 'statusSeleksi');
    $this->load->model('PendaftaranModel', 'pendaftaran');
    $this->load->model('CalonSiswaModel', 'calonSiswa');
    $this->load->model('KonfigurasiModel', 'konfigurasi');
    $this->load->model('PanitiaModel','panitia');
  }

  public function getKonfigurasiList()
  {
    return $this->konfigurasi->findAll();
  }

  public function getPanitia()
  {
    return $this->panitia->findById($this->session->userdata('id'));
  }

  public function isLogin()
  {
    if ($this->session->userdata('status') == LoginSessionConstant::PANITIA) {
      return true;
    }
    redirect('login');
  }

  public function index()
  {
    if ($this->isLogin()) {
      $konfigTahunAjaran = $this->konfigurasi->findByModule(ModuleConstant::TAHUN_AJARAN);
      $pendaftaranList = $this->pendaftaran->findByTahunAjaran($konfigTahunAjaran[0]->value);
      $calonSiswaIdList = array();
      if (!empty($pendaftaranList)) {
        foreach ($pendaftaranList as $pendaftaran) {
          $calonSiswaIdList[] = $pendaftaran->calon_siswa_id;
        }
      }

      $calonSiswaList = array();
      if (!empty($calonSiswaIdList)) {
        $calonSiswaList = $this->calonSiswa->findByIdIn($calonSiswaIdList);
      }
      $data = array(
        "activeMenu" => MenuConstant::PENDAFTARAN,
        "pendaftaranList" => $pendaftaranList,
        "calonSiswaList" => $calonSiswaList,
        'konfigurasiList' => $this->getKonfigurasiList(),
        'panitia' => $this->getPanitia()
      );

      $this->load->view('pendaftaran/list', $data);
    }
  }

  public function getTahunAjaran()
  {
    if ($this->isLogin()) {
      $konfigurasiOptional = $this->konfigurasi->findByModule(ModuleConstant::TAHUN_AJARAN);
      return $konfigurasiOptional[0]->value;
    }
  }

  private function getNoPendaftaran()
  {
    if ($this->isLogin()) {
      $dateNow = Date('dmY');
      $pendaftaranOptional = $this->pendaftaran->findOrderByIdDescLimit1();
      if (empty($pendaftaranOptional)) {
        return $this->getTahunAjaran().$dateNow.'0000001';
      } else {
        $counter = (int) substr($pendaftaranOptional[0]->no_pendaftaran, 13);
        $counter++;
        return $this->getTahunAjaran().$dateNow.sprintf('%07s', $counter);
      }
    }
  }

  public function add()
  {
    if ($this->isLogin()) {
      $calonSiswaList = $this->calonSiswa->findAll();
      $data = array(
        'activeMenu' => MenuConstant::PENDAFTARAN,
        'tanggalDaftar' => Date('d-m-Y'),
        'noPendaftaran' => $this->getNoPendaftaran(),
        'tahunAjaran' => $this->getTahunAjaran(),
        'calonSiswaList' => $calonSiswaList,
        'konfigurasiList' => $this->getKonfigurasiList(),
        'panitia' => $this->getPanitia()
      );

      $this->load->view('pendaftaran/add', $data);
    }
  }

  public function store()
  {
    if ($this->isLogin()) {
      $id = $this->input->post('id');
      $noPendaftaran = $this->getNoPendaftaran();
      $tanggalDaftar = Date('Y-m-d h:i:s');
      $tahunAjaran = $this->getTahunAjaran();
      $noIjazah = $this->input->post('noIjazah');
      $calonSiswaId = $this->input->post('calonSiswaId');

      $path = './assert/dokumen/pendaftaran/'.$tahunAjaran.'/';
      if((!file_exists($path))&&(!is_dir($path))) {
        mkdir ($path);
      }
  
      $config['upload_path'] = $path;
      $config['allowed_types'] = 'pdf';
      $config['max_size'] = 2000;
      $config['file_name'] = $noPendaftaran.'_IJAZAH.pdf';
      $config['overwrite'] = true;
      $this->load->library('upload', $config);
      if (! $this->upload->do_upload('uploadIjazah')) {
        if (empty($id)) {
          $this->session->set_flashdata('failed', FlashData::generalError($this->upload->display_errors()));
          redirect('master/pendaftaran');
        } else {
          if ('You did not select a file to upload.' != $this->upload->display_errors()) {
            $this->session->set_flashdata('failed', FlashData::generalError($this->upload->display_errors()));
            redirect('master/pendaftaran');
          }
        }
      }
      $ijazah = $this->upload->data();

      $config['file_name']  = $noPendaftaran.'_AKTA.pdf';
      $this->load->library('upload', $config);
      if (! $this->upload->do_upload('uploadAkta')) {
        if (empty($id)) {
          $this->session->set_flashdata('failed', FlashData::generalError($this->upload->display_errors()));
          redirect('master/pendaftaran');
        } else {
          if ('You did not select a file to upload.' != $this->upload->display_errors()) {
            $this->session->set_flashdata('failed', FlashData::generalError($this->upload->display_errors()));
            redirect('master/pendaftaran');
          }
        }
      }
      $akta = $this->upload->data();

      $config['file_name']  = $noPendaftaran.'_KK.pdf';
      $this->load->library('upload', $config);
      if (! $this->upload->do_upload('uploadAkta')) {
        if (empty($id)) {
          $this->session->set_flashdata('failed', FlashData::generalError($this->upload->display_errors()));
          redirect('master/pendaftaran');
        } else {
          if ('You did not select a file to upload.' != $this->upload->display_errors()) {
            $this->session->set_flashdata('failed', FlashData::generalError($this->upload->display_errors()));
            redirect('master/pendaftaran');
          }
        }
      }
      $kk = $this->upload->data();

      $store = false;
      $data = null;
      $pendaftaranOptional = $this->pendaftaran->findByNoIjazah($noIjazah);
      $pendaftaranOptional2 = $this->pendaftaran->findByTahunAjaranAndCalonSiswaId($tahunAjaran, $calonSiswaId);
      if (empty($id)) {
        if (!empty($pendaftaranOptional)) {
          $this->session->set_flashdata('failed', FlashData::DataFound('Gagal simpan data. Nomor ijazah sudah digunakan.'));
          redirect('master/pendaftaran');
        }

        if (!empty($pendaftaranOptional2)) {
          $this->session->set_flashdata('failed', FlashData::DataFound('Gagal simpan data. Data user sudah digunakan.'));
          redirect('master/pendaftaran');
        }

        $data = array(
          'no_pendaftaran' => $noPendaftaran,
          'tanggal_daftar' => $tanggalDaftar,
          'tahun_ajaran' => $tahunAjaran,
          'no_ijazah' => $noIjazah,
          'calon_siswa_id' => $calonSiswaId,
          'upload_ijazah' => $ijazah['file_name'],
          'upload_akta' =>  $akta['file_name'],
          'upload_kk' =>  $kk['file_name']
        );

        $store = $this->pendaftaran->save($data);
      } else {
        //todo  : drop  fitur edit  from  admin
        $pendaftaran = $this->pendaftaran->findById($id);
        if ($noIjazah != $pendaftaran[0]->no_ijazah && !empty($pendaftaranOptional)) {
          $this->session->set_flashdata('failed', FlashData::DataFound('Gagal simpan data. Nomor ijazah sudah digunakan.'));
          redirect('master/pendaftaran');
        }

        if ($calonSiswaId != $pendaftaran[0]->calon_siswa_id && !empty($pendaftaranOptional2)) {
          $this->session->set_flashdata('failed', FlashData::DataFound('Gagal simpan data. Data user sudah digunakan.'));
          redirect('master/pendaftaran');
        }

        if ($this->upload->display_errors() != 'You did not select a file to upload.') {
          $ijazah = $ijazah['file_name'];
        } else {
          $ijazah = $noPendaftaran.'_IJAZAH.pdf';
        }
        
        $data = array(
          'id_pendaftaran' => $id,
          'no_pendaftaran' => $noPendaftaran,
          'tanggal_daftar' => $tanggalDaftar,
          'tahun_ajaran' => $tahunAjaran,
          'no_ijazah' => $noIjazah,
          'calon_siswa_id' => $calonSiswaId,
          'upload_ijazah' => $ijazah,

        );

        $store = $this->pendaftaran->update($data);
      }

      if ($store) {
        $this->session->set_flashdata('success', FlashData::successSaved());
        redirect('master/pendaftaran');
      } else {
        $this->session->set_flashdata('failed', FlashData::failedSaved());
        redirect('master/pendaftaran');
      }
    }
  }

  public function get($id)
  {
    if ($this->isLogin()) {
      $pendaftaranOptional = $this->pendaftaran->findById($id);
      $calonSiswaList = $this->calonSiswa->findAll();
      $data = array(
        "activeMenu" => MenuConstant::PENDAFTARAN,
        'pendaftaran' => $pendaftaranOptional,
        'calonSiswaList' => $calonSiswaList,
        'konfigurasiList' => $this->getKonfigurasiList(),
        'panitia' => $this->getPanitia()
      );

      $this->load->view('pendaftaran/edit', $data);
    }
  }

  public function delete()
  {
    if ($this->isLogin()) {
      $id = $this->input->post('id');
      $pendaftaranOptional = $this->pendaftaran->findById($id);
      $statusSeleksiOptional = $this->statusSeleksi->findByNoPendaftaran($pendaftaranOptional[0]->no_pendaftaran);
      if (empty($statusSeleksiOptional)) {
        $this->session->set_flashdata('failed', FlashData::failedDeleteData('Data pendaftaran sudah digunakan untuk seleksi.'));
      } else {
        $store = $this->pendaftaran->delete($id);
        if ($store) {
          $this->session->set_flashdata('success', FlashData::deletedSuccess());
          redirect('master/pendaftaran');
        } else {
          $this->session->set_flashdata('failed', FlashData::deletedFailed());
          redirect('master/pendaftaran');
        }
      }
    }
  }
}
