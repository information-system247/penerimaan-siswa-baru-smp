<?php 

class CalonSiswa extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('CalonSiswaModel', 'calonSiswa');
    $this->load->model('PendaftaranModel', 'pendaftaran');
    $this->load->model('KonfigurasiModel', 'konfigurasi');
    $this->load->model('PanitiaModel','panitia');
  }

  public function getKonfigurasiList()
  {
    return $this->konfigurasi->findAll();
  }

  public function getPanitia()
  {
    return $this->panitia->findById($this->session->userdata('id'));
  }

  public function isLogin()
  {
    if ($this->session->userdata('status') == LoginSessionConstant::PANITIA) {
      return true;
    }
    redirect('index');
  }

  public function index()
  {
    if ($this->isLogin()) {
      $calonSiswaList = $this->calonSiswa->findAll();
      $data = array(
        "activeMenu" => MenuConstant::CALON_SISWA,
        "calonSiswaList" => $calonSiswaList,
        'konfigurasiList' => $this->getKonfigurasiList(),
        'panitia' => $this->getPanitia()
      );

      $this->load->view('calon_siswa/list', $data);
    }
  }

  public function add()
  {
    if ($this->isLogin()) {
      $data = array(
        'activeMenu' => MenuConstant::CALON_SISWA,
        'konfigurasiList' => $this->getKonfigurasiList(),
        'panitia' => $this->getPanitia()
      );

      $this->load->view('calon_siswa/add', $data);
    }
  }

  public function store()
  {
    if ($this->isLogin()) {
      $id = $this->input->post('id');
      $namaLengkap = $this->input->post('namaLengkap');
      $gender = $this->input->post('gender');
      $tempatLahir = $this->input->post('tempatLahir');
      $tanggalLahir = $this->input->post('tanggalLahir');
      $alamat = $this->input->post('alamat');
      $kecamatan = $this->input->post('kecamatan');
      $kabupaten = $this->input->post('kabupaten');
      $agama = $this->input->post('agama');
      $noHp = $this->input->post('telepon');
      $username = $this->input->post('username');
      $password = $this->encryption->encrypt($this->input->post('password'));
      $namaOrtu = $this->input->post('namaOrtu');
      $nisn = $this->input->post('nisn');

      $data = null;
      $store = false;
      $calonSiswaOptional = $this->calonSiswa->findByUsername($username);
      if (empty($id)) {
        if (!empty($calonSiswaOptional)) {
          $this->session->set_flashdata('failed', FlashData::usernameAlreadyExist());
          redirect('master/calonsiswa');
        }

        $calonSiswaOptional2 = $this->calonSiswa->findByNisn($nisn);
        if (!empty($calonSiswaOptional2)) {
          $this->session->set_flashdata('failed', FlashData::DataFound('NISN sudah terdaftar.'));
          redirect('master/calonsiswa');
        }

        $data = array(
          'nama_lengkap' => $namaLengkap,
          'gender' => $gender,
          'agama' => $agama,
          'alamat' => $alamat,
          'kecamatan' => $kecamatan,
          'kabupaten' => $kabupaten,
          'tempat_lahir' => $tempatLahir,
          'tanggal_lahir' => $tanggalLahir,
          'no_hp' => $noHp,
          'nama_ortu' => $namaOrtu,
          'username' => $username,
          'password' => $password,
          'nisn' => $nisn,
          'status' => StatusPendaftarConstant::AKTIF
        );

        $store = $this->calonSiswa->save($data);
      } else {
        $siswaOptional = $this->calonSiswa->findById($id);
        if ($siswaOptional[0]->username != $username && !empty($calonSiswaOptional)) {
          $this->session->set_flashdata('failed', FlashData::usernameAlreadyExist());
          redirect('master/calonsiswa');
        }

        $siswaOptional2 = $this->calonSiswa->findByNisn($nisn);
        if (!empty($siswaOptional2) && $siswaOptional2[0]->nisn != $nisn) {
          $this->session->set_flashdata('failed', FlashData::DataFound('NISN sudah terdaftar.'));
          redirect('master/calonsiswa');
        }

        $data = array(
          'id_calon_siswa' => $id,
          'nama_lengkap' => $namaLengkap,
          'gender' => $gender,
          'agama' => $agama,
          'alamat' => $alamat,
          'kecamatan' => $kecamatan,
          'kabupaten' => $kabupaten,
          'tempat_lahir' => $tempatLahir,
          'tanggal_lahir' => $tanggalLahir,
          'no_hp' => $noHp,
          'nama_ortu' => $namaOrtu,
          'username' => $username,
          'password' => $password,
          'nisn' => $nisn,
          'status' => StatusPendaftarConstant::AKTIF
        );

        $store = $this->calonSiswa->update($data);
      }

      if ($store) {
        $this->session->set_flashdata('success', FlashData::successSaved());
        redirect('master/calonsiswa');
      } else {
        $this->session->set_flashdata('failed', FlashData::failedSaved());
        redirect('master/calonsiswa');
      }
    }
  }

  public function get($id)
  {
    if ($this->isLogin()) {
      $calonSiswaOptional = $this->calonSiswa->findById($id);
      $data = array(
        "activeMenu" => MenuConstant::CALON_SISWA,
        'calonSiswa' => $calonSiswaOptional,
        'konfigurasiList' => $this->getKonfigurasiList(),
        'panitia' => $this->getPanitia()
      );

      $this->load->view('calon_siswa/edit', $data);
    }
  }

  public function delete()
  {
    if ($this->isLogin()) {
      $id = $this->input->post('id');
      $pendaftaranOptional = $this->pendaftaran->findByCalonSiswaId($id);
      if (empty($pendaftaranOptional)) {
        $this->session->set_flashdata('failed', FlashData::failedDeleteData('Data calon siswa sudah digunakan untuk melakukan pendaftaran.'));
      } else {
        $store = $this->calonSiswa->delete($id);
        if ($store) {
          $this->session->set_flashdata('success', FlashData::deletedSuccess());
          redirect('master/calonsiswa');
        } else {
          $this->session->set_flashdata('failed', FlashData::deletedFailed());
          redirect('master/calonsiswa');
        }
      }
    }
  }
}
