<?php 

class DaftarUlang extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('StatusSeleksiPendaftaranCalonSiswaModel', 'statusSeleksi');
    $this->load->model('PendaftaranModel', 'pendaftaran');
    $this->load->model('DaftarUlangModel', 'daftarUlang');
    $this->load->model('KonfigurasiModel', 'konfigurasi');
    $this->load->model('CalonSiswaModel', 'calonSiswa');
    $this->load->model('PanitiaModel','panitia');
  }

  public function getKonfigurasiList()
  {
    return $this->konfigurasi->findAll();
  }

  public function getPanitia()
  {
    return $this->panitia->findById($this->session->userdata('id'));
  }

  public function isLogin()
  {
    if ($this->session->userdata('status') == LoginSessionConstant::PANITIA) {
      return true;
    }
    redirect('login');
  }

  public function index()
  {
    if ($this->isLogin()) {
      $konfigTahunAjaran = $this->konfigurasi->findByModule(ModuleConstant::TAHUN_AJARAN);
      $pendaftaranList = $this->pendaftaran->findByTahunAjaran($konfigTahunAjaran[0]->value);
      $noPendaftaranList = array();
      if (!empty($pendaftaranList)) {
        foreach ($pendaftaranList as $pendaftaran) {
          $noPendaftaranList[] = $pendaftaran->no_pendaftaran;
        }
      }
      
      $daftarUlangList = array();
      if (!empty($noPendaftaranList)) {
        $daftarUlangList = $this->daftarUlang->findByNoPendaftaranIn($noPendaftaranList);
      }

      $data = array(
        "activeMenu" => MenuConstant::DAFTAR_ULANG,
        "daftarUlangList" => $daftarUlangList,
        'pendaftaranList' => $pendaftaranList,
        'konfigurasiList' => $this->getKonfigurasiList(),
        'panitia' => $this->getPanitia()
      );

      $this->load->view('daftar_ulang/list', $data);
    }
  }

  public function approved()
  {
    if ($this->isLogin()) {
      $catatan = $this->input->post('catatan');
      $id = $this->input->post('id');
      $statusApproved = $this->input->post('statusApproved');
      $daftarUlangOptional = $this->daftarUlang->findById($id);
      if (!empty($daftarUlangOptional)) {
        $data = array(
          'id_daftar_ulang' => $daftarUlangOptional[0]->id_daftar_ulang,
          'tgl_daftar_ulang' => $daftarUlangOptional[0]->tgl_daftar_ulang,
          'status_id' => $daftarUlangOptional[0]->status_id,
          'no_pendaftaran' => $daftarUlangOptional[0]->no_pendaftaran,
          'nama_lengkap' => $daftarUlangOptional[0]->nama_lengkap,
          'gender' => $daftarUlangOptional[0]->gender,
          'nisn' => $daftarUlangOptional[0]->nisn,
          'nik_siswa' => $daftarUlangOptional[0]->nik_siswa,
          'tmp_lhr_siswa' => $daftarUlangOptional[0]->tmp_lhr_siswa,
          'tgl_lhr_siswa' => $daftarUlangOptional[0]->tgl_lhr_siswa,
          'agama' => $daftarUlangOptional[0]->agama,
          'kewarganegaan' => $daftarUlangOptional[0]->kewarganegaan,
          'alamat' => $daftarUlangOptional[0]->alamat,
          'rt' => $daftarUlangOptional[0]->rt,
          'rw' => $daftarUlangOptional[0]->rw,
          'nama_dusun' => $daftarUlangOptional[0]->nama_dusun,
          'nama_kelurahan' => $daftarUlangOptional[0]->nama_kelurahan,
          'kecamatan' => $daftarUlangOptional[0]->kecamatan,
          'kabupaten' => $daftarUlangOptional[0]->kabupaten,
          'kode_pos' => $daftarUlangOptional[0]->kode_pos,
          'nama_ayah' => $daftarUlangOptional[0]->nama_ayah,
          'nik_ayah' => $daftarUlangOptional[0]->nik_ayah,
          'thn_lhr_ayah' => $daftarUlangOptional[0]->thn_lhr_ayah,
          'nama_ibu' => $daftarUlangOptional[0]->nama_ibu,
          'nik_ibu' => $daftarUlangOptional[0]->nik_ibu,
          'thn_lhr_ibu'=> $daftarUlangOptional[0]->thn_lhr_ibu,
          'no_hp_ortu'=> $daftarUlangOptional[0]->no_hp_ortu,
          'upload_dokumen' => $daftarUlangOptional[0]->upload_dokumen,
          'status_review' => 'REVIEWED',
          'catatan' => $catatan,
          'status_approved' => $statusApproved
        );

        $calonSiswaId = null;
        if (!empty($daftarUlangOptional)) {
          echo $daftarUlangOptional[0]->no_pendaftaran;
          $pendaftaranOptional = $this->pendaftaran->findByNoPendaftaran($daftarUlangOptional[0]->no_pendaftaran);
          if (!empty($pendaftaranOptional)) {
            $calonSiswaId = $pendaftaranOptional[0]->calon_siswa_id;
          }
        }
        $dataCalonSiswa = null;
        if ($calonSiswaId != null) {
          $calonSiswaOptional = $this->calonSiswa->findById($calonSiswaId);
          if (!empty($calonSiswaOptional)) {
            $dataCalonSiswa = array(
              'id_calon_siswa' => $calonSiswaOptional[0]->id_calon_siswa,
              'nama_lengkap' => $calonSiswaOptional[0]->nama_lengkap,
              'gender' => $calonSiswaOptional[0]->gender,
              'agama' => $calonSiswaOptional[0]->agama,
              'alamat' => $calonSiswaOptional[0]->alamat,
              'kecamatan' => $calonSiswaOptional[0]->kecamatan,
              'kabupaten' => $calonSiswaOptional[0]->kabupaten,
              'tempat_lahir' => $calonSiswaOptional[0]->tempat_lahir,
              'tanggal_lahir' => $calonSiswaOptional[0]->tanggal_lahir,
              'no_hp' => $calonSiswaOptional[0]->no_hp,
              'nama_ortu' => $calonSiswaOptional[0]->nama_ortu,
              'username' => $calonSiswaOptional[0]->username,
              'password' => $calonSiswaOptional[0]->password,
              'nisn' => $calonSiswaOptional[0]->nisn,
              'status' => StatusPendaftarConstant::BLACKLIST
            );
          }
        }
        $store = $this->daftarUlang->update($data);
        if ($store) {
          if ($dataCalonSiswa != null) {
            $this->calonSiswa->update($dataCalonSiswa);
          }
          $this->session->set_flashdata('success', FlashData::successSaved());
          redirect('master/daftarulang');
        } else {
          $this->session->set_flashdata('failed', FlashData::failedSaved());
          redirect('master/daftarulang');
        }
      }
    }
  }
}
