<?php 

class Seleksi extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('StatusSeleksiPendaftaranCalonSiswaModel', 'statusSeleksi');
    $this->load->model('PendaftaranModel', 'pendaftaran');
    $this->load->model('CalonSiswaModel', 'calonSiswa');
    $this->load->model('KonfigurasiModel', 'konfigurasi');
    $this->load->model('PanitiaModel','panitia');
  }

  public function getKonfigurasiList()
  {
    return $this->konfigurasi->findAll();
  }

  public function getPanitia()
  {
    return $this->panitia->findById($this->session->userdata('id'));
  }

  public function isLogin()
  {
    if ($this->session->userdata('status') == LoginSessionConstant::PANITIA) {
      return true;
    }
    redirect('login');
  }

  public function index()
  {
    if ($this->isLogin()) {
      $pendaftaranList = $this->pendaftaran->findByTahunAjaran($this->getTahunAjaran());
      $noPendaftaranList = array();
      if (!empty($pendaftaranList)) {
        foreach ($pendaftaranList as $pendaftaran) {
          $noPendaftaranList[] = $pendaftaran->no_pendaftaran;
        }
      }

      $statusSeleksiList = array();
      if (!empty($noPendaftaranList)) {
        $statusSeleksiList = $this->statusSeleksi->findByNoPendaftaranIn($noPendaftaranList);
      }
      
      $noPendaftaranList = array();
      if (!empty($statusSeleksiList)) {
        foreach ($statusSeleksiList as $statusSeleksi) {
          $noPendaftaranList[] = $statusSeleksi->no_pendaftaran;
        }
      }

      $calonSiswaIdList = array();
      if (!empty($noPendaftaranList)) {
        $pendaftaranList = $this->pendaftaran->findByNoPendaftaranIn($noPendaftaranList);
        if (!empty($pendaftaranList)) {
          foreach ($pendaftaranList as $pendaftaran) {
            $calonSiswaIdList[] = $pendaftaran->calon_siswa_id;
          }
        }
      }

      $calonSiswaList = array();
      if (!empty($calonSiswaIdList)) {
        $calonSiswaList = $this->calonSiswa->findByIdIn($calonSiswaIdList);
      }

      $data = array(
        "activeMenu" => MenuConstant::SELEKSI,
        "seleksiList" => $statusSeleksiList,
        "pendaftaranList" => $pendaftaranList,
        'needSeleksi' => $this->calculateBatasWaktuSeleksi(),
        'calonSiswaList' => $calonSiswaList,
        'konfigurasiList' => $this->getKonfigurasiList(),
        'panitia' => $this->getPanitia()
      );

      $this->load->view('seleksi/list', $data);
    }
  }

  public function getPengumumanSeleksi()
  {
    if ($this->isLogin()) {
      $konfigOptional = $this->konfigurasi->findByModule(ModuleConstant::PENGUMUMAN_SELEKSI);
      return $konfigOptional[0]->value;
    }
  }

  public function calculateBatasWaktuSeleksi()
  {
    if ($this->isLogin()) {
      $konfigBatasWaktuPendaftaran = $this->konfigurasi->findByModule(ModuleConstant::BATAS_PENDAFTARAN);
      $konfigBatasWaktuPendaftaranLong = strtotime($konfigBatasWaktuPendaftaran[0]->value);
      $pengumumanSeleksi = $this->getPengumumanSeleksi();
      $pengumumanSeleksiLong = strtotime($pengumumanSeleksi);
      $dateNowLong = strtotime(Date('d-m-Y'));
      $lebihDariBatasPendaftaran = ($dateNowLong - $konfigBatasWaktuPendaftaranLong) > 0;
      $kurangDariBatasPengumuman = ($dateNowLong - $pengumumanSeleksiLong) < 0;
      if ($lebihDariBatasPendaftaran && $kurangDariBatasPengumuman) {
        return true;
      }
      return false;
    }
  }

  private function calculateAge($tanggalLahir)
  {
    if ($this->isLogin()) {
      $dateNowLong = strtotime(Date('d-m-Y'));
      $tanggalLahirLong = strtotime($tanggalLahir);
      $remainderDate = (int) $dateNowLong - $tanggalLahirLong;
      $resultDay = $remainderDate / 86400;
      $age = $resultDay / 365;
      return round($age);
    }
  }

  public function getTahunAjaran()
  {
    if ($this->isLogin()) {
      $konfigurasiOptional = $this->konfigurasi->findByModule(ModuleConstant::TAHUN_AJARAN);
      return $konfigurasiOptional[0]->value;
    }
  }

  public function allSelection()
  {
    if ($this->isLogin()) {
      $pendaftaranList = $this->pendaftaran->findByTahunAjaran($this->getTahunAjaran());
      if (empty($pendaftaranList)) {
        $this->session->set_flashdata('failed', FlashData::generalError('Data pendaftaran masih kosong.'));
        redirect('master/seleksi');
      }

      $counter = 0;
      $konfigZonasi = $this->konfigurasi->findByModule(ModuleConstant::ZONASI);
      $zonasi = null;
      if (!empty($konfigZonasi)) {
        $zonasi = $konfigZonasi[0]->value;
      }

      foreach ($pendaftaranList as $data) {
        $calonSiswaOptional = $this->calonSiswa->findById($data->calon_siswa_id);
        $umur = $this->calculateAge($calonSiswaOptional[0]->tanggal_lahir);
        $calonSiswaOptional2 = $this->calonSiswa->findByIdCalonSiswaAndKabupaten($data->id_calon_siswa, $zonasi);
        $statusSeleksi = StatusSeleksiConstant::TERIMA;
        $catatan = null;
        if ($umur > 15) {
          $statusSeleksi = StatusSeleksiConstant::TOLAK;
          $catatan = 'usia anda tidak memenuhi syarat';
        } else if (empty($calonSiswaOptional2)) {
          $statusSeleksi = StatusSeleksiConstant::TOLAK;
          $catatan = 'anda tidak lolos seleksi zonasi';
        }

        $dataStore = array(
          'no_pendaftaran' => $data->no_pendaftaran,
          'tgl_diterima_atau_ditolak' => Date('Y-m-d h:i:s'),
          'umur' => $umur,
          'status_diterima_atau_ditolak' => $statusSeleksi,
          'admin_pendaftaran_id' => $this->session->userdata('id'),
          'status_review' => StatusReviewConstant::WAITING_REVIEW,
          'batas_waktu_seleksi' => $this->getPengumumanSeleksi(),
          'catatan' => $catatan,
        );

        $statusSeleksiOptional = $this->statusSeleksi->findByNoPendaftaran($data->no_pendaftaran);
        if (empty($statusSeleksiOptional)) {
          $store = $this->statusSeleksi->save($dataStore);
          if ($store) {
            $counter++;
          }
        }
      }

      if ($counter > 0) {
        $this->session->set_flashdata('success', FlashData::success('Berhasil menyeleksi semua data. Harap periksa kembali data yang sudah diseleksi.'));
        redirect('master/seleksi');
      } else {
        $this->session->set_flashdata('warning', FlashData::warning('Tidak ada data yang diseleksi.'));
        redirect('master/seleksi');
      }
    }
  }

  public function seleksiPersonal()
  {
    if ($this->isLogin()) {
      $noPendaftaran = $this->input->post('noPendaftaran');
      $catatan = $this->input->post('catatan');
      $statusSeleksi = $this->input->post('statusSeleksi');
      $statusSeleksiOptional = $this->statusSeleksi->findByNoPendaftaran($noPendaftaran);
      $data = array(
        'id_status' => $statusSeleksiOptional[0]->id_status,
        'no_pendaftaran' => $statusSeleksiOptional[0]->no_pendaftaran,
        'tgl_diterima_atau_ditolak' => Date('Y-m-d h:i:s'),
        'umur' => $statusSeleksiOptional[0]->umur,
        'status_diterima_atau_ditolak' => $statusSeleksi,
        'admin_pendaftaran_id' => $this->session->userdata('id'),
        'status_review' => StatusReviewConstant::REVIEWED,
        'catatan' => $catatan,
        'batas_waktu_seleksi' => $statusSeleksiOptional[0]->batas_waktu_seleksi
      );

      $store = $this->statusSeleksi->update($data);
      if ($store) {
        $this->session->set_flashdata('success', FlashData::successSaved());
        redirect('master/seleksi');
      } else {
        $this->session->set_flashdata('failed', FlashData::failedSaved());
        redirect('master/seleksi');
      }
    }
  }
}
