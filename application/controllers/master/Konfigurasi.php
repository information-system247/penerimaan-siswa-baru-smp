<?php 

class Konfigurasi extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('KonfigurasiModel', 'konfigurasi');
  }

  public function getKonfigurasiList()
  {
    return $this->konfigurasi->findAll();
  }

  public function isLogin()
  {
    if ($this->session->userdata('status') == LoginSessionConstant::PANITIA) {
      return true;
    }
    redirect('login');
  }

  public function index()
  {
    if ($this->isLogin()) {
      $tahunAjaran = null;
      $batasPendaftaran = null;
      $pengumumanSeleksi = null;
      $batasDaftarUlang = null;
      $zonasi = null;
      $konfigurasiList = $this->konfigurasi->findAll();
      if (!empty($konfigurasiList)) {
        foreach ($konfigurasiList as $konfig) {
          if ($konfig->module === ModuleConstant::TAHUN_AJARAN) {
            $tahunAjaran = $konfig->value;
          } else if ($konfig->module === ModuleConstant::BATAS_PENDAFTARAN) {
            $batasPendaftaran = $konfig->value;
          } else if ($konfig->module === ModuleConstant::PENGUMUMAN_SELEKSI) {
            $pengumumanSeleksi = $konfig->value;
          } else if ($konfig->module === ModuleConstant::BATAS_DAFTAR_ULANG) {
            $batasDaftarUlang = $konfig->value;
          } else if ($konfig->module === ModuleConstant::ZONASI) {
            $zonasi = $konfig->value;
          }
        }
      }
      $data = array(
        "activeMenu" => MenuConstant::KONFIGURASI,
        "tahunAjaran" => $tahunAjaran,
        'batasPendaftaran' => $batasPendaftaran,
        'pengumumanSeleksi' => $pengumumanSeleksi,
        'batasDaftarUlang' => $batasDaftarUlang,
        'zonasi' => $zonasi,
        'konfigurasiList' => $this->getKonfigurasiList()
      );

      $this->load->view('konfigurasi/add', $data);
    }
  }

  public function store()
  {
    if ($this->isLogin()) {
      $tahunAjaran = $this->input->post('tahunAjaran');
      $batasPendaftaran = $this->input->post('jangkaWaktuPendaftaran');
      $pengumumanSeleksi = $this->input->post('jangkaWaktuSeleksi');
      $batasDaftarUlang = $this->input->post('jangkaWaktuDaftarUlang');
      $zonasi = $this->input->post('zonasi');
      $konfigTahunAjaran = $this->konfigurasi->findByModule(ModuleConstant::TAHUN_AJARAN);
      if (empty($konfigTahunAjaran)) {
        $data = array(
          'module' => ModuleConstant::TAHUN_AJARAN,
          'value' => $tahunAjaran
        );
        $this->konfigurasi->save($data);
      } else {
        $data = array(
          'id_parameter' => $konfigTahunAjaran[0]->id_parameter,
          'module' => $konfigTahunAjaran[0]->module,
          'value' => $tahunAjaran
        );
        $this->konfigurasi->update($data);
      }

      $konfigBatasPendaftaran = $this->konfigurasi->findByModule(ModuleConstant::BATAS_PENDAFTARAN);
      if (empty($konfigBatasPendaftaran)) {
        $data = array(
          'module' => ModuleConstant::BATAS_PENDAFTARAN,
          'value' => $batasPendaftaran
        );
        $this->konfigurasi->save($data);
      } else {
        $data = array(
          'id_parameter' => $konfigBatasPendaftaran[0]->id_parameter,
          'module' => $konfigBatasPendaftaran[0]->module,
          'value' => $batasPendaftaran
        );
        $this->konfigurasi->update($data);
      }

      $konfigPengumumanSeleksi = $this->konfigurasi->findByModule(ModuleConstant::PENGUMUMAN_SELEKSI);
      if (empty($konfigPengumumanSeleksi)) {
        $data = array(
          'module' => ModuleConstant::PENGUMUMAN_SELEKSI,
          'value' => $pengumumanSeleksi
        );
        $this->konfigurasi->save($data);
      } else {
        $data = array(
          'id_parameter' => $konfigPengumumanSeleksi[0]->id_parameter,
          'module' => $konfigPengumumanSeleksi[0]->module,
          'value' => $pengumumanSeleksi
        );
        $this->konfigurasi->update($data);
      }

      $konfigDaftarUlang = $this->konfigurasi->findByModule(ModuleConstant::BATAS_DAFTAR_ULANG);
      if (empty($konfigDaftarUlang)) {
        $data = array(
          'module' => ModuleConstant::BATAS_DAFTAR_ULANG,
          'value' => $batasDaftarUlang
        );
        $this->konfigurasi->save($data);
      } else {
        $data = array(
          'id_parameter' => $konfigDaftarUlang[0]->id_parameter,
          'module' => $konfigDaftarUlang[0]->module,
          'value' => $batasDaftarUlang
        );
        $this->konfigurasi->update($data);
      }

      $konfigZonasi = $this->konfigurasi->findByModule(ModuleConstant::ZONASI);
      if (empty($konfigZonasi)) {
        $data = array(
          'module' => ModuleConstant::ZONASI,
          'value' => $zonasi
        );
        $this->konfigurasi->save($data);
      } else {
        $data = array(
          'id_parameter' => $konfigZonasi[0]->id_parameter,
          'module' => $konfigZonasi[0]->module,
          'value' => $zonasi
        );
        $this->konfigurasi->update($data);
      }

      $data = array(
        "activeMenu" => MenuConstant::KONFIGURASI,
        "tahunAjaran" => $tahunAjaran,
        'batasPendaftaran' => $batasPendaftaran,
        'pengumumanSeleksi' => $pengumumanSeleksi,
        'batasDaftarUlang' => $batasDaftarUlang,
        'zonasi' => $zonasi
      );

      $this->load->view('konfigurasi/add', $data);
    }
  }
}
