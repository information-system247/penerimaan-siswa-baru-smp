<?php 

class PanitiaPendaftaran extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('PanitiaModel','panitia');
    $this->load->model('StatusSeleksiPendaftaranCalonSiswaModel', 'statusSeleksi');
    $this->load->model('KonfigurasiModel', 'konfigurasi');
    $this->load->model('PanitiaModel','panitia');
  }

  public function getKonfigurasiList()
  {
    return $this->konfigurasi->findAll();
  }

  public function getPanitia()
  {
    return $this->panitia->findById($this->session->userdata('id'));
  }

  public function isLogin()
  {
    if ($this->session->userdata('status') == LoginSessionConstant::PANITIA) {
      return true;
    }
    redirect('login');
  }
  
  public function index()
  {
    if ($this->isLogin()) {
      
      $hakAkses = $this->session->userdata('hak_akses');
      $panitiaList = $this->panitia->findByNotInHakAkses($hakAkses);
      $data = array(
        "activeMenu" => MenuConstant::PANITIA,
        "panitiaList" => $panitiaList,
        'konfigurasiList' => $this->getKonfigurasiList(),
        'panitia' => $this->getPanitia()
      );

      $this->load->view('panitia/list', $data);
    }
  }

  public function add()
  {
    if ($this->isLogin()) {
      $data = array(
        'activeMenu' => MenuConstant::PANITIA,
        'konfigurasiList' => $this->getKonfigurasiList(),
        'panitia' => $this->getPanitia()
      );

      $this->load->view('panitia/add', $data);
    }
  }

  public function store()
  {
    if ($this->isLogin()) {
      $nik = $this->input->post('nik');
      $namaLengkap = $this->input->post('namaLengkap');
      $gender = $this->input->post('gender');
      $email = $this->input->post('email');
      $hp = $this->input->post('telepon');
      $username = $this->input->post('username');
      $password = $this->input->post('password');
      $hakAkses = $this->input->post('hakAkses');

      $panitiaOptional = $this->panitia->findByNik($nik);
      if (!empty($panitiaOptional)) {
        $this->session->set_flashdata('failed', FlashData::nikAlreadyExist());
        redirect('master/panitiapendaftaran');
      }

      $panitiaOptional = $this->panitia->findByEmail($email);
      if (!empty($panitiaOptional)) {
        $this->session->set_flashdata('failed', FlashData::emailAlreadyExist());
        redirect('master/panitiapendaftaran');
      }

      $panitiaOptional = $this->panitia->findByPhoneNumber($hp);
      if (!empty($panitiaOptional)) {
        $this->session->set_flashdata('failed', FlashData::phoneNumberAlreadyExist());
        redirect('master/panitiapendaftaran');
      }

      $panitiaOptional = $this->panitia->findByUsername($username);
      if (!empty($panitiaOptional)) {
        $this->session->set_flashdata('failed', FlashData::usernameAlreadyExist());
        redirect('master/panitiapendaftaran');
      }

      $panitiaOptional = $this->panitia->findByHakAkses($hakAkses);
      if (!empty($panitiaOptional) && $hakAkses == HakAksesConstant::ADMIN) {
        $this->session->set_flashdata('failed', FlashData::hakAksesAlreadyExist());
        redirect('master/panitiapendaftaran');
      }

      $data = array(
        'nama' => $namaLengkap,
        'gender' => $gender,
        'email' => $email,
        'no_hp' => $hp,
        'username' => $username,
        'password' => $this->encryption->encrypt($password),
        'nik' => $nik,
        'hak_akses' => $hakAkses
      );

      $store = $this->panitia->save($data);
      if ($store) {
        $this->session->set_flashdata('success', FlashData::successSaved());
        redirect('master/panitiapendaftaran');
      } else {
        $this->session->set_flashdata('failed', FlashData::failedSaved());
        redirect('master/panitiapendaftaran');
      }
    }
  }

  public function get($id)
  {
    if ($this->isLogin()) {
      $panitiaOptional = $this->panitia->findById($id);
      $data = array(
        "activeMenu" => MenuConstant::PANITIA,
        'panitia' => $panitiaOptional,
        'konfigurasiList' => $this->getKonfigurasiList(),
        'panitia' => $this->getPanitia()
      );

      $this->load->view('panitia/edit', $data);
    }
  }

  public function update()
  {
    if ($this->isLogin()) {
      $id = $this->input->post('id');
      $nik = $this->input->post('nik');
      $namaLengkap = $this->input->post('namaLengkap');
      $gender = $this->input->post('gender');
      $email = $this->input->post('email');
      $hp = $this->input->post('telepon');
      $username = $this->input->post('username');
      $password = $this->input->post('password');
      $hakAkses = $this->input->post('hakAkses');

      $panitiaOptional = $this->panitia->findById($id);
      if (!empty($panitiaOptional)) {
        if ($panitiaOptional[0]->nik != $nik) {
          $panitiaOptional = $this->panitia->findByNik($nik);
          if (!empty($panitiaOptional)) {
            $this->session->set_flashdata('failed', FlashData::nikAlreadyExist());
            redirect('master/panitiapendaftaran');
          }
        }

        if ($panitiaOptional[0]->email != $email) {
          $panitiaOptional = $this->panitia->findByEmail($email);
          if (!empty($panitiaOptional)) {
            $this->session->set_flashdata('failed', FlashData::emailAlreadyExist());
            redirect('master/panitiapendaftaran');
          }
        }

        if ($panitiaOptional[0]->no_hp != $hp) {
          $panitiaOptional = $this->panitia->findByPhoneNumber($hp);
          if (!empty($panitiaOptional)) {
            $this->session->set_flashdata('failed', FlashData::phoneNumberAlreadyExist());
            redirect('master/panitiapendaftaran');
          }
        }

        if ($panitiaOptional[0]->username != $username) {
          $panitiaOptional = $this->panitia->findByUsername($username);
          if (!empty($panitiaOptional)) {
            $this->session->set_flashdata('failed', FlashData::usernameAlreadyExist());
            redirect('master/panitiapendaftaran');
          }
        }

        if ($hakAkses == HakAksesConstant::ADMIN && $panitiaOptional[0]->hak_akses != HakAksesConstant::ADMIN) {
          $panitiaOptional = $this->panitia->findByHakAkses($hakAkses);
          if (!empty($panitiaOptional)) {
            $this->session->set_flashdata('failed', FlashData::hakAksesAlreadyExist());
            redirect('master/panitiapendaftaran');
          }
        }

        $data = array(
          'id_admin_pendaftaran' => $id,
          'nama' => $namaLengkap,
          'gender' => $gender,
          'email' => $email,
          'no_hp' => $hp,
          'username' => $username,
          'password' => $this->encryption->encrypt($password),
          'nik' => $nik,
          'hak_akses' => $hakAkses
        );

        $store = $this->panitia->update($data);
        if ($store) {
          $this->session->set_flashdata('success', FlashData::successSaved());
          redirect('master/panitiapendaftaran');
        } else {
          $this->session->set_flashdata('failed', FlashData::failedSaved());
          redirect('master/panitiapendaftaran');
        }
      }
    }
  }

  public function delete()
  {
    if ($this->isLogin()) {
      $id = $this->input->post('id');
      $statusSeleksiList = $this->statusSeleksi->findByAdminPendaftaranId($id);
      if (!empty($statusSeleksiList)) {
        $this->session->set_flashdata('failed', FlashData::failedDeleteData('Data panitia sudah digunakan untuk seleksi.'));
      } else {
        $store = $this->panitia->delete($id);
        if ($store) {
          $this->session->set_flashdata('success', FlashData::deletedSuccess());
          redirect('master/panitiapendaftaran');
        } else {
          $this->session->set_flashdata('failed', FlashData::deletedFailed());
          redirect('master/panitiapendaftaran');
        }
      }
    }
  }
}

?>