<?php
    class Index extends CI_Controller
    {
        public function __construct() {
            parent::__construct();
            $this->load->model('KonfigurasiModel', 'konfigurasi');
            $this->load->model('PendaftaranModel', 'pendaftaran');
            $this->load->model('StatusSeleksiPendaftaranCalonSiswaModel','statusSeleksi');
            $this->load->model('PanitiaModel','panitia');
            $this->load->model('CalonSiswaModel','calonSiswa');
        }

        public function getPanitia()
        {
            return $this->panitia->findById($this->session->userdata('id'));
        }

        public function isLogin()
        {
            if ($this->session->userdata('status') == LoginSessionConstant::PANITIA) {
                return true;
            }
            redirect('login');
        }

        public function index() {
            // $encrypt = "7122ec39733cc51de892a1d1973eb00e23745de066dc7c29e296657d7c6900390f561861ed2cb9404823eb1f07cee69598b7e424317a1f8f218d9287d3332047mTLUTLX5JvPTLi3FIsVIfdc1FpuVQQSdn0R/WKw+Va0=";
            // $decrypt = $this->encryption->decrypt($encrypt);
            if ($this->isLogin()) {
                $configTahunAjaran = $this->konfigurasi->findByModule(ModuleConstant::TAHUN_AJARAN);
                $tahunAjaran = null;
                if (!empty($configTahunAjaran)) {
                    $tahunAjaran = $configTahunAjaran[0]->value;
                }

                $pendaftaranList = array();
                if ($tahunAjaran != null) {
                    $pendaftaranList = $this->pendaftaran->findByTahunAjaran($tahunAjaran);
                }
                
                $noPendaftaranList = array();
                if (!empty($pendaftaranList)) {
                    foreach ($pendaftaranList as $pendaftaran) {
                        $noPendaftaranList[] = $pendaftaran->no_pendaftaran;
                    }
                }

                $noPendaftaranListLolos = array();
                $noPendaftaranListTidakLolos = array();
                if (!empty($noPendaftaranList)) {
                    $statusSeleksiList = $this->statusSeleksi->findByNoPendaftaranIn($noPendaftaranList);
                    if (!empty($statusSeleksiList)) {
                        foreach ($statusSeleksiList as $statusSeleksi) {
                            if ($statusSeleksi->status_diterima_atau_ditolak == StatusSeleksiConstant::TERIMA) {
                                $noPendaftaranListLolos[] = $statusSeleksi->no_pendaftaran;
                            } else {
                                $noPendaftaranListTidakLolos[] = $statusSeleksi->no_pendaftaran;
                            }
                        }
                    }
                }

                $statusSeleksiLolosList = array();
                if (!empty($noPendaftaranListLolos)) {
                    $statusSeleksiLolosList = $this->statusSeleksi->findByNoPendaftaranIn($noPendaftaranListLolos);
                }

                $statusSeleksiTidakLolosList = array();
                if (!empty($noPendaftaranListTidakLolos)) {
                    $statusSeleksiTidakLolosList = $this->statusSeleksi->findByNoPendaftaranIn($noPendaftaranListTidakLolos);
                }

                $konfigurasiList = $this->konfigurasi->findAll();

                $pendaftaranList = $this->pendaftaran->findByTahunAjaran($tahunAjaran);
                $calonSiswaIdList = array();
                if (!empty($pendaftaranList)) {
                foreach ($pendaftaranList as $pendaftaran) {
                    $calonSiswaIdList[] = $pendaftaran->calon_siswa_id;
                }
                }

                $calonSiswaList = array();
                if (!empty($calonSiswaIdList)) {
                $calonSiswaList = $this->calonSiswa->findByIdIn($calonSiswaIdList);
                }
                
                $pendaftaranLolosSeleksiList = array();
                if (!empty($noPendaftaranListLolos)) {
                $pendaftaranLolosSeleksiList = $this->pendaftaranLolosSeleksiList($noPendaftaranListLolos);
                }
                
                $pendaftaranTidakLolosSeleksiList = array();
                if (!empty($noPendaftaranListTidakLolos)) {
                $pendaftaranTidakLolosSeleksiList = $this->pendaftaranLolosSeleksiList($noPendaftaranListTidakLolos);
                }
                
                $data = array(
                    "activeMenu" => MenuConstant::BERANDA,
                    'tahunAjaran' => $tahunAjaran,
                    'totalResultPendaftaran' => count($pendaftaranList),
                    'totalResultStatusSeleksiLolos' => count($statusSeleksiLolosList),
                    'totalResultSTatusSeleksiTidakLolos' => count($statusSeleksiTidakLolosList),
                    'konfigurasiList' => $konfigurasiList,
                    'calonSiswaList' => $calonSiswaList,
                    "pendaftaranList" => $pendaftaranList,
                    'lolosSeleksiList' => $statusSeleksiLolosList,
                    'pendaftaranLolosSeleksiList' => $pendaftaranLolosSeleksiList,
                    'calonSiswaLolosSeleksiList' => $this->calonSiswaLolosSeleksiList($pendaftaranLolosSeleksiList),
                    'pendaftaranTidakLolosSeleksiList' => $pendaftaranTidakLolosSeleksiList,
                    'calonSiswaTidakLolosSeleksiList' => $this->calonSiswaLolosSeleksiList($pendaftaranTidakLolosSeleksiList),
                    'panitia' => $this->getPanitia()
                );
                
                $this->load->view('index', $data);
            }
        }

        public function pendaftaranLolosSeleksiList($noPendaftaranListLolos)
        {
            $seleksiList = $this->pendaftaran->findByNoPendaftaranIn($noPendaftaranListLolos);
            $noPendaftaranList = array();
            if (!empty($seleksiList)) {
            foreach ($seleksiList as $seleksi) {
                $noPendaftaranList[] = $seleksi->no_pendaftaran;
            }
            }

            $pendaftaranList = array();
            if (!empty($noPendaftaranList)) {
            $pendaftaranList = $this->pendaftaran->findByNoPendaftaranIn($noPendaftaranList);
            }

            return $pendaftaranList;
        }

        public function calonSiswaLolosSeleksiList($pendaftaranLolosSeleksiList)
        {
            $calonsiswaIdList = array();
            if (!empty($pendaftaranLolosSeleksiList)) {
            foreach ($pendaftaranLolosSeleksiList as $pendaftaran) {
                $calonsiswaIdList[] = $pendaftaran->calon_siswa_id;
            }
            }

            $calonSiswaList = array();
            if (!empty($calonsiswaIdList)) {
            $calonSiswaList = $this->calonSiswa->findByIdIn($calonsiswaIdList);
            }
            return $calonSiswaList;
        }
    }