<?php 

class Pdf extends CI_Controller
{
  function __construct(){
		parent::__construct();
    $this->load->library('pdfgenerator');
    $this->load->model('PendaftaranModel', 'pendaftaran');
    $this->load->model('CalonSiswaModel', 'calonSiswa');
    $this->load->model('StatusSeleksiPendaftaranCalonSiswaModel', 'statusSeleksi');
	}

  public function isLogin()
  {
    if ($this->session->userdata('status') == LoginSessionConstant::PANITIA) {
      return true;
    }
    redirect('login');
  }

  public function pendaftaran()
  {
    $pendaftaranList = $this->pendaftaran->findAll();
    $calonSiswaIdList = array();
    if (!empty($pendaftaranList)) {
      foreach ($pendaftaranList as $pendaftaran) {
        $calonSiswaIdList[] = $pendaftaran->calon_siswa_id;
      }
    }

    $calonSiswaList = array();
    if (!empty($calonSiswaIdList)) {
      $calonSiswaList = $this->calonSiswa->findByIdIn($calonSiswaIdList);
    }

    $file_pdf = 'Laporan pendaftaran';
    $data = array(
      'file_pdf' => $file_pdf,
      "pendaftaranList" => $pendaftaranList,
      "calonSiswaList" => $calonSiswaList,
    );

    $html = $this->load->view('laporan/pendaftaran/download', $data, true);
    $this->pdfgenerator->generate($html, $file_pdf);
  }

  public function terima()
  {
    if ($this->isLogin()) {
      $seleksiList = $this->statusSeleksi->findByStatusDiterimaAtauDitolak('TERIMA');
      $noPendaftaranList = array();
      if (!empty($seleksiList)) {
        foreach ($seleksiList as $seleksi) {
          $noPendaftaranList[] = $seleksi->no_pendaftaran;
        }
      }

      $pendaftaranList = array();
      if (!empty($noPendaftaranList)) {
        $pendaftaranList = $this->pendaftaran->findByNoPendaftaranIn($noPendaftaranList);
      }

      $calonsiswaIdList = array();
      if (!empty($pendaftaranList)) {
        foreach ($pendaftaranList as $pendaftaran) {
          $calonsiswaIdList[] = $pendaftaran->calon_siswa_id;
        }
      }

      $calonSiswaList = array();
      if (!empty($calonsiswaIdList)) {
        $calonSiswaList = $this->calonSiswa->findByIdIn($calonsiswaIdList);
      }

      $file_pdf = 'Laporan seleksi pendaftaran diterima';
      $data = array(
        'file_pdf' => $file_pdf,
        "pendaftaranList" => $pendaftaranList,
        "calonSiswaList" => $calonSiswaList,
      );

      $html = $this->load->view('laporan/seleksi_diterima/download', $data, true);
      $this->pdfgenerator->generate($html, $file_pdf);
    }
  }

  public function tolak()
  {
    if ($this->isLogin()) {
      $seleksiList = $this->statusSeleksi->findByStatusDiterimaAtauDitolak('TOLAK');
      $noPendaftaranList = array();
      if (!empty($seleksiList)) {
        foreach ($seleksiList as $seleksi) {
          $noPendaftaranList[] = $seleksi->no_pendaftaran;
        }
      }

      $pendaftaranList = array();
      if (!empty($noPendaftaranList)) {
        $pendaftaranList = $this->pendaftaran->findByNoPendaftaranIn($noPendaftaranList);
      }

      $calonsiswaIdList = array();
      if (!empty($pendaftaranList)) {
        foreach ($pendaftaranList as $pendaftaran) {
          $calonsiswaIdList[] = $pendaftaran->calon_siswa_id;
        }
      }

      $calonSiswaList = array();
      if (!empty($calonsiswaIdList)) {
        $calonSiswaList = $this->calonSiswa->findByIdIn($calonsiswaIdList);
      }

      $file_pdf = 'Laporan seleksi pendaftaran ditolak';
      $data = array(
        'file_pdf' => $file_pdf,
        "pendaftaranList" => $pendaftaranList,
        "calonSiswaList" => $calonSiswaList,
      );

      $html = $this->load->view('laporan/seleksi_ditolak/download', $data, true);
      $this->pdfgenerator->generate($html, $file_pdf);
    }
  }
}
