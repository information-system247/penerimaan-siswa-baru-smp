<?php

class Login extends CI_Controller
{
  public function __construct() {
    parent::__construct();
    $this->load->model('PanitiaModel', 'panitia');
    $this->load->model('KonfigurasiModel', 'konfigurasi');
    $this->load->model('PendaftaranModel', 'pendaftaran');
    $this->load->model('StatusSeleksiPendaftaranCalonSiswaModel','statusSeleksi');
    $this->load->model('CalonSiswaModel', 'calonSiswa');
  }

  public function index()
  {
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $panitiaOptional = $this->panitia->findByUsername($username);
    if (empty($panitiaOptional)) {
      $this->session->set_flashdata('failed', FlashData::loginFailedUsernameWrong());
      $this->load->view('login');
    } else {
      $passwordDb = $this->encryption->decrypt($panitiaOptional[0]->password);
      if ($password != $passwordDb) {
        $this->session->set_flashdata('failed', FlashData::loginFailedPasswordWrong());
        $this->load->view('login');
      } else {
        $dataSession = array(
          'id' => $panitiaOptional[0]->id_admin_pendaftaran,
          'nama' => $panitiaOptional[0]->nama,
          'gender' => $panitiaOptional[0]->gender,
          'email' => $panitiaOptional[0]->email,
          'noHp' => $panitiaOptional[0]->no_hp,
          'username' => $panitiaOptional[0]->username,
          'password' => $panitiaOptional[0]->password,
          'nik' => $panitiaOptional[0]->nik,
          'hak_akses' => $panitiaOptional[0]->hak_akses,
          'status' => LoginSessionConstant::PANITIA
        );
        
        $this->session->set_userdata($dataSession);
        $this->session->set_flashdata('success', FlashData::loginSuccess());
        $configTahunAjaran = $this->konfigurasi->findByModule(ModuleConstant::TAHUN_AJARAN);
        $tahunAjaran = null;
        if (!empty($configTahunAjaran)) {
          $tahunAjaran = $configTahunAjaran[0]->value;
        }

        $pendaftaranList = array();
        if ($tahunAjaran != null) {
          $pendaftaranList = $this->pendaftaran->findByTahunAjaran($tahunAjaran);
        }
        
        $noPendaftaranList = array();
        if (!empty($pendaftaranList)) {
          foreach ($pendaftaranList as $pendaftaran) {
            $noPendaftaranList[] = $pendaftaran->no_pendaftaran;
          }
        }

        $noPendaftaranListLolos = array();
        $noPendaftaranListTidakLolos = array();
        if (!empty($noPendaftaranList)) {
          $statusSeleksiList = $this->statusSeleksi->findByNoPendaftaranIn($noPendaftaranList);
          if (!empty($statusSeleksiList)) {
            foreach ($statusSeleksiList as $statusSeleksi) {
              if ($statusSeleksi->status_diterima_atau_ditolak == StatusSeleksiConstant::TERIMA) {
                $noPendaftaranListLolos[] = $statusSeleksi->no_pendaftaran;
              } else {
                $noPendaftaranListTidakLolos[] = $statusSeleksi->no_pendaftaran;
              }
            }
          }
        }

        $statusSeleksiLolosList = array();
        if (!empty($noPendaftaranListLolos)) {
          $statusSeleksiLolosList = $this->statusSeleksi->findByNoPendaftaranIn($noPendaftaranListLolos);
        }

        $statusSeleksiTidakLolosList = array();
        if (!empty($noPendaftaranListTidakLolos)) {
          $statusSeleksiTidakLolosList = $this->statusSeleksi->findByNoPendaftaranIn($noPendaftaranListTidakLolos);
        }

        $konfigurasiList = $this->konfigurasi->findAll();

        $pendaftaranList = $this->pendaftaran->findByTahunAjaran($tahunAjaran);
        $calonSiswaIdList = array();
        if (!empty($pendaftaranList)) {
          foreach ($pendaftaranList as $pendaftaran) {
            $calonSiswaIdList[] = $pendaftaran->calon_siswa_id;
          }
        }

        $calonSiswaList = array();
        if (!empty($calonSiswaIdList)) {
          $calonSiswaList = $this->calonSiswa->findByIdIn($calonSiswaIdList);
        }
        
        $pendaftaranLolosSeleksiList = array();
        if (!empty($noPendaftaranListLolos)) {
          $pendaftaranLolosSeleksiList = $this->pendaftaranLolosSeleksiList($noPendaftaranListLolos);
        }
        
        $pendaftaranTidakLolosSeleksiList = array();
        if (!empty($noPendaftaranListTidakLolos)) {
          $pendaftaranTidakLolosSeleksiList = $this->pendaftaranLolosSeleksiList($noPendaftaranListTidakLolos);
        }
        

        $data = array(
          "activeMenu" => MenuConstant::BERANDA,
          'tahunAjaran' => $tahunAjaran,
          'totalResultPendaftaran' => count($pendaftaranList),
          'totalResultStatusSeleksiLolos' => count($statusSeleksiLolosList),
          'totalResultSTatusSeleksiTidakLolos' => count($statusSeleksiTidakLolosList),
          'konfigurasiList' => $konfigurasiList,
          'calonSiswaList' => $calonSiswaList,
          "pendaftaranList" => $pendaftaranList,
          'lolosSeleksiList' => $statusSeleksiLolosList,
          'pendaftaranLolosSeleksiList' => $pendaftaranLolosSeleksiList,
          'calonSiswaLolosSeleksiList' => $this->calonSiswaLolosSeleksiList($pendaftaranLolosSeleksiList),
          'pendaftaranTidakLolosSeleksiList' => $pendaftaranTidakLolosSeleksiList,
          'calonSiswaTidakLolosSeleksiList' => $this->calonSiswaLolosSeleksiList($pendaftaranTidakLolosSeleksiList),
          'panitia' => $panitiaOptional
        );

        $this->load->view('index', $data);
      }
    }
  }

  public function pendaftaranLolosSeleksiList($noPendaftaranListLolos)
  {
    $seleksiList = $this->pendaftaran->findByNoPendaftaranIn($noPendaftaranListLolos);
    $noPendaftaranList = array();
    if (!empty($seleksiList)) {
      foreach ($seleksiList as $seleksi) {
        $noPendaftaranList[] = $seleksi->no_pendaftaran;
      }
    }

    $pendaftaranList = array();
    if (!empty($noPendaftaranList)) {
      $pendaftaranList = $this->pendaftaran->findByNoPendaftaranIn($noPendaftaranList);
    }

    return $pendaftaranList;
  }

  public function calonSiswaLolosSeleksiList($pendaftaranLolosSeleksiList)
  {
    $calonsiswaIdList = array();
    if (!empty($pendaftaranLolosSeleksiList)) {
      foreach ($pendaftaranLolosSeleksiList as $pendaftaran) {
        $calonsiswaIdList[] = $pendaftaran->calon_siswa_id;
      }
    }

    $calonSiswaList = array();
    if (!empty($calonsiswaIdList)) {
      $calonSiswaList = $this->calonSiswa->findByIdIn($calonsiswaIdList);
    }
    return $calonSiswaList;
  }
}
